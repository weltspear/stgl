#version 150 core

in vec2 texposf;
out vec4 outColor;

uniform sampler2D tex;
uniform float blurSizeU;
uniform float blurSizeV;

void main() {

    vec4 _average = vec4(0);

    for (int x = -4; x <= 4; x++)
    {
        for (int y = -4; y <= 4; y++){
            _average += texture(tex, vec2(texposf.x + x*blurSizeU, texposf.y + y*blurSizeV))/81.0;
        }
    }

    outColor = _average;

}