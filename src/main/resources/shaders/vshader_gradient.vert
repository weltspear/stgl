#version 150 core
in vec2 pos;
in vec4 color;

out vec4 colorf;

void main()
{
    gl_Position = vec4(pos, 0.0, 1.0);
    colorf = color;
}