#version 150 core
in vec4 pos; // one half will be for pos and the other for textpos
out vec2 texpos;

void main()
{
    gl_Position =  vec4(pos.x, pos.y, 0.0, 1.0);
    texpos = vec2(pos.z, pos.w);
}