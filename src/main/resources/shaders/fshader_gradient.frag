#version 150 core

in vec4 colorf;
out vec4 outColor;

void main() {
    outColor = colorf;
}