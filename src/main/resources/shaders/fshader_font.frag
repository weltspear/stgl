#version 150 core
in vec2 texpos;

uniform sampler2D text;
uniform vec3 textColor;

out vec4 outColor;

void main()
{
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, texpos).r);
    outColor = vec4(textColor, 1.0) * sampled;
}