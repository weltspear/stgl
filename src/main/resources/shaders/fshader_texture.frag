#version 150 core

in vec2 texposf;
out vec4 outColor;

uniform vec4 color;
uniform sampler2D tex;

void main() {
    outColor = color*texture(tex, texposf);
}