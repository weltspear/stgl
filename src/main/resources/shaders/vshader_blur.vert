#version 150 core
in vec2 pos;
in vec2 texpos;

out vec2 texposf;

void main()
{
    gl_Position = vec4(pos, 0.0, 1.0);
    texposf = texpos;
}