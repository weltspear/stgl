#version 150 core

in vec2 texposf;
flat in float _layer;

out vec4 outColor;

uniform vec4 color;
uniform sampler2DArray tex;

void main() {
    outColor = color*texture(tex, vec3(texposf, _layer));
}