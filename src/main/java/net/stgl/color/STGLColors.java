package net.stgl.color;

import net.stgl.Utils;
import org.joml.Vector4f;

import java.awt.*;

public class STGLColors {

    public final static Vector4f BLUE = Utils.colorToVec(Color.BLUE);
    public final static Vector4f RED = Utils.colorToVec(Color.RED);
    public final static Vector4f GREEN = Utils.colorToVec(Color.GREEN);
    public final static Vector4f CYAN = Utils.colorToVec(Color.CYAN);
    public final static Vector4f GRAY = Utils.colorToVec(Color.GRAY);
    public final static Vector4f WHITE = Utils.colorToVec(Color.WHITE);
    public final static Vector4f DARK_GRAY = Utils.colorToVec(Color.DARK_GRAY);
    public final static Vector4f LIGHT_GRAY = Utils.colorToVec(Color.LIGHT_GRAY);
    public final static Vector4f PINK = Utils.colorToVec(Color.PINK);
    public final static Vector4f ORANGE = Utils.colorToVec(Color.ORANGE);
    public final static Vector4f MAGENTA = Utils.colorToVec(Color.MAGENTA);
}
