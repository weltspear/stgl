package net.stgl.image;

import net.stgl.texture.Texture;
import org.jetbrains.annotations.NotNull;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

/***
 * Manages <code>Texture</code> object.
 * Image cannot be modified.
 */
public class STGLImage {
    private final ByteBuffer image;
    private final int width;
    private final int height;
    private final Texture.TextureOptions options;

    private Texture texture = null;

    public STGLImage(ByteBuffer image, int width, int height, Texture.TextureOptions options){
        this.image = image;
        this.width = width;
        this.height = height;
        this.options = options;
    }

    public STGLImage(ByteBuffer image, int width, int height){
        this.image = image;
        this.width = width;
        this.height = height;
        this.options = Texture.DEFAULT_TEXTURE_OPTIONS;
    }

    public STGLImage(BufferedImage bufferedImage){
        this(Texture.toByteBuffer(bufferedImage), bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    public STGLImage(BufferedImage bufferedImage, Texture.TextureOptions textureOptions){
        this(Texture.toByteBuffer(bufferedImage), bufferedImage.getWidth(), bufferedImage.getHeight(), textureOptions);
    }

    public boolean isAccelerated(){
        return texture != null;
    }

    /***
     * Deallocates OpenGL texture, it will be automatically reallocated on next <code>getTexture</code> call.
     * After using an image "drop" it to avoid leaks.
     */
    public void drop(){
        if (texture != null) {
            texture.dispose();
            texture = null;
        }
    }

    public @NotNull Texture getTexture(){
        if (texture == null){
            texture = new Texture(width, height, image, options);
        }
        return texture;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
