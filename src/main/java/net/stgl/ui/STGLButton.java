package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.ActionListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import org.joml.Vector4f;

public class STGLButton extends STGLComponent {

    private int width;
    private int height;
    private int x;
    private int y;
    private String text;
    private STGLFont font = null;
    private Vector4f fg = null;
    private Vector4f bg = null;
    private Vector4f fg_hover = null;
    private Vector4f bg_hover = null;

    private Vector4f fg_pressed = null;
    private Vector4f bg_pressed = null;

    private Vector4f fg_partially_disabled = null;

    private Vector4f bg_partially_disabled = null;

    private boolean isHover = false;
    private boolean isPressed = false;

    private ActionListener actionListener = null;

    private final STGLButtonMouseListener mouseListener = new STGLButtonMouseListener();

    public class STGLButtonMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }

            if (x > STGLButton.this.x && y > STGLButton.this.y &&
                    x < STGLButton.this.x+width && y < STGLButton.this.y+height){
                requestRepaint();
                isHover = true;
            }
            else{
                isHover = false;
                isPressed = false;
                requestRepaint();
            }
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLButton.this.x && y > STGLButton.this.y &&
                        x < STGLButton.this.x + width && y < STGLButton.this.y + height) {
                    isPressed = true;
                    requestRepaint();
                }
            }
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLButton.this.x && y > STGLButton.this.y &&
                        x < STGLButton.this.x + width && y < STGLButton.this.y + height) {
                    if (isPressed) {
                        isPressed = false;
                        requestRepaint();
                        if (actionListener != null) {
                            actionListener.action();
                        }
                    }
                }
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

    private final STGLImage icon;

    public STGLButton(int width, int height,
                      int x, int y, String text) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.text = text;
        this.icon = null;
    }

    public STGLButton(int width, int height,
                      int x, int y, STGLImage icon) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.text = null;
        this.icon = icon;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public void setHoverForegroundColor(Vector4f color){
        fg_hover = color;
    }

    public void setHoverBackgroundColor(Vector4f color){
        bg_hover = color;
    }

    public void setPressedForegroundColor(Vector4f color){
        fg_pressed = color;
    }

    public void setPressedBackgroundColor(Vector4f color){
        bg_pressed = color;
    }

    public void setPartiallyDisabledBackgroundColor(Vector4f color){
        bg_partially_disabled = color;
    }

    public void setPartiallyDisabledForegroundColor(Vector4f color){
        fg_partially_disabled = color;
    }

    private boolean isBorderDisabled = false;
    public void disableBorder(){
        isBorderDisabled = true;
    }

    private int x_align = -1;

    public void setXAlignment(int x){
        x_align = x;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        Vector4f current_fg_color = isPartiallyDisabled() ? fg_partially_disabled : isHover && !isPressed ? fg_hover : !isPressed ? fg :fg_pressed;
        Vector4f current_bg_color = isPartiallyDisabled() ? bg_partially_disabled : isHover && !isPressed ? bg_hover : !isPressed ? bg :bg_pressed;

        graphics.fillRect(x, y, width, height, current_bg_color);
        if (!isBorderDisabled)
            graphics.drawRect(x, y, width, height, current_fg_color);

        if (icon == null) {
            STGLFont.FontMetrics fontMetrics = font.getFontMetrics();
            int tx = (width - fontMetrics.getStringWidth(text)) / 2;
            int ty = (height - fontMetrics.getMaxCharHeight()) / 2;
            graphics.renderText(font, x + (x_align == -1 ? tx : x_align), y + ty + fontMetrics.getMaxCharHeight(), text, current_fg_color);
        } else{
            int tx = x + (x_align == -1 ? (width - icon.getWidth()) / 2  : x_align);
            int ty = y+(height - icon.getHeight()) / 2;
            System.out.println(tx);
            if (current_bg_color != bg){
                graphics.drawTexture(tx, ty, icon.getTexture(), current_bg_color);
            }
            else{
                graphics.drawImage(tx, ty, icon);
            }
        }
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(mouseListener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(mouseListener);
        if (icon != null)
            icon.drop();
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(mouseListener);
        });
        if (icon != null)
            icon.drop();
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(mouseListener);
        });
    }

    public void setActionListener(ActionListener actionListener){
        this.actionListener = actionListener;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
