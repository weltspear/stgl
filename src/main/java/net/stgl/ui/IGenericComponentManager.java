package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;

public interface IGenericComponentManager {

    void requestRepaint();

    void addMouseListener(MouseListener mouseListener);
    void addKeyboardListener(KeyboardListener keyboardListener);

    void removeMouseListener(MouseListener mouseListener);

    void removeKeyboardListener(KeyboardListener keyboardListener);

    void addComponent(STGLComponent component);

    void removeComponent(STGLComponent component);

    STGLGraphics getGraphics();

    void invokeLater(Runnable runnable);

    STGLComponent getPriorityComponent(int x, int y);
}
