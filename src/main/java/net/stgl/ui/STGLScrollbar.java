package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;
import org.joml.Vector4i;

import java.util.ArrayList;

public class STGLScrollbar extends STGLComponent{

    private final int width;
    private final int height;
    private final int scrollBarHeight;
    private int x;
    private int y;

    private final int maxScrollbarY;

    private int scrollbarY = 0;

    private float scrollBarSpeed = 1;
    private float scrollBarSpeedWheel = 1;

    private Vector4f fg;
    private Vector4f bg;

    private final ArrayList<Vector4i> mouseWheelAreas = new ArrayList<>();

    private class STGLScrollbarMouseListener implements MouseListener {

        private boolean doMouseWheel = false;
        private boolean mouseMovementActive = false;
        private int lastMouseY = -1;

        @Override
        public void mouseMoved(double x, double y) {
            doMouseWheel = x >= STGLScrollbar.this.x && y >= STGLScrollbar.this.y
                    && x <= STGLScrollbar.this.x + width && y <= STGLScrollbar.this.y + height;

            for (Vector4i area: mouseWheelAreas){
                if (area.x <= x && x <= area.x + area.z && area.y <= y && y <= area.y + area.w) {
                    doMouseWheel = true;
                    break;
                }
            }

            if (mouseMovementActive){
                int d_y = (int) (y- lastMouseY);
                lastMouseY = (int) y;
                scrollbarY += (int) (d_y*scrollBarSpeed);

                if (scrollbarY > maxScrollbarY){
                    scrollbarY = maxScrollbarY;
                }
                if (scrollbarY < 0){
                    scrollbarY = 0;
                }
                requestRepaint();
            }
        }

        @Override
        public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (x >= STGLScrollbar.this.x && y >= STGLScrollbar.this.y
                && x <= STGLScrollbar.this.x+width && y <= STGLScrollbar.this.y+height){
                mouseMovementActive = true;

                scrollbarY = (int) (y-STGLScrollbar.this.y)-scrollBarHeight/2;
                if (scrollbarY > maxScrollbarY){
                    scrollbarY = maxScrollbarY;
                }
                if (scrollbarY < 0){
                    scrollbarY = 0;
                }
                lastMouseY = (int) y;
                requestRepaint();
            }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
            mouseMovementActive = false;
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {
            if (doMouseWheel) {
                scrollbarY -= (int) (yoffset*3*scrollBarSpeedWheel);
                if (scrollbarY > maxScrollbarY) {
                    scrollbarY = maxScrollbarY;
                }
                if (scrollbarY < 0) {
                    scrollbarY = 0;
                }
                requestRepaint();
            }
        }
    }

    public STGLScrollbar(int x, int y, int width, int height, int scrollBarHeight){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.scrollBarHeight = scrollBarHeight;
        maxScrollbarY = height-4-scrollBarHeight;
    }

    @Override
    public void setFont(STGLFont f) {

    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        graphics.fillRect(x, y, width, height, bg);
        graphics.drawRect(x, y, width, height, fg);
        graphics.fillRect(x+2, y+2+ scrollbarY, width-4, scrollBarHeight, fg);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    private final STGLScrollbarMouseListener listener = new STGLScrollbarMouseListener();
    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(listener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(listener);
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(listener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(listener);
        });
    }

    public void setScrollBarSpeed(float scrollBarSpeed) {
        this.scrollBarSpeed = scrollBarSpeed;
    }

    public void setScrollBarSpeedWheel(float scrollBarSpeedWheel) {
        this.scrollBarSpeedWheel = scrollBarSpeedWheel;
    }

    public float getScrollBarProgress(){
        return ((float) scrollbarY) /((float) maxScrollbarY);
    }

    @Override
    public void partiallyDisable() {
        super.partiallyDisable();
    }

    /***
     * Adds an area in which scrollbar will respond to mouse wheel movement
     * @param area (x,y,width,height)
     */
    public void addMouseWheelArea(Vector4i area){
        mouseWheelAreas.add(area);
    }
}
