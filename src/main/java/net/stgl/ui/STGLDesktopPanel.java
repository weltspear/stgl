package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.ui.iframe.AbstractInternalFrame;
import net.stgl.ui.iframe.STGLInternalFrame;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.util.ArrayList;

public class STGLDesktopPanel extends STGLComponent{
    private int x;
    private int y;
    private final int width;
    private final int height;

    private boolean focusPause = false;

    private final STGLDesktopPanelMouseListener mouseListener = new STGLDesktopPanelMouseListener();
    private final STGLDesktopPanelKeyboardListener keyboardListener = new STGLDesktopPanelKeyboardListener();
    private STGLGraphics panelGraphics = null;
    private STGLGraphics transformedGraphics = null;

    private STGLDesktopPanelInternalFrameContainer cur_focused = null;
    private final ArrayList<STGLDesktopPanelInternalFrameContainer> containers = new ArrayList<>();

    public class STGLDesktopPanelInternalFrameContainer implements IGenericComponentManager {

        private final AbstractInternalFrame frame;
        private final ArrayList<MouseListener> mouseListeners = new ArrayList<>();
        private final ArrayList<KeyboardListener> keyboardListeners = new ArrayList<>();

        public STGLDesktopPanelInternalFrameContainer(AbstractInternalFrame frame){
            this.frame = frame;
        }

        @Override
        public void requestRepaint() {
             STGLDesktopPanel.this.requestRepaint();
        }

        @Override
        public void addMouseListener(MouseListener mouseListener) {
            mouseListeners.add(mouseListener);
        }

        @Override
        public void addKeyboardListener(KeyboardListener keyboardListener) {
            keyboardListeners.add(keyboardListener);
        }

        @Override
        public void removeMouseListener(MouseListener mouseListener) {
            mouseListeners.remove(mouseListener);
        }

        @Override
        public void removeKeyboardListener(KeyboardListener keyboardListener) {
            keyboardListeners.remove(keyboardListener);
        }

        @Override
        public void addComponent(STGLComponent component) {
            if (component.isPriorityComponent()){
                component.setX(STGLDesktopPanel.this.x+component.getX());
                component.setY(STGLDesktopPanel.this.y+component.getY());
                getComponentManager().addComponent(component);
            }
            else
                STGLDesktopPanel.this.addInternalFrame((AbstractInternalFrame) component);
        }

        @Override
        public void removeComponent(STGLComponent component) {
            if (!component.isPriorityComponent())
                STGLDesktopPanel.this.removeInternalFrame((AbstractInternalFrame) component);
            else
                getComponentManager().removeComponent(component);
        }

        @Override
        public STGLGraphics getGraphics() {
            return STGLDesktopPanel.this.getGraphics();
        }

        @Override
        public void invokeLater(Runnable runnable) {
            getComponentManager().invokeLater(runnable);
        }

        @Override
        public STGLComponent getPriorityComponent(int x, int y) {
            return getComponentManager().getPriorityComponent(x + STGLDesktopPanel.this.x,y + STGLDesktopPanel.this.y);
        }

        public int getWidth(){
            return width;
        }

        public int getHeight(){
            return height;
        }

        public int getX(){
            return x;
        }

        public int getY(){
            return y;
        }
    }

    public class STGLDesktopPanelMouseListener implements MouseListener {

        @Override
        public void mouseMoved(double x, double y) {
            for (STGLDesktopPanelInternalFrameContainer c : containers) {
                if (cur_focused == c) {
                    for (MouseListener m: c.mouseListeners) {
                        m.mouseMoved(x - STGLDesktopPanel.this.x, y - STGLDesktopPanel.this.y);
                    }
                }
            }
        }

        private STGLDesktopPanelInternalFrameContainer focusInternalFrame(int x, int y){
            if (focusPause){
                focusPause = false;
                return cur_focused;
            }
            if (cur_focused!=null){
                if (!cur_focused.frame.isMinimized()) {
                    if ((cur_focused.frame.getX() <= x - STGLDesktopPanel.this.x
                            && x - STGLDesktopPanel.this.x <= cur_focused.frame.getX() + cur_focused.frame.getWidth()
                            && cur_focused.frame.getY() <= y - STGLDesktopPanel.this.y
                            && y - STGLDesktopPanel.this.y <= cur_focused.frame.getY() + cur_focused.frame.getHeight())) {
                        cur_focused.frame.setFocused(true);
                        requestRepaint();
                        return cur_focused;
                    }

                    cur_focused.frame.setFocused(false);
                    requestRepaint();
                }
                cur_focused = null;
            }
            for (int i = containers.size()-1; i >= 0; i--){
                STGLDesktopPanelInternalFrameContainer cframe = containers.get(i);
                if (cframe.frame.isMinimized()){
                    continue;
                }
                if ((cframe.frame.getX() <= x - STGLDesktopPanel.this.x && x - STGLDesktopPanel.this.x <= cframe.frame.getX()+cframe.frame.getWidth() &&
                        cframe.frame.getY() <= y - STGLDesktopPanel.this.y && y - STGLDesktopPanel.this.y <= cframe.frame.getY()+cframe.frame.getHeight())){
                    containers.remove(cframe);
                    cur_focused = cframe;
                    cur_focused.frame.setFocused(true);
                    containers.add(cframe);
                    requestRepaint();
                    return cframe;
                }
            }
            return null;
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (!containers.isEmpty()) {
                if (cur_focused != null) {
                    boolean pr_m_state = cur_focused.frame.isMinimized() || cur_focused.frame.isClosed();

                    fc:
                    {
                        if ((cur_focused.frame.isMinimized() || cur_focused.frame.isClosed()) != pr_m_state) {
                            break fc;
                        }
                        STGLDesktopPanelInternalFrameContainer _cur_focused = cur_focused;
                        focusInternalFrame((int) x, (int) y);
                        if (cur_focused != null) {
                            for (MouseListener m: cur_focused.mouseListeners) {
                                m.mousePressed(button, glfw_mods, x - STGLDesktopPanel.this.x, y - STGLDesktopPanel.this.y);
                            }

                        } else {
                            _cur_focused.frame.setFocused(false);
                            requestRepaint();
                        }
                    }

                } else {
                    cur_focused = focusInternalFrame((int) x, (int) y);
                    if (cur_focused != null) {
                        for (MouseListener m: cur_focused.mouseListeners) {
                            m.mousePressed(button, glfw_mods, x - STGLDesktopPanel.this.x, y - STGLDesktopPanel.this.y);
                        }
                    }
                }

            }

            int m = 0;
            for (STGLDesktopPanelInternalFrameContainer component : containers) {
                if (component.frame.isMinimized()) {
                    int _x = STGLDesktopPanel.this.x + m * 32;
                    int _y = STGLDesktopPanel.this.y +  height - 32 - 6;

                    if (_x <= x && x <= _x + 32 && _y <= y && y <= _y + 32) {
                        component.frame.setMinimized(false);
                        requestRepaint();
                        System.out.println("Checking stuff");
                        break;
                    }
                    m++;
                }

            }


        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            if (!containers.isEmpty()) {
                if (cur_focused != null) {
                    boolean pr_m_state = cur_focused.frame.isMinimized() || cur_focused.frame.isClosed();

                    fc:
                    {
                        if ((cur_focused.frame.isMinimized() || cur_focused.frame.isClosed()) != pr_m_state) {
                            break fc;
                        }
                        STGLDesktopPanelInternalFrameContainer _cur_focused = cur_focused;
                        focusInternalFrame((int) x, (int) y);
                        if (cur_focused != null) {
                            for (MouseListener m: cur_focused.mouseListeners) {
                                m.mouseReleased(button, glfw_mods, x - STGLDesktopPanel.this.x, y - STGLDesktopPanel.this.y);
                            }

                        } else {
                            _cur_focused.frame.setFocused(false);
                            requestRepaint();
                        }
                    }

                } else {
                    cur_focused = focusInternalFrame((int) x, (int) y);
                    if (cur_focused != null) {
                        for (MouseListener m: cur_focused.mouseListeners) {
                            m.mouseReleased(button, glfw_mods, x - STGLDesktopPanel.this.x, y - STGLDesktopPanel.this.y);
                        }
                    }
                }

            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {
            for (STGLDesktopPanelInternalFrameContainer c : containers) {
                if (cur_focused == c) {
                    for (MouseListener m: c.mouseListeners) {
                        m.mouseWheelMoved(xoffset,yoffset);
                    }
                }
            }
        }
    }

    public class STGLDesktopPanelKeyboardListener implements KeyboardListener{

        @Override
        public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (cur_focused != null)
                if (cur_focused.frame.isMinimized()) {
                    cur_focused = null;
                }
            if (cur_focused != null) {
                for (KeyboardListener keyboardListener : cur_focused.keyboardListeners) {
                    keyboardListener.keyPressed(key, glfw_scancode, glfw_modifiers);
                }
            }
        }

        @Override
        public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (cur_focused != null)
                if (cur_focused.frame.isMinimized()) {
                    cur_focused = null;
                }
            if (cur_focused != null) {
                for (KeyboardListener keyboardListener : cur_focused.frame.getKeyboardListeners()) {
                    keyboardListener.keyReleased(key, glfw_scancode, glfw_modifiers);
                }
            }
        }

        @Override
        public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (cur_focused != null)
                if (cur_focused.frame.isMinimized()) {
                    cur_focused = null;
                }
            if (cur_focused != null) {
                for (KeyboardListener keyboardListener : cur_focused.keyboardListeners) {
                    keyboardListener.keyPressedRepeat(key, glfw_scancode, glfw_modifiers);
                }
            }
        }

        @Override
        public void charTyped(int codepoint) {
            if (cur_focused != null)
                if (cur_focused.frame.isMinimized()) {
                    cur_focused = null;
                }
            if (cur_focused != null) {
                for (KeyboardListener keyboardListener : cur_focused.keyboardListeners ) {
                    keyboardListener.charTyped(codepoint);
                }
            }
        }
    }

    public STGLDesktopPanel(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void addInternalFrame(@NotNull AbstractInternalFrame internalFrame) {
        STGLDesktopPanelInternalFrameContainer container = new STGLDesktopPanelInternalFrameContainer(internalFrame);
        containers.add(container);
        internalFrame.setComponentManager(container);
        internalFrame.initAdd();
        requestRepaint();
    }

    public void removeInternalFrame(@NotNull AbstractInternalFrame component) {
        STGLDesktopPanelInternalFrameContainer cc = null;
        for (STGLDesktopPanelInternalFrameContainer c: containers){
            if (c.frame == component){
                cc = c;
            }
        }

        containers.remove(cc);
        component.deinitRemove();
        if (cur_focused == cc){
            cur_focused = null;
        }
        requestRepaint();
    }

    public STGLGraphics getGraphics() {
        return panelGraphics;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(mouseListener);
        getComponentManager().addKeyboardListener(keyboardListener);
        validateInternalGraphics();
    }

    public void validateInternalGraphics(){
        transformedGraphics = getComponentManager().getGraphics().childGraphics();
        transformedGraphics.setTranslationVector(new Vector2i(x, y));
        transformedGraphics.defineScissorBox(x, y, width, height);
        panelGraphics = transformedGraphics.childGraphics();
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(mouseListener);
        getComponentManager().removeKeyboardListener(keyboardListener);
    }

    @Override
    public void disable() {
        super.disable();
        for (STGLDesktopPanelInternalFrameContainer c : containers){
            c.frame.disable();
        }
    }

    @Override
    public void enable() {
        super.enable();
        for (STGLDesktopPanelInternalFrameContainer c : containers){
            c.frame.enable();
        }
    }

    @Override
    public void setFont(STGLFont f) {

    }

    @Override
    public void setForeground(Vector4f color) {

    }

    @Override
    public void setBackground(Vector4f color) {

    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        validateInternalGraphics();
        if (cur_focused != null)
            if (cur_focused.frame.isMinimized()) {
                cur_focused = null;
            }
        int m = 0;
        ArrayList<STGLDesktopPanelInternalFrameContainer> cleanup = new ArrayList<>();
        ArrayList<STGLDesktopPanelInternalFrameContainer> minimized = new ArrayList<>();
        for (STGLDesktopPanelInternalFrameContainer component : containers) {
            if (component.frame.isClosed()) {
                cleanup.add(component);
                if (component == cur_focused) {
                    cur_focused = null;
                }
                continue;
            }

            if (!component.frame.isMinimized()) {
                if (cur_focused != component)
                    if (component.frame.isEnabled()) {
                        component.frame.paint(panelGraphics);
                    }
            } else {
                minimized.add(component);
            }
        }


        containers.removeAll(cleanup);

        if (cur_focused != null)
            cur_focused.frame.paint(panelGraphics);

        for (STGLDesktopPanelInternalFrameContainer c: minimized){
            panelGraphics.drawImageScaled(m * 32, height - 32 - 6, c.frame.getIcon(), 32, 32);
            m++;
        }
    }

    @Override
    public void update() {
        for (STGLDesktopPanelInternalFrameContainer container: containers){
            if (container.frame.isEnabled())
                container.frame.update();
        }
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void dispose(){
        panelGraphics.dispose();
        transformedGraphics.dispose();
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    public ArrayList<AbstractInternalFrame> getInternalFrames(){
        ArrayList<AbstractInternalFrame> frames = new ArrayList<>();
        for (STGLDesktopPanelInternalFrameContainer container: containers){
            frames.add(container.frame);
        }
        return frames;
    }

    public void focusFrame(AbstractInternalFrame frame){
        frame.setFocused(true);
        if (cur_focused != null)
            cur_focused.frame.setFocused(false);
        STGLDesktopPanelInternalFrameContainer cframe = null;
        for (STGLDesktopPanelInternalFrameContainer _cframe: containers){
            if (frame == _cframe.frame){
                cframe = _cframe;
                break;
            }
        }
        cur_focused = cframe;
        focusPause = true;
    }
}
