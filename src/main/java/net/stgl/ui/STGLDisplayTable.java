package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

import java.util.List;

public class STGLDisplayTable extends STGLComponent{

    private int x;
    private int y;
    private final int width;
    private final int height;
    private List<STGLMultiList.DefaultColumn> columns;
    private final int rowHeight;
    private final int headHeight;

    private STGLFont font;

    private Vector4f bg;
    private Vector4f fg;
    private Vector4f bg_2;
    private Vector4f bg_1;

    public STGLDisplayTable(List<STGLMultiList.DefaultColumn> columns, int x, int y, int width, int height,
                            int row_height, int head_height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.columns = columns;
        rowHeight = row_height;
        headHeight = head_height;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    public void setBackgroundA(Vector4f color){
        bg_1 = color;
    }

    public void setBackgroundB(Vector4f color){
        bg_2 = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        graphics.fillRect(x, y, width, height, bg);

        STGLGraphics child = graphics.childGraphics();
        child.defineScissorBox(x, y, width, height);

        int _x = 0;
        Vector4f prv = bg_2;
        for (STGLMultiList.DefaultColumn defaultColumn: columns) {
            int _y = headHeight;
            Vector4f cbg;
            if (prv == bg_2){
                cbg = bg_1;
            }
            else{
                cbg = bg_2;
            }
            prv = cbg;

            child.renderText(font, x+_x+4, y+_y-(headHeight- font.getHeight())/2, defaultColumn.getHead(), fg);

            int _idx = 0;
            while (_y < height) {
                child.fillRect(x+_x, y + _y,
                        defaultColumn.getColumnWidth() == -1 ? width:
                        defaultColumn.getColumnWidth(), rowHeight, cbg);
                if (cbg == bg_1) {
                    cbg = bg_2;
                } else {
                    cbg = bg_1;
                }
                _y += rowHeight;
                if (_idx < defaultColumn.getColumn().size()){
                    child.renderText(font, x+_x+4, _y+y-(rowHeight-font.getMaxHeight())/2, defaultColumn.getColumn().get(_idx), fg);
                }
                _idx += 1;
            }
            _x += defaultColumn.getColumnWidth();
        }
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {

    }

    @Override
    public void deinitRemove() {

    }

    public void setColumns(List<STGLMultiList.DefaultColumn> columns) {
        this.columns = columns;
    }
}
