package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.ActionListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import org.joml.Vector4f;

public class STGLIconButton extends STGLComponent {

    private final int width;
    private final int height;
    private int x;
    private int y;
    private Vector4f bg_hover = null;
    private Vector4f bg_pressed = null;

    private Vector4f bg_partially_disabled = null;

    private boolean isHover = false;
    private boolean isPressed = false;

    private ActionListener actionListener = null;

    private final STGLIconButtonMouseListener mouseListener = new STGLIconButtonMouseListener();

    public class STGLIconButtonMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (x > STGLIconButton.this.x && y > STGLIconButton.this.y &&
                    x < STGLIconButton.this.x+width && y < STGLIconButton.this.y+height){
                requestRepaint();
                isHover = true;
            }
            else{
                isHover = false;
                isPressed = false;
                requestRepaint();
            }
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLIconButton.this.x && y > STGLIconButton.this.y &&
                        x < STGLIconButton.this.x + width && y < STGLIconButton.this.y + height) {
                    isPressed = true;
                    requestRepaint();
                }
            }
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLIconButton.this.x && y > STGLIconButton.this.y &&
                        x < STGLIconButton.this.x + width && y < STGLIconButton.this.y + height) {
                    isPressed = false;
                    requestRepaint();
                    if (actionListener != null) {
                        actionListener.action();
                    }
                }
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

    private final STGLImage icon;

    public STGLIconButton(
                      int x, int y, STGLImage icon) {
        this.width = icon.getWidth();
        this.height = icon.getHeight();
        this.x = x;
        this.y = y;
        this.icon = icon;
    }

    @Override
    public void setFont(STGLFont f) {

    }

    @Override
    public void setForeground(Vector4f color) {

    }

    @Override
    public void setBackground(Vector4f color) {

    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public void setHoverBackgroundColor(Vector4f color){
        bg_hover = color;
    }

    public void setPressedBackgroundColor(Vector4f color){
        bg_pressed = color;
    }

    public void setPartiallyDisabledBackgroundColor(Vector4f color){
        bg_partially_disabled = color;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        Vector4f current_bg_color = isPartiallyDisabled() ? bg_partially_disabled : isHover && !isPressed ? bg_hover : !isPressed ? null : bg_pressed;

        int tx = x;
        int ty = y;
        if (current_bg_color != null) {
            graphics.drawTexture(tx, ty, icon.getTexture(), current_bg_color);
        } else {
            graphics.drawImage(tx, ty, icon);
        }

    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(mouseListener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(mouseListener);
        if (icon != null)
            icon.drop();
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(mouseListener);
        });
        if (icon != null)
            icon.drop();
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(mouseListener);
        });
    }

    public void setActionListener(ActionListener actionListener){
        this.actionListener = actionListener;
    }
}
