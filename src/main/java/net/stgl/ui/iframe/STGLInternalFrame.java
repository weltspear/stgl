package net.stgl.ui.iframe;

import net.stgl.STGLGraphics;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.TextureManager;
import net.stgl.ui.STGLClassicButton;
import net.stgl.ui.STGLPanel;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.util.Objects;

public class STGLInternalFrame extends AbstractInternalFrame {

    private Vector4f bg;

    private Vector4f border_color;

    private final STGLInternalFrameTopPanel topPanel;

    private STGLFont font;

    private boolean isFocused = false;

    private boolean isClosed = false;
    private boolean isMinimized = false;

    private final STGLPanel contentPanel;

    public void setIcon(STGLImage image) {
        topPanel.setIcon(image);
    }

    private Vector4f top_panel_fg;
    private Vector4f top_panel_bg;

    public class STGLInternalFrameTopPanel extends STGLPanel{
        private String title = "";
        private STGLImage icon;
        private STGLClassicButton close;
        private STGLClassicButton minimize;

        private STGLImage btn1;
        private STGLImage btn2;

        private Vector2i pos1;
        private Vector2i pos2;

        public STGLInternalFrameTopPanel(int x, int y, int width, int height) {
            super(x, y, width, height);
            try {
                btn1 = new STGLImage(TextureManager.loadBufferedImage("default/internalframe_btn_1.png"));

                pos1 = new Vector2i(width - 14 - 3, (getHeight() - 14) / 2);

                close = new STGLClassicButton(14+1, 14, pos1.x, pos1.y, btn1);
                close.setActionListener(()-> isClosed = true);
                close.setForeground(new Vector4f(0, 0, 0, 1));

                pos2 = new Vector2i(width - 14 * 2 - 5, (getHeight() - 14) / 2);

                btn2 = new STGLImage(TextureManager.loadBufferedImage("default/internalframe_btn_2.png"));
                minimize = new STGLClassicButton(14+1, 14, pos2.x, pos2.y, btn2);
                minimize.setForeground(new Vector4f(0, 0, 0, 1));
                minimize.setActionListener(() -> isMinimized = true);

                icon = new STGLImage(TextureManager.loadBufferedImage("default/internalframe.png"));
            } catch (Exception ignored){

            }
        }

        public void setTitle(String title){
            this.title = title;
        }

        public void setIcon(STGLImage icon){
            this.icon = icon;
        }

        @Override
        public void prePaint(STGLGraphics graphics) {
            super.prePaint(graphics);
            Vector4f _bg = isFocused ? top_panel_bg: new Vector4f(0.2f, 0.2f, 0.2f, 1);
            graphics.fillRectGradient(0, 0, getWidth(), getHeight(), _bg, new Vector4f(_bg).mul(4));
            graphics.drawRect(1, 1, getWidth()-1, getHeight(), border_color);
            graphics.drawRect(2, 1, getWidth()-3, getHeight(), border_color);
            graphics.drawLine(0, getHeight()-2, getWidth(), getHeight()-2, border_color, 2);
            if (!Objects.equals(title, ""))
                graphics.renderText(font, 14+font.getMaxWidth(), (getHeight()- font.getMaxHeight())/2+ font.getMaxHeight(), title, top_panel_fg);
            graphics.drawImageScaled(3, (getHeight()-16)/2, icon, 16, 16);
        }

        @Override
        public void initAdd() {
            super.initAdd();
            addComponent(close);
            addComponent(minimize);
        }

        @Override
        public void deinitRemove() {
            super.initAdd();
            removeComponent(close);
            removeComponent(minimize);
            btn1.drop();
            btn2.drop();
            icon.drop();
        }
    }

    public STGLInternalFrame(int x, int y, int width, int height, int topBarHeight) {
        super(x, y, width, height);
        this.topBarHeight = topBarHeight;
        topPanel = new STGLInternalFrameTopPanel(0,0,width, topBarHeight);
        contentPanel = new STGLPanel(0, topBarHeight, width, height-topBarHeight);
    }

    @Override
    public void setForeground(Vector4f color) {
        top_panel_fg = color;
    }

    public void setBorderColor(Vector4f color) {
        border_color = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    public void setTopPanelBackground(Vector4f color) {
        top_panel_bg = color;
    }

    private boolean minimizable = true;
    private boolean closable = true;

    public void setMinimizable(boolean minimizable){
        this.minimizable = minimizable;
        setCorrectButtonPositions();
    }

    public void setClosable(boolean closable){
        this.closable = closable;
        setCorrectButtonPositions();
    }

    private void setCorrectButtonPositions(){
        if (minimizable && !closable){
            topPanel.minimize.setX(topPanel.pos1.x);
            topPanel.minimize.setY(topPanel.pos1.y);
            topPanel.close.disable();
        }
        else if (!minimizable && closable){
            topPanel.close.setX(topPanel.pos1.x);
            topPanel.close.setY(topPanel.pos1.y);
            topPanel.minimize.disable();
        }
        else if (!minimizable){
            topPanel.minimize.disable();
            topPanel.close.disable();
        }
        else {
            topPanel.minimize.setX(topPanel.pos2.x);
            topPanel.minimize.setY(topPanel.pos2.y);
            topPanel.close.setX(topPanel.pos1.x);
            topPanel.close.setY(topPanel.pos1.y);

            topPanel.close.enable();
            topPanel.minimize.enable();
        }
    }

    public void setTitle(String title){
        topPanel.setTitle(title);
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);
        graphics.fillRect(0, 0, getWidth(), getHeight(), bg);
        graphics.drawRect(1, 0, getWidth()-1, getHeight()-1, border_color);
        graphics.drawRect(2, 0, getWidth()-3, getHeight()-2, border_color);
    }

    private final InternalFrameMouseListener iMouseListener = new InternalFrameMouseListener();

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(topPanel);
        addComponent(contentPanel);
        getComponentManager().addMouseListener(iMouseListener);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(topPanel);
        removeComponent(contentPanel);
        getComponentManager().removeMouseListener(iMouseListener);
    }

    /***
     * This method is internally used by <code>STGLDesktopPanel</code>, do not use this manually!
     */
    public void setFocused(boolean focus){
        this.isFocused = focus;
    }

    public boolean isMinimized() {
        return isMinimized;
    }
    public void setMinimized(boolean minimized) {
        isMinimized = minimized;
        isFocused = false;
        requestRepaint();
    }

    public @NotNull STGLImage getIcon(){
        return topPanel.icon;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    /***
     * @return Content panel of <code>STGLInternalFrame</code>. Add your components to it not to frame directly.
     */
    public STGLPanel getContentPanel(){
        return contentPanel;
    }
}
