package net.stgl.ui.iframe;

import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.image.STGLImage;
import net.stgl.ui.STGLDesktopPanel;
import net.stgl.ui.STGLPanel;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractInternalFrame extends STGLPanel {

    protected int topBarHeight;

    public AbstractInternalFrame(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    /***
     * Called by STGLDesktopPanel to signalize that InternalFrame is focused. Should not be called manually.
     */
    public abstract void setFocused(boolean focus);

    public abstract boolean isClosed();
    public abstract boolean isMinimized();
    public abstract void setMinimized(boolean minimized);

    public abstract @NotNull STGLImage getIcon();

    public abstract void setClosable(boolean closable);
    public abstract void setMinimizable(boolean minimizable);

    /***
     * Implements basic internal frame movement functionality
     */
    public class InternalFrameMouseListener implements MouseListener {

        private boolean isMoving = false;
        private int origin_x = -1;
        private int origin_y = -1;

        @Override
        public void mouseMoved(double x, double y) {
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                isMoving = false;
                return;
            }
            if (isMoving){
                int delta_x = (int) (x-origin_x);
                int delta_y = (int) (y-origin_y);
                STGLDesktopPanel.STGLDesktopPanelInternalFrameContainer container =
                        (STGLDesktopPanel.STGLDesktopPanelInternalFrameContainer) getComponentManager();

                // do not allow moving out of desktop panel
                if (getX()+delta_x+getWidth() < container.getWidth() && getX()+delta_x >= 0) {
                    setX(getX() + delta_x);
                }
                if (getY()+delta_y+getHeight() < container.getHeight() && getY()+delta_y >= 0) {
                    setY(getY() + delta_y);
                }

                validateInternalGraphics();
                requestRepaint();
                origin_x = (int) x;
                origin_y = (int) y;
            }
        }

        @Override
        public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }
            if (mouseButton == MousePress.MOUSE_BUTTON_1)
                if ((AbstractInternalFrame.this.getX() <= x && x <= AbstractInternalFrame.this.getX() + AbstractInternalFrame.this.getWidth())
                        && (AbstractInternalFrame.this.getY() <= y && y <= AbstractInternalFrame.this.getY() + topBarHeight)) {
                    isMoving = true;
                    origin_x = (int) x;
                    origin_y = (int) y;
                }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
            if (isMoving){
                isMoving = false;
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

}
