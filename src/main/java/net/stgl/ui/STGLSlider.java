package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

public class STGLSlider extends STGLComponent{
    private final int width;
    private final int height;
    private int x;
    private int y;

    private final int maxSliderX;

    private int sliderX = 0;

    private float sliderSpeed = 1;
    private float sliderSpeedWheel = 1;

    private Vector4f fg;
    private Vector4f bg;

    private final int r;

    private class STGLSliderMouseListener implements MouseListener {

        private boolean doMouseWheel = false;
        private boolean mouseMovementActive = false;
        private int lastMouseX = -1;

        @Override
        public void mouseMoved(double x, double y) {
            doMouseWheel = x >= STGLSlider.this.x && y >= STGLSlider.this.y
                    && x <= STGLSlider.this.x + width && y <= STGLSlider.this.y + height;

            if (mouseMovementActive){
                int d_x = (int) (x- lastMouseX);
                lastMouseX = (int) x;
                sliderX += (int) (d_x* sliderSpeed);

                if (sliderX > maxSliderX){
                    sliderX = maxSliderX;
                }
                if (sliderX < 0){
                    sliderX = 0;
                }
                requestRepaint();
            }
        }

        @Override
        public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (x >= STGLSlider.this.x && y >= STGLSlider.this.y
                    && x <= STGLSlider.this.x+width && y <= STGLSlider.this.y+height){
                mouseMovementActive = true;

                sliderX = (int) (x-STGLSlider.this.x)-r;
                if (sliderX > maxSliderX){
                    sliderX = maxSliderX;
                }
                if (sliderX < 0){
                    sliderX = 0;
                }
                lastMouseX = (int) x;
                requestRepaint();
            }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
            mouseMovementActive = false;
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {
            if (doMouseWheel) {
                sliderX += (int) (yoffset*3* sliderSpeedWheel);
                if (sliderX > maxSliderX) {
                    sliderX = maxSliderX;
                }
                if (sliderX < 0) {
                    sliderX = 0;
                }
                requestRepaint();
            }
        }
    }

    public STGLSlider(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        r = height/2;
        maxSliderX = width-r*2;
    }

    @Override
    public void setFont(STGLFont f) {

    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        graphics.drawLine(x, y+height/2, x+width, y+height/2, fg, 3);
        graphics.fillCircle(sliderX +x+r, y+height/2, 20, r, fg);
        graphics.fillCircle(sliderX +x+r, y+height/2, 20, (int) (r*0.7), bg);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    private final STGLSliderMouseListener listener = new STGLSliderMouseListener();
    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(listener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(listener);
    }

    public void setSliderSpeed(float sliderSpeed) {
        this.sliderSpeed = sliderSpeed;
    }

    public void setSliderSpeedWheel(float sliderSpeedWheel) {
        this.sliderSpeedWheel = sliderSpeedWheel;
    }

    public float getSliderProgress(){
        return ((float) sliderX) /((float) maxSliderX);
    }

    @Override
    public void partiallyDisable() {
        super.partiallyDisable();
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(listener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(listener);
        });
    }
}
