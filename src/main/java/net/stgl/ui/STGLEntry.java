package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

public class STGLEntry extends STGLComponent{

    private int x;
    private int y;
    private final int width;
    private final int height;
    private final String hint;

    private StringBuilder text = new StringBuilder();

    private STGLFont font;

    private Vector4f fg = null;
    private Vector4f bg = null;

    private Vector4f active_fg = null;
    private Vector4f active_bg = null;

    private Vector4f partially_disabled_fg = null;
    private Vector4f partially_disabled_bg = null;

    private boolean isActive = false;
    private boolean showHint = true;

    private final MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseMoved(double x, double y) {

        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (x > STGLEntry.this.x && y > STGLEntry.this.y &&
                    x < STGLEntry.this.x + width && y < STGLEntry.this.y + height){
                isActive = true;
                showHint = false;
                requestRepaint();
            }
            else{
                isActive = false;
                requestRepaint();
            }
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {

        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    };

    private final KeyboardListener entryKeyboardListener = new KeyboardListener() {
        @Override
        public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (isPartiallyDisabled()) {
                return;
            }

            if (isActive)
                if (key == KeyPress.KEY_BACKSPACE) {
                    if (!text.isEmpty()) {
                        text.deleteCharAt(text.length() - 1);
                        requestRepaint();
                    }
                }
        }

        @Override
        public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {

        }

        @Override
        public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (isPartiallyDisabled()) {
                return;
            }

            if (isActive)
                if (key == KeyPress.KEY_BACKSPACE) {
                    if (!text.isEmpty()) {
                        text.deleteCharAt(text.length() - 1);
                        requestRepaint();
                    }
                }
        }

        @Override
        public void charTyped(int codepoint) {
            if (isPartiallyDisabled()){
                return;
            }

            if (isActive) {
                text.append(Utils.unicodeCodepointToString(codepoint));
                requestRepaint();
            }
        }
    };

    public STGLEntry(int x, int y, int width, int height, String hint){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.hint = hint;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        Vector4f current_bg = isPartiallyDisabled() ? partially_disabled_bg: isActive ? active_bg: bg;
        Vector4f current_fg = isPartiallyDisabled() ? partially_disabled_fg: isActive ? active_fg: fg;

        graphics.fillRect(x, y, width, height, current_bg);
        graphics.drawRect(x, y, width, height, current_fg);
        STGLFont.FontMetrics fontMetrics = font.getFontMetrics();
        int ty = (height-fontMetrics.getMaxCharHeight())/2+fontMetrics.getMaxCharHeight();
        if (showHint){
            graphics.renderText(font, x+3, y+ty, hint, current_fg);
        }
        else if (isActive){
            int mchars = (width/fontMetrics.getStringWidth(" "))-3;
            if (text.toString().length() < mchars) {
                graphics.renderText(font, x + 3, y + ty, text.toString(), current_fg);
                graphics.fillRect(x + fontMetrics.getStringWidth(text.toString())+3, y + ty - fontMetrics.getMaxCharHeight(), fontMetrics.getCharWidth(' ') / 2, fontMetrics.getMaxCharHeight(), current_fg);
            }else{
                graphics.renderText(font, x + 3, y + ty, text.substring(0, mchars-1)+"...", current_fg);
            }
        }
        else{
            graphics.renderText(font, x+3, y+ty, text.toString(), current_fg);
        }
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(mouseListener);
        getComponentManager().addKeyboardListener(entryKeyboardListener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(mouseListener);
        getComponentManager().removeKeyboardListener(entryKeyboardListener);
    }

    @Override
    public void partiallyDisable() {
        super.partiallyDisable();
        isActive = false;
    }

    public void setActiveForegroundColor(Vector4f active_fg) {
        this.active_fg = active_fg;
    }

    public void setActiveBackgroundColor(Vector4f active_bg) {
        this.active_bg = active_bg;
    }

    public void setPartiallyDisabledForegroundColor(Vector4f partially_disabled_fg) {
        this.partially_disabled_fg = partially_disabled_fg;
    }

    public void setPartiallyDisabledBackgroundColor(Vector4f partially_disabled_bg) {
        this.partially_disabled_bg = partially_disabled_bg;
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(mouseListener);
            getComponentManager().removeKeyboardListener(entryKeyboardListener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(mouseListener);
            getComponentManager().addKeyboardListener(entryKeyboardListener);
        });
    }

    public String getText(){
        return text.toString();
    }

    public void setText(StringBuilder text) {
        this.text = text;
        showHint = false;
    }
}
