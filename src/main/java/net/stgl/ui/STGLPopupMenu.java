package net.stgl.ui;

import net.stgl.event.ActionListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

public class STGLPopupMenu extends STGLPanel{
    private final ArrayList<STGLButton> buttons = new ArrayList<>();

    public static class PopupMenuEntry{
        private final String text;
        private final ActionListener action;

        public PopupMenuEntry(String text, ActionListener action){
            this.text = text;
            this.action = action;
        }
    }

    public STGLPopupMenu(int x, int y, STGLFont font,
                         List<PopupMenuEntry> entries){
        super(x,y,0,0);

        STGLFont.FontMetrics fontMetrics = font.getFontMetrics();

        int _y = 0;
        int _xmax = 0;

        for (PopupMenuEntry entry: entries){
            STGLButton button = new STGLButton(fontMetrics.getStringWidth(entry.text)
                    +fontMetrics.getMaxCharWidth()*2,
                    (int) (fontMetrics.getMaxCharHeight()*1.5f), 0, _y, entry.text);

            button.setActionListener(() -> {
                getComponentManager().invokeLater(() -> {
                    if (getComponentManager() != null) getComponentManager().removeComponent(STGLPopupMenu.this);
                });
                entry.action.action();
            });
            button.setFont(font);
            button.disableBorder();
            button.setXAlignment(fontMetrics.getMaxCharWidth());
            _y += (int) (fontMetrics.getMaxCharHeight()*1.5f);
            buttons.add(button);

            if (fontMetrics.getStringWidth(entry.text)
                    +fontMetrics.getMaxCharWidth()*2 > _xmax){
                _xmax = fontMetrics.getStringWidth(entry.text)
                        +fontMetrics.getMaxCharWidth()*2;
            }
        }

        for (STGLButton button: buttons){
            button.setWidth(_xmax);
        }
        setHeight(_y);
        setWidth(_xmax);
    }

    @Override
    public void setFont(STGLFont f) {
        throw new RuntimeException("Cannot change STGLPopupMenu's font");
    }

    @Override
    public boolean isPriorityComponent() {
        return true;
    }

    @Override
    public void setForeground(Vector4f color) {
        for (STGLButton button: buttons){
            button.setForeground(color);
        }
        super.setForeground(color);
    }

    private final STGLPopupMenuMouseListener mouseListener = new STGLPopupMenuMouseListener();
    @Override
    public void initAdd() {
        super.initAdd();

        for (STGLButton button: buttons){
            addComponent(button);
        }
        getComponentManager().addMouseListener(mouseListener);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();

        for (STGLButton button: buttons){
            removeComponent(button);
        }
        getComponentManager().removeMouseListener(mouseListener);
    }

    @Override
    public void setBackground(Vector4f color) {
        for (STGLButton button: buttons){
            button.setBackground(color);
        }
        super.setBackground(color);
    }
    public void setHoverForegroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setHoverForegroundColor(color);
        }
    }

    public void setHoverBackgroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setHoverBackgroundColor(color);
        }
    }

    public void setPressedForegroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setPressedForegroundColor(color);
        }
    }

    public void setPressedBackgroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setPressedBackgroundColor(color);
        }
    }

    public void setPartiallyDisabledBackgroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setPartiallyDisabledBackgroundColor(color);
        }
    }

    public void setPartiallyDisabledForegroundColor(Vector4f color){
        for (STGLButton button: buttons){
            button.setPartiallyDisabledForegroundColor(color);
        }
    }

    private class STGLPopupMenuMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {

        }

        @Override
        public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
            if (mouseButton == MousePress.MOUSE_BUTTON_2){
                getComponentManager().invokeLater(() -> getComponentManager().removeComponent(STGLPopupMenu.this));
                return;
            }

            checkWhetherToRemove(x, y);
        }

        private void checkWhetherToRemove(double x, double y) {
            if (!(STGLPopupMenu.this.getX()<=x && x<STGLPopupMenu.this.getX()+STGLPopupMenu.this.getWidth()
                && STGLPopupMenu.this.getY()<=y && y<STGLPopupMenu.this.getY()+STGLPopupMenu.this.getHeight())
                    || getComponentManager().getPriorityComponent((int) x, (int) y) != STGLPopupMenu.this){
                getComponentManager().invokeLater(() -> {
                    if (getComponentManager() != null) getComponentManager().removeComponent(STGLPopupMenu.this);
                });
            }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
            checkWhetherToRemove(x, y);
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }
}
