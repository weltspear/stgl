package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

/***
 * Handle mouse scroll event
 */
public class STGLPanel extends STGLComponent implements IGenericComponentManager{
    private int x;
    private int y;
    private int width;
    private int height;

    private final ArrayList<MouseListener> mouseListeners = new ArrayList<>();

    private final ArrayList<KeyboardListener> keyboardListeners = new ArrayList<>();

    private final STGLPanelRedirectMouseListener mouseListener = new STGLPanelRedirectMouseListener();

    private final ArrayList<STGLComponent> components = new ArrayList<>();

    private STGLGraphics panelGraphics = null;
    private STGLGraphics transformedGraphics = null;

    public class STGLPanelRedirectMouseListener implements MouseListener {

        @Override
        public void mouseMoved(double x, double y) {
            for (MouseListener mouseListener : mouseListeners) {
                mouseListener.mouseMoved(x - STGLPanel.this.x, y - STGLPanel.this.y);
            }
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            for (MouseListener mouseListener : mouseListeners) {
                mouseListener.mousePressed(button, glfw_mods, x - STGLPanel.this.x, y - STGLPanel.this.y);
            }
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            for (MouseListener mouseListener : mouseListeners) {
                mouseListener.mouseReleased(button, glfw_mods, x - STGLPanel.this.x, y - STGLPanel.this.y);
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {
            for (MouseListener mouseListener : mouseListeners) {
                mouseListener.mouseWheelMoved(xoffset, yoffset);
            }
        }
    }

    public STGLPanel(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public void addMouseListener(MouseListener mouseListener) {
        mouseListeners.add(mouseListener);
    }

    @Override
    public void addKeyboardListener(KeyboardListener keyboardListener) {
        invokeAfterInitialization(() -> {
            getComponentManager().addKeyboardListener(keyboardListener);
        });
        keyboardListeners.add(keyboardListener);
    }

    @Override
    public void removeMouseListener(MouseListener mouseListener) {
        mouseListeners.remove(mouseListener);
    }

    @Override
    public void removeKeyboardListener(KeyboardListener keyboardListener) {
        invokeAfterInitialization(() -> {
            getComponentManager().removeKeyboardListener(keyboardListener);
        });
        keyboardListeners.remove(keyboardListener);
    }

    @Override
    public void addComponent(STGLComponent component) {
        if (component.isPriorityComponent()){
            component.setX(this.x+component.getX());
            component.setY(this.y+component.getY());
            invokeAfterInitialization(() -> {
                getComponentManager().addComponent(component);
            });
        }
        else {
            components.add(component);
            component.setComponentManager(this);
            component.initAdd();

            for (var r: component.getRunnablesToBeInvokedAfterInitialization())
                r.run();

            component.getRunnablesToBeInvokedAfterInitialization().clear();

            requestRepaint();
        }
    }

    @Override
    public void removeComponent(STGLComponent component) {
        if (!component.isPriorityComponent()) {
            components.remove(component);
            component.setComponentManager(this);
            component.deinitRemove();
            requestRepaint();
        }
        else{
            getComponentManager().removeComponent(component);
        }
    }

    @Override
    public STGLGraphics getGraphics() {
        return panelGraphics;
    }

    @Override
    public void invokeLater(Runnable runnable) {
        invokeAfterInitialization(() -> {
            getComponentManager().invokeLater(runnable);
        });
    }

    @Override
    public STGLComponent getPriorityComponent(int x, int y) {
        return getComponentManager().getPriorityComponent(x + STGLPanel.this.x,y + STGLPanel.this.y);
    }

    @Override
    public void initAdd() {
        for (KeyboardListener keyboardListener: keyboardListeners){
            getComponentManager().addKeyboardListener(keyboardListener);
        }
        getComponentManager().addMouseListener(mouseListener);
        validateInternalGraphics();
    }

    public void validateInternalGraphics(){
        transformedGraphics = getComponentManager().getGraphics().childGraphics();
        transformedGraphics.setTranslationVector(new Vector2i(x, y));
        transformedGraphics.defineScissorBox(x, y, width, height);
        panelGraphics = transformedGraphics.childGraphics();
    }

    @Override
    public void deinitRemove() {
        for (KeyboardListener keyboardListener: keyboardListeners){
            getComponentManager().removeKeyboardListener(keyboardListener);
        }
        getComponentManager().removeMouseListener(mouseListener);
    }

    @Override
    public void disable() {
        super.disable();
        for (STGLComponent component : components){
            component.disable();
        }
    }

    @Override
    public void enable() {
        super.enable();
        for (STGLComponent component : components){
            component.enable();
        }
    }

    @Override
    public void setFont(STGLFont f) {

    }

    @Override
    public void setForeground(Vector4f color) {

    }

    @Override
    public void setBackground(Vector4f color) {

    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        validateInternalGraphics();
        prePaint(panelGraphics);
        for (STGLComponent component: components){
            if (component.isEnabled()){
                component.paint(panelGraphics);
            }
        }
    }

    @Override
    public void update() {
        for (STGLComponent component: components){
            if (component.isEnabled())
                component.update();
        }
    }

    /***
     * @param graphics framebufferGraphics of this panel
     */
    public void prePaint(STGLGraphics graphics){

    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void dispose(){
        panelGraphics.dispose();
        transformedGraphics.dispose();
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    public ArrayList<MouseListener> getMouseListeners() {
        return mouseListeners;
    }

    public ArrayList<KeyboardListener> getKeyboardListeners() {
        return keyboardListeners;
    }

    public STGLPanelRedirectMouseListener getRedirectMouseListener() {
        return mouseListener;
    }

    public List<STGLComponent> getComponents(){
        return components;
    }
}
