package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.ActionListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import org.jetbrains.annotations.Nullable;
import org.joml.Vector4f;

public class STGLCheckbox extends STGLComponent{
    private int x;
    private int y;
    private final String text;
    private STGLFont font;
    private Vector4f bg;
    private Vector4f fg;

    private Vector4f fg_partially_disabled = null;

    private Vector4f bg_partially_disabled = null;

    private final STGLImage yes;
    private final STGLImage no;
    private boolean check = false;

    private final CheckBoxMouseListener checkBoxMouseListener = new CheckBoxMouseListener();

    private ActionListener onCheckActionListener = null;

    public class CheckBoxMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {

        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (STGLCheckbox.this.x < x && STGLCheckbox.this.y < y &&
                        STGLCheckbox.this.x + getWidth() > x && STGLCheckbox.this.y + getHeight() > y) {
                    check = !check;
                    if (onCheckActionListener != null)
                        onCheckActionListener.action();
                    requestRepaint();
                }
            }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {

        }


        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

    public STGLCheckbox(int x, int y, String text, STGLImage yes, STGLImage no) {
        this.x = x;
        this.y = y;
        this.text = text;
        this.yes = yes;
        this.no = no;
    }

    private STGLImage yes_partially_disabled = null;
    private STGLImage no_partially_disabled = null;

    public void setPartiallyDisabledTextures(STGLImage yes, STGLImage no){
        yes_partially_disabled = yes;
        no_partially_disabled = no;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {
        bg = color;
    }

    public void setPartiallyDisabledBackgroundColor(Vector4f color){
        bg_partially_disabled = color;
    }

    public void setPartiallyDisabledForegroundColor(Vector4f color){
        fg_partially_disabled = color;
    }

    @Override
    public int getWidth() {
        return font.getFontMetrics().getStringWidth(text)+16+4;
    }

    @Override
    public int getHeight() {
        return Math.max(16, font.getFontMetrics().getMaxCharHeight());
    }

    @Override
    public void paint(STGLGraphics graphics) {
        Vector4f current_bg = isPartiallyDisabled() ? bg_partially_disabled : bg;
        Vector4f current_fg = isPartiallyDisabled() ? fg_partially_disabled : fg;

        graphics.fillRect(x, y, getWidth(), getHeight(), current_bg);

        if (check)
            graphics.drawImage(x, y, isPartiallyDisabled() && yes_partially_disabled != null ? yes_partially_disabled: yes);
        else graphics.drawImage(x, y, isPartiallyDisabled() && no_partially_disabled != null ? no_partially_disabled: no);

        graphics.renderText(font, x+16+4, y+font.getFontMetrics().getMaxCharHeight(), text, current_fg);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(checkBoxMouseListener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(checkBoxMouseListener);
        yes.drop();
        no.drop();
        if (yes_partially_disabled != null)
            yes_partially_disabled.drop();
        if (no_partially_disabled != null)
            no_partially_disabled.drop();
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(checkBoxMouseListener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(checkBoxMouseListener);
        });
    }

    public boolean isChecked(){
        return check;
    }

    public void setCheckActionListener(@Nullable ActionListener actionListener){
        onCheckActionListener = actionListener;
    }

    public void setChecked(boolean check) {
        this.check = check;
    }
}
