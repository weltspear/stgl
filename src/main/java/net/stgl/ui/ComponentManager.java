package net.stgl.ui;

import net.stgl.BlendUtils;
import net.stgl.Framebuffer;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.WindowResizeListener;
import org.jetbrains.annotations.Nullable;
import org.joml.Vector4f;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class ComponentManager implements IGenericComponentManager {
    private final MainFrame frame;
    private final ArrayList<STGLComponent> components = new ArrayList<>();
    private final ArrayList<STGLComponent> priorityComponents = new ArrayList<>();

    private final Framebuffer mainFrameBuffer;

    private final Framebuffer secondaryFrameBuffer;

    private final STGLGraphics componentFrameBufferGraphics;

    private final ArrayDeque<Runnable> invokeLaterStuff = new ArrayDeque<>();

    private boolean isRepaintRequested = true;

    private boolean directDrawing = false;

    private STGLGraphics targetGraphics;

    private final WindowResizeListener windowResizeListener = new WindowResizeListener() {
        @Override
        public void windowResize(int width, int height) {
            if (mainFrameBuffer != null){
                requestRepaint();
                mainFrameBuffer.recreateFramebuffer(width, height);
            }
            if (secondaryFrameBuffer != null){
                requestRepaint();
                secondaryFrameBuffer.recreateFramebuffer(width, height);
            }
        }
    };

    public ComponentManager(MainFrame frame, STGLGraphics targetGraphics){
        this.frame = frame;
        this.targetGraphics = targetGraphics;

        if (targetGraphics.getFramebuffer() == null)
            mainFrameBuffer = new Framebuffer(frame.getWidth(), frame.getHeight());
        else
            mainFrameBuffer = new Framebuffer(targetGraphics.getFramebuffer().getWidth(), targetGraphics.getFramebuffer().getHeight());

        componentFrameBufferGraphics = new STGLGraphics(mainFrameBuffer);
        secondaryFrameBuffer = null;

        frame.addWindowResizeListener(windowResizeListener);
    }

    public ComponentManager(MainFrame frame){
        this(frame, frame.getGraphics());
    }

    public ComponentManager(MainFrame frame, int samples, STGLGraphics targetGraphics){
        this.frame = frame;
        this.targetGraphics = targetGraphics;

        if (targetGraphics.getFramebuffer() == null)
            mainFrameBuffer = new Framebuffer(frame.getWidth(), frame.getHeight(), true, samples);
        else
            mainFrameBuffer = new Framebuffer(targetGraphics.getFramebuffer().getWidth(), targetGraphics.getFramebuffer().getHeight(), true, samples);

        componentFrameBufferGraphics = new STGLGraphics(mainFrameBuffer);
        secondaryFrameBuffer = new Framebuffer(frame.getWidth(), frame.getHeight());
    }

    public ComponentManager(MainFrame frame, int samples){
        this(frame, samples, frame.getGraphics());
    }

    public void addComponent(STGLComponent component){
        if (!component.isPriorityComponent()) {
            components.add(component);
        }
        else{
            priorityComponents.add(component);
        }
        component.setComponentManager(this);
        component.initAdd();

        for (Runnable r: component.getRunnablesToBeInvokedAfterInitialization()){
            r.run();
        }

        component.getRunnablesToBeInvokedAfterInitialization().clear();
        requestRepaint();
    }

    public void removeComponent(STGLComponent component){
        if (!component.isPriorityComponent())
            components.remove(component);
        else
            priorityComponents.remove(component);
        component.deinitRemove();
        component.setComponentManager(null);
        requestRepaint();
    }

    @Override
    public STGLGraphics getGraphics() {
        return directDrawing ? targetGraphics: componentFrameBufferGraphics;
    }

    @Override
    public void invokeLater(Runnable runnable) {
        invokeLaterStuff.add(runnable);
    }

    @Override
    public @Nullable STGLComponent getPriorityComponent(int x, int y) {
        if (!priorityComponents.isEmpty()) {
            for (int i = priorityComponents.size()-1; i >= 0; i--) {
                STGLComponent component = priorityComponents.get(i);
                if (component.getX()<=x &&
                        x<component.getX()+component.getWidth() &&
                        component.getY()<=y &&
                        y<component.getY()+component.getHeight()){
                    return component;
                }
            }
        }
        return null;
    }

    /***
     * If this is called framebuffers will be disposed and all components will be directly drawn
     * to main framebuffer
     */
    public void enableDirectDrawing(){
        mainFrameBuffer.dispose();
        if (secondaryFrameBuffer != null)
            secondaryFrameBuffer.dispose();
        componentFrameBufferGraphics.dispose();
        directDrawing = true;
    }

    /***
     * Paints and updates enabled components.
     * Automatically fixes blending if drawn to <code>Framebuffer</code> other than default one
     */
    public void paint(){
        if (!directDrawing) {
            if (isRepaintRequested) {
                BlendUtils.setCorrectFBOBlending();
                componentFrameBufferGraphics.clear();
                for (STGLComponent component : components) {
                    if (component.isEnabled()) {
                        component.update();
                        component.paint(componentFrameBufferGraphics);
                    }
                }
                for (STGLComponent component : priorityComponents) {
                    if (component.isEnabled()) {
                        component.update();
                        component.paint(componentFrameBufferGraphics);
                    }
                }
                BlendUtils.setDefaultBlending();

                // automatically fixes blending
                if (targetGraphics.getFramebuffer() != null){
                    BlendUtils.setCorrectFBOBlending();
                }
            }

            if (secondaryFrameBuffer != null) {
                if (isRepaintRequested)
                    mainFrameBuffer.blitThisToOtherFrameBuffer(secondaryFrameBuffer.getId());
                targetGraphics.drawFramebufferTexture(0, 0, secondaryFrameBuffer, new Vector4f(1, 1, 1, 1), true);
            } else {
                targetGraphics.drawFramebufferTexture(0, 0, mainFrameBuffer, new Vector4f(1, 1, 1, 1), true);
            }

            isRepaintRequested = false;
        }
        else {
            for (STGLComponent component : components) {
                // automatically fixes blending #2
                if (targetGraphics.getFramebuffer() != null){
                    BlendUtils.setCorrectFBOBlending();
                }

                if (component.isEnabled())
                    component.paint(targetGraphics);
            }
        }

        // shouldn't really be here
        while(!invokeLaterStuff.isEmpty()){
            invokeLaterStuff.pop().run();
        }

        // automatically fixes blending #3
        BlendUtils.setDefaultBlending();
    }

    @Override
    public void requestRepaint() {
        isRepaintRequested = true;
    }

    public void dispose(){
        if (!directDrawing) {
            mainFrameBuffer.dispose();
            if (secondaryFrameBuffer != null)
                secondaryFrameBuffer.dispose();
            componentFrameBufferGraphics.dispose();
        }
        frame.removeWindowResizeListener(windowResizeListener);
    }


    @Override
    public void addMouseListener(MouseListener mouseListener) {
        frame.addMouseListener(mouseListener);
    }

    @Override
    public void addKeyboardListener(KeyboardListener keyboardListener) {
        frame.addKeyboardListener(keyboardListener);
    }

    @Override
    public void removeMouseListener(MouseListener mouseListener) {
        frame.removeMouseListener(mouseListener);
    }

    @Override
    public void removeKeyboardListener(KeyboardListener keyboardListener) {
        frame.removeKeyboardListener(keyboardListener);
    }

    /***
     * This will not resize <code>ComponentManager</code>'s buffers
     */
    public void setTargetGraphics(STGLGraphics targetGraphics){
        this.targetGraphics = targetGraphics;
    }

    public List<STGLComponent> getComponents(){
        ArrayList<STGLComponent> comps = new ArrayList<>(components);
        comps.addAll(priorityComponents);
        return comps;
    }
}
