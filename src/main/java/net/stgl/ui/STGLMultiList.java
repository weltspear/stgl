package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;
import org.joml.Vector4i;

import java.util.List;

public class STGLMultiList extends STGLPanel {

    private List<DefaultColumn> columns;
    private final int rowHeight;
    private final int headHeight;

    private STGLFont font;

    private Vector4f fg;
    private Vector4f bg;

    private boolean isFocused = false;

    private Vector4f selectedFg;
    private Vector4f selectedBg;

    private STGLScrollbar scrollbar;

    private int scrollbarOffset = 0;

    private int selectedIdx = 0;

    private class MultilistMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {


        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double _x, double _y) {

            if (getComponentManager().getPriorityComponent((int) _x, (int) _y) != null){
                return;
            }
            int coverage_height = headHeight+((getHeight()-headHeight)/rowHeight)*rowHeight;

            if (button == MousePress.MOUSE_BUTTON_1) {
                isFocused = _x >= getX() && _x <= getX() + getWidth() && _y >= getY() && _y <= getY() + getHeight();

                if (_x >= getX() && (_x <= getX() + getWidth() - scrollbar_width) && (_y >= getY()) && (_y <= getY() + coverage_height)) {
                    int __y = (int) (_y - getY() - headHeight);

                    if ((scrollbarOffset + __y / rowHeight) < maxSize && (scrollbarOffset + __y / rowHeight) >= 0)
                        selectedIdx = scrollbarOffset + __y / rowHeight;
                    requestRepaint();
                }

            }
        }

        @Override
        public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {

        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

    private class MultilistKeyboardListener implements KeyboardListener{

        @Override
        public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            if (isFocused) {
                if (key == KeyPress.KEY_UP) {
                    if (selectedIdx - 1 >= 0) {
                        selectedIdx--;
                        requestRepaint();
                    }
                }
                if (key == KeyPress.KEY_DOWN) {
                    if (selectedIdx + 1 < maxSize) {
                        selectedIdx++;
                        requestRepaint();
                    }
                }
            }
        }

        @Override
        public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {

        }

        @Override
        public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {
            keyPressed(key, glfw_scancode, glfw_modifiers);
        }

        @Override
        public void charTyped(int codepoint) {

        }
    }

    MultilistMouseListener mouseListener = new MultilistMouseListener();
    MultilistKeyboardListener keyboardListener = new MultilistKeyboardListener();

    public interface ColumnFormatter{
        void paint(STGLGraphics graphics, STGLFont font, String column, int idx, int x, int y, boolean is_selected);
    }

    public static class DefaultColumn{
        private final String head;
        private List<String> column;
        private final int columnWidth;

        private final ColumnFormatter formatter;

        public DefaultColumn(String head, List<String> column){
            this.head = head;
            this.column = column;
            columnWidth = -1;
            formatter = null;
        }

        public DefaultColumn(String head, List<String> column, ColumnFormatter columnFormatter){
            this.head = head;
            this.column = column;
            columnWidth = -1;
            formatter = columnFormatter;
        }

        public DefaultColumn(String head, List<String> column, int column_width){
            this.head = head;
            this.column = column;
            columnWidth = column_width;
            formatter = null;
        }

        public DefaultColumn(String head, List<String> column, int column_width, ColumnFormatter columnFormatter){
            this.head = head;
            this.column = column;
            columnWidth = column_width;
            formatter = columnFormatter;
        }

        public void setColumn(List<String> column){
            this.column = column;
        }

        public List<String> getColumn() {
            return column;
        }

        public String getHead() {
            return head;
        }

        public int getColumnWidth() {
            return columnWidth;
        }
    }

    public STGLMultiList(List<DefaultColumn> columns, int x, int y, int width, int height, int row_height, int head_height){
        super(x, y, width, height);
        this.columns = columns;
        rowHeight = row_height;
        headHeight = head_height;
        calculateMaxSize();
        setupScrollbar(width, height);
    }


    public STGLMultiList(DefaultColumn column, int x, int y, int width, int height, int row_height, int head_height){
        super(x, y, width, height);
        this.columns = List.of(column);
        rowHeight = row_height;
        headHeight = head_height;
        calculateMaxSize();
        setupScrollbar(width, height);
    }

    private void setupScrollbar(int width, int height){
        int coverage_height = ((getHeight()-headHeight)/rowHeight);
        float coef = ((float)coverage_height/(float)(maxSize));
        if (coef > 1)
            coef = 1;
        int scrollbar_h = (int) ((height-8)*(coef));
        scrollbar = new STGLScrollbar(width-scrollbar_width,0,scrollbar_width,height, scrollbar_h);
        scrollbar.addMouseWheelArea(new Vector4i(0,0,width,height));
    }

    private void calculateMaxSize(){
        maxSize = 0 ;
        for (DefaultColumn c: columns){
            if (c.column.size() > maxSize){
                maxSize = c.column.size();
            }
        }
    }

    private int maxSize = 0;

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f fg) {
        this.fg = fg;
        scrollbar.setForeground(fg);
    }

    @Override
    public void setBackground(Vector4f bg) {
        this.bg = bg;
        scrollbar.setBackground(bg);
    }

    public void setSelectedForeground(Vector4f fg) {
        this.selectedFg = fg;
    }

    public void setSelectedBackground(Vector4f bg) {
        this.selectedBg = bg;
    }

    private static final int scrollbar_width = 20;
    @Override
    public void paint(STGLGraphics graphics) {
        super.paint(graphics);

        graphics.fillRect(getX(),getY(),getWidth()-scrollbar_width-1,getHeight(),bg);
        graphics.drawRect(getX(),getY(),getWidth(),getHeight(),fg);
        graphics.drawRect(getX(),getY(),getWidth()-scrollbar_width,headHeight,fg);

        int coverage_height = ((getHeight()-headHeight)/rowHeight);

        if (coverage_height < maxSize)
            scrollbarOffset = (int) (scrollbar.getScrollBarProgress()*(maxSize -coverage_height));
        else scrollbarOffset = 0;

        int __y = getY() + headHeight + 2 + (selectedIdx - scrollbarOffset)*rowHeight;

        if (__y >= getY() + headHeight && __y+rowHeight-2 <= getY() + getHeight())
            graphics.fillRect(getX()+font.getMaxWidth()-3, __y, getWidth()-font.getMaxWidth()-scrollbar_width, rowHeight-2, selectedBg);

        int _x = getX();
        for (DefaultColumn column: columns) {
            graphics.renderText(font, (_x + font.getMaxWidth()), getY() + (headHeight + font.getMaxHeight()) / 2, column.head, fg);

            int _y = getY() + headHeight + 2;
            int i = scrollbarOffset;
            if (scrollbarOffset < column.getColumn().size())
                for (String s : column.getColumn().subList(scrollbarOffset, column.getColumn().size())) {
                    if (i == selectedIdx) {
                        if (column.formatter == null)
                            graphics.renderText(font, _x + font.getMaxWidth(), _y + font.getFontMetrics().getMaxCharHeight(), s, selectedFg);
                        else
                            column.formatter.paint(graphics, font, s, i, _x + font.getMaxHeight(), _y, true);
                    } else {
                        if (column.formatter == null)
                            graphics.renderText(font, _x + font.getMaxWidth(), _y + font.getFontMetrics().getMaxCharHeight(), s, fg);
                        else
                            column.formatter.paint(graphics, font, s, i,_x + font.getMaxHeight(), _y, false);
                    }
                    _y += rowHeight;
                    if (_y + rowHeight > getY() + getHeight())
                        break;
                    i++;
                }
            _x += column.columnWidth;
        }
    }

    @Override
    public void initAdd() {
        super.initAdd();
        getComponentManager().addMouseListener(mouseListener);
        getComponentManager().addKeyboardListener(keyboardListener);

        super.addComponent(scrollbar);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        getComponentManager().removeMouseListener(mouseListener);
        getComponentManager().removeKeyboardListener(keyboardListener);

        super.removeComponent(scrollbar);
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(mouseListener);
            getComponentManager().removeKeyboardListener(keyboardListener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(mouseListener);
            getComponentManager().addKeyboardListener(keyboardListener);
        });
    }

    public int getSelectedIdx() {
        return selectedIdx;
    }

    public void setColumns(List<DefaultColumn> columns){
        this.columns = columns;
        calculateMaxSize();
        requestRepaint();
    }

    public STGLScrollbar getScrollbar() {
        return scrollbar;
    }
}
