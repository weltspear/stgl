package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

import java.util.ArrayList;

public abstract class STGLComponent {
    private boolean isEnabled = true;
    private boolean isPartiallyDisabled = false;
    private IGenericComponentManager componentManager = null;

    public abstract void setFont(STGLFont f);

    public abstract void setForeground(Vector4f color);

    public abstract void setBackground(Vector4f color);

    public abstract int getWidth();
    public abstract int getHeight();

    public void update(){

    }

    public abstract void paint(STGLGraphics graphics);

    public abstract int getX();
    public abstract int getY();

    public abstract void setX(int x);
    public abstract void setY(int y);

    public abstract void initAdd();
    public abstract void deinitRemove();

    public void disable(){
        if (!isEnabled)
            throw new RuntimeException("Double component disable");

        isEnabled = false;
    }
    public void enable(){
        if (isEnabled)
            throw new RuntimeException("Double component enable");

        isEnabled = true;
    }

    /***
     * If a component is partially disabled it will be drawn
     * by the <code>ComponentManager</code> but it may not react
     * to mouse or keyboard input (but that depends on component
     * because they are the ones which implement that sort of
     * behaviour)
     */
    public void partiallyDisable(){
        isPartiallyDisabled = true;
    }
    public void partiallyEnable(){
        isPartiallyDisabled = false;
    }

    public boolean isPartiallyDisabled(){
        return isPartiallyDisabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void requestRepaint(){
        if (componentManager != null)
            componentManager.requestRepaint();
    }

    public IGenericComponentManager getComponentManager(){
        return componentManager;
    }

    public void setComponentManager(IGenericComponentManager componentManager){
        this.componentManager = componentManager;
    }

    /***
     * Priority components can only be added to root <code>ComponentManager</code>,
     * adding them for example to <code>STGLPanel</code> will result in them being added to root <code>ComponentManager</code>
     */
    public boolean isPriorityComponent(){
        return false;
    }

    private final ArrayList<Runnable> invokeAfterInitialization = new ArrayList<>();

    /***
     * If component hasn't been initialized, stages <code>runnable</code> for execution, if it has been initialized the runnable is executed immediately
     */
    public void invokeAfterInitialization(Runnable runnable){
        if (getComponentManager() != null)
            runnable.run();
        else
            invokeAfterInitialization.add(runnable);
    }

    public ArrayList<Runnable> getRunnablesToBeInvokedAfterInitialization() {
        return invokeAfterInitialization;
    }
}
