package net.stgl.ui;

import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.ActionListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import org.joml.Vector4f;

import java.awt.*;

public class STGLClassicButton extends STGLComponent {

    private int width;
    private int height;
    private int x;
    private int y;
    private String text;
    private STGLFont font = null;
    private Vector4f fg = null;

    private STGLImage icon = null;

    private boolean isPressed = false;

    private ActionListener actionListener = null;

    private final STGLButtonMouseListener mouseListener = new STGLButtonMouseListener();

    public class STGLButtonMouseListener implements MouseListener{

        @Override
        public void mouseMoved(double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }

            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (x > STGLClassicButton.this.x && y > STGLClassicButton.this.y &&
                    x < STGLClassicButton.this.x+width && y < STGLClassicButton.this.y+height){
                requestRepaint();
            }
            else{
                isPressed = false;
                requestRepaint();
            }
        }

        @Override
        public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }

            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLClassicButton.this.x && y > STGLClassicButton.this.y &&
                        x < STGLClassicButton.this.x + width && y < STGLClassicButton.this.y + height) {
                    isPressed = true;
                    requestRepaint();
                }
            }
        }

        @Override
        public void mouseReleased(MousePress button, int glfw_mods, double x, double y) {
            if (isPartiallyDisabled()){
                return;
            }
            if (getComponentManager().getPriorityComponent((int) x, (int) y) != null){
                return;
            }

            if (button == MousePress.MOUSE_BUTTON_1) {
                if (x > STGLClassicButton.this.x && y > STGLClassicButton.this.y &&
                        x < STGLClassicButton.this.x + width && y < STGLClassicButton.this.y + height) {
                    if (isPressed) {
                        isPressed = false;
                        requestRepaint();
                        if (actionListener != null) {
                            actionListener.action();
                        }
                    }
                }
            }
        }

        @Override
        public void mouseWheelMoved(double xoffset, double yoffset) {

        }
    }

    public STGLClassicButton(int width, int height,
                      int x, int y, String text) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.text = text;
    }

    public STGLClassicButton(int width, int height,
                             int x, int y, STGLImage icon) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.text = null;
        this.icon = icon;
    }

    @Override
    public void setFont(STGLFont f) {
        font = f;
    }

    @Override
    public void setForeground(Vector4f color) {
        fg = color;
    }

    @Override
    public void setBackground(Vector4f color) {

    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void paint(STGLGraphics graphics) {
        Vector4f c1b = Utils.colorToVec(new Color(255, 255, 255));
        Vector4f c2b = Utils.colorToVec(new Color(0, 0, 0));

        Vector4f c3 = Utils.colorToVec(new Color(169,169,169)).mul(1.1f);
        Vector4f c4 = Utils.colorToVec(new Color(128, 128, 128)).mul(1.1f);

        Vector4f disabled = Utils.colorToVec(new Color(98, 98, 98));

        Vector4f border_top_color = !isPressed ? c1b :c2b;
        Vector4f border_bottom_color = !isPressed ? c2b :c1b;

        if (!isPartiallyDisabled())
        graphics.fillRect(x, y, width, height, isPressed ? c3 : c4);
        else graphics.fillRect(x, y, width, height, disabled);

        if (!isPartiallyDisabled()) {
            graphics.drawLine(x, y, x + width, y, border_top_color);
            graphics.drawLine(x, y, x, y + height, border_top_color);

            graphics.drawLine(x, y + height, x + width, y + height, border_bottom_color);
            graphics.drawLine(x + width, y, x + width, y + height, border_bottom_color);
        }
        else{
            graphics.drawLine(x, y, x + width, y, new Vector4f(border_top_color).mul(0.6f));
            graphics.drawLine(x, y, x, y + height, new Vector4f(border_top_color).mul(0.6f));

            graphics.drawLine(x, y + height, x + width, y + height, new Vector4f(border_bottom_color).mul(0.6f));
            graphics.drawLine(x + width, y, x + width, y + height, new Vector4f(border_bottom_color).mul(0.6f));
        }

        if (text != null) {
            STGLFont.FontMetrics fontMetrics = font.getFontMetrics();
            int tx = (width - fontMetrics.getStringWidth(text)) / 2;
            int ty = (height - fontMetrics.getMaxCharHeight()) / 2;

            graphics.renderText(font, x + tx, y + ty + fontMetrics.getMaxCharHeight(), text, fg);
        }
        else {
            graphics.drawImage(x+(width-icon.getWidth())/2, y+(height-icon.getHeight())/2, icon);
        }
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void initAdd() {
        getComponentManager().addMouseListener(mouseListener);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(mouseListener);
    }

    @Override
    public void disable() {
        super.disable();
        invokeAfterInitialization(() -> {
            getComponentManager().removeMouseListener(mouseListener);
        });
    }

    @Override
    public void enable() {
        super.enable();
        invokeAfterInitialization(() -> {
            getComponentManager().addMouseListener(mouseListener);
        });
    }

    public void setActionListener(ActionListener actionListener){
        this.actionListener = actionListener;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
