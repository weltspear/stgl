package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.ActionListener;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.image.STGLImage;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLCheckbox;

import java.awt.*;
import java.io.IOException;

public class Demo6 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        ComponentManager componentManager = new ComponentManager(frame, 4);
        //componentManager.enableDirectDrawing();

        STGLButton button = new STGLButton( 200, 30, 0, 0, "Disable checkbox?");

        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        button.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        button.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));

        STGLCheckbox checkbox = new STGLCheckbox( 250, 250, "Enable button?",
                new STGLImage(TextureManager.loadBufferedImage("tassets/checkbox_yes.png")),
                new STGLImage(TextureManager.loadBufferedImage("tassets/checkbox_no.png")));
        checkbox.setFont(monogram_12);
        checkbox.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        checkbox.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        checkbox.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(35, 35, 35)));
        checkbox.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        componentManager.addComponent(checkbox);
        checkbox.setCheckActionListener(new ActionListener() {
            private boolean isCheck = false;

            @Override
            public void action() {
                isCheck = !isCheck;
                if (isCheck)
                    button.partiallyEnable();
                else button.partiallyDisable();
            }
        });

        button.setFont(monogram_12);
        button.setActionListener(checkbox::partiallyDisable);
        componentManager.addComponent(button);
        button.partiallyDisable();

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
