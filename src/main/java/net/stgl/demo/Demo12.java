package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLDesktopPanel;
import net.stgl.ui.iframe.STGLInternalFrame;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.io.IOException;

public class Demo12 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_15 = FontManager.deriveFont("monogram", 15, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        ComponentManager componentManager = new ComponentManager(frame);
        //componentManager.enableDirectDrawing();

        STGLDesktopPanel desktopPanel = new STGLDesktopPanel(0,0,832,576);
        componentManager.addComponent(desktopPanel);

        for (int i = 0; i<5; i++) {
            STGLInternalFrame internalFrame2 = new STGLInternalFrame(150, 50, 300, 300, 20);
            STGLButton button = new STGLButton(75, 30, 20, 0, "text 1");

            button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
            button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
            button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
            button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
            button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
            button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
            button.setFont(monogram_15);
            button.setActionListener(() -> System.out.println("TEST"));

            internalFrame2.getContentPanel().addComponent(button);

            internalFrame2.setForeground(Utils.colorToVec(new Color(255, 255, 255)));
            internalFrame2.setBorderColor(Utils.colorToVec(new Color(89, 89, 89)));
            internalFrame2.setBackground(Utils.colorToVec(new Color(215, 215, 215)));
            internalFrame2.setTopPanelBackground(Utils.colorToVec(new Color(16, 29, 58)));
            internalFrame2.setFont(monogram_15);
            internalFrame2.setTitle("Window " + i);
            desktopPanel.addInternalFrame(internalFrame2);
        }

        PerformanceMeasurer p = new PerformanceMeasurer();

        while (!frame.shouldClose()){
            p.begin();

            frame.getGraphics().fillCircle(0, 0, 20, 50, new Vector4f(1,0,1,1));
            componentManager.paint();

            frame.update();
            last = p.end();
            frame.getGraphics().clear();
        }

        monogram_15.dispose();

        frame.dispose();

        p.showResults();
    }
}