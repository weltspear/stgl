package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import org.joml.Vector4f;

import java.io.IOException;

public class Demo13 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 36, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();


        while (!frame.shouldClose()){
            p.begin();
            graphics.drawRect(45, 45, 45, 45, 3, new Vector4f(1,0,0,1));
            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
