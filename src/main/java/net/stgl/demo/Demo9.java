package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.io.IOException;

public class Demo9 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();
        graphics.defineScissorBox(0,0,300,300);

        STGLGraphics graphics1 = graphics.childGraphics();
        graphics1.defineScissorBox(50, 50, 50, 50);
        graphics1.setTranslationVector(new Vector2i(50, 50));

        STGLGraphics graphics2 = graphics1.childGraphics();
        graphics2.defineScissorBox(10, 10, 20, 20);
        graphics2.setTranslationVector(new Vector2i(10, 10));

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  new Vector4f(1, 1, 1, 1));

            graphics.drawRect(0,0,300,300, new Vector4f(1,0,0,1));
            graphics.drawRect(3,3,600,600, new Vector4f(0,1,0,1));

            graphics1.drawRect(0, 0, 50, 50, new Vector4f(0,0,1,1));
            graphics1.drawRect(0, 20, 75, 75, new Vector4f(0,1,0,1));

            graphics2.fillRect(0, 0, 20, 20, new Vector4f(1,1,1,1));

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
