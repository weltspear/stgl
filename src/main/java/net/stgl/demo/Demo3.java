package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLPanel;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;

public class Demo3 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        ComponentManager componentManager = new ComponentManager(frame, 4);
        //componentManager.enableDirectDrawing();

        STGLPanel panel = new STGLPanel(100, 100, 100, 100);
        componentManager.addComponent(panel);

        STGLButton button = new STGLButton( 75, 30, 0, 0, "text 1");

        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        button.setFont(monogram_12);
        button.setActionListener(() -> System.out.println("TEST"));
        panel.addComponent(button);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  new Vector4f(1, 1, 1, 1));

            graphics.drawRect(0,0,50,50, new Vector4f(1,0,0,1));

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
