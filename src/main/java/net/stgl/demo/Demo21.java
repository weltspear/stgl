package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLDisplayTable;
import net.stgl.ui.STGLMultiList;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

public class Demo21 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();
        graphics.defineScissorBox(0,0,300,300);

        ComponentManager componentManager = new ComponentManager(frame);
        componentManager.enableDirectDrawing();

        BufferedImage img = null;

        try{
            img = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");
        }catch (Exception ignored){

        }

        STGLDisplayTable table = new STGLDisplayTable(List.of(new
                STGLMultiList.DefaultColumn("bbbbbbbb", List.of("aaaa", "aaaa", "aaaa"), 125/2),
                new
                        STGLMultiList.DefaultColumn("aaaaaaaa", List.of("bbbb", "aaaa", "aaaa"))), 50, 50, 300,
                300, 9, 30);
        table.setFont(monogram_12);
        table.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        table.setBackgroundA(Utils.colorToVec(new Color(60, 38, 22)));
        table.setBackgroundB(Utils.colorToVec(new Color(93, 58, 34)));
        table.setBackground(Utils.colorToVec(new Color(40, 26, 15)));
        componentManager.addComponent(table);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
