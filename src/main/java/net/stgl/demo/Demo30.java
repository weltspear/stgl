package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLMultiList;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

public class Demo30 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");


        STGLFont monogram_12 = FontManager.deriveFont("monogram", 16, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        ComponentManager componentManager = new ComponentManager(frame);
        componentManager.enableDirectDrawing();

        BufferedImage img = null;

        try{
            img = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");
        }catch (Exception ignored){

        }

        Texture t = new Texture(img);

        STGLMultiList list = new STGLMultiList(new STGLMultiList.DefaultColumn("Test", List.of("a", "b", "c"), (graphics, font, column, i, x, y, is_selected) -> {
            graphics.drawTextureScaled(x, y, t, STGLColors.WHITE, 16, 16);
        }),60,60,300,300, 16, 24);
        list.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        list.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        list.setSelectedBackground(Utils.colorToVec(new Color(51, 39, 31)));
        list.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        list.setFont(monogram_12);
        componentManager.addComponent(list);

        while (!frame.shouldClose()){
            p.begin();

            frame.getGraphics().clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
