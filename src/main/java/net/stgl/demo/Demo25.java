package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLPopupMenu;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class Demo25 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        ComponentManager componentManager = new ComponentManager(frame, 4);
        //componentManager.enableDirectDrawing();

        ArrayList<STGLPopupMenu.PopupMenuEntry> entries = new ArrayList<>();
        entries.add(new STGLPopupMenu.PopupMenuEntry("Delete System32", () -> System.out.println("A")));
        entries.add(new STGLPopupMenu.PopupMenuEntry("Delete Windows", () -> System.out.println("A")));
        entries.add(new STGLPopupMenu.PopupMenuEntry("Delete *", () -> System.out.println("A")));


        STGLPopupMenu popupMenu = new STGLPopupMenu(50, 50, monogram_12, entries);
        popupMenu.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        popupMenu.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        popupMenu.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        popupMenu.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        popupMenu.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        popupMenu.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        componentManager.addComponent(popupMenu);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
