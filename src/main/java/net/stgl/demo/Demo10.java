package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLMultiList;

import java.awt.*;
import java.io.IOException;
import java.util.List;

public class Demo10 {
    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        long last = 0;

        ComponentManager componentManager = new ComponentManager(frame, 4);

        STGLMultiList stglMultiList = new STGLMultiList(
                List.of(new STGLMultiList.DefaultColumn("A", List.of("AAA", "BBB", "CCC"), 120),
                        new STGLMultiList.DefaultColumn("A", List.of("AAA", "BBB", "CCC", "AAA", "BBB", "CCC","BBB", "CCC","BBB", "CCC","BBB", "CCC","BBB", "CCC","BBB", "CCC","BBB", "CCC", "AAA", "BBB", "CCC", "AAA", "BBB", "CCC", "AAA", "BBB", "CCC"), 50)),
                100, 100, 500, 300, monogram_12.getMaxHeight()+4, 25);

        stglMultiList.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        stglMultiList.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        stglMultiList.setSelectedBackground(Utils.colorToVec(new Color(51, 39, 31)));
        stglMultiList.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        stglMultiList.setFont(monogram_12);

        componentManager.addComponent(stglMultiList);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
