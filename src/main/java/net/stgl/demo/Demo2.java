package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.image.STGLImage;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLCheckbox;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;

public class Demo2 {
    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4));

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        long last = 0;

        ComponentManager componentManager = new ComponentManager(frame, 4);

        STGLButton button = new STGLButton( 75, 30, 150, 150, "text 1");

        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        button.setFont(monogram_12);
        button.setActionListener(() -> System.out.println("TEST"));

        STGLCheckbox checkbox = new STGLCheckbox( 250, 250, "abcdefghijklmnop",
                new STGLImage(TextureManager.loadBufferedImage("tassets/checkbox_yes.png")),
                new STGLImage(TextureManager.loadBufferedImage("tassets/checkbox_no.png")));
        checkbox.setFont(monogram_12);
        checkbox.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        checkbox.setBackground(Utils.colorToVec(new Color(60, 38, 22)));

        componentManager.addComponent(checkbox);
        componentManager.addComponent(button);

        final int[] xm = {0};
        final int[] ym = {0};

        frame.addMouseListener(new MouseListener() {
            @Override
            public void mouseMoved(double x, double y) {
                xm[0] = (int) x;
                ym[0] = (int) y;
            }

            @Override
            public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {

            }

            @Override
            public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {

            }

            @Override
            public void mouseWheelMoved(double xoffset, double yoffset) {

            }
        });

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  new Vector4f(1, 1, 1, 1));
            graphics.renderText( monogram_12,0, 100, xm[0] + " " + ym[0],  new Vector4f(1, 1, 1, 1));

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
