package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import org.joml.Vector4f;

import java.io.IOException;

public class Demo15 {
    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig());

        frame.show();
        ErrorCheck.enableDebugging();
        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");
        ErrorCheck.checkErrorGL();

        Texture _t = new Texture(TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png"));

        Texture t = new Texture(32, 32);
        t.dispose();

        t = new Texture(32, 32);

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        STGLGraphics graphics = frame.getGraphics();


        while (!frame.shouldClose()){
            graphics.clear();

            ErrorCheck.checkErrorGL();
            graphics.renderText(monogram_12, 50, 50, "TEST", new Vector4f(1,1,1,1));

            graphics.drawTexture(89, 89, _t);
            ErrorCheck.checkErrorGL();

            frame.update();
        }

        monogram_12.dispose();

        frame.dispose();


    }
}