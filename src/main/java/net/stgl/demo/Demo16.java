package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.ArrayTexture;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class Demo16 {
    public static void main(String[] args) throws IOException {
        ErrorCheck.enableDebugging();
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig());

        frame.show();
        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        STGLGraphics graphics = frame.getGraphics();

        ArrayTexture arrayTexture = new ArrayTexture(32, 32, 5, new Texture.TextureOptions()
                .setMinFiltering(Texture.Filtering.NEAREST).setMagFiltering(Texture.Filtering.NEAREST));

        BufferedImage image = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");

        for (int i = 0; i < 2; i++)
            arrayTexture.writeImage(i, image);
        ErrorCheck.checkErrorGL();

        while (!frame.shouldClose()){
            graphics.clear();

            ErrorCheck.checkErrorGL();
            graphics.renderText(monogram_12, 50, 50, "TEST", new Vector4f(1,1,1,1));
            graphics.drawArrayTextureScaled(56, 56, 1, arrayTexture, new Vector4f(1,1,1,1), 32, 32);
            ErrorCheck.checkErrorGL();

            frame.update();
        }

        monogram_12.dispose();

        frame.dispose();


    }
}