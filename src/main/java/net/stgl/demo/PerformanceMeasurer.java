package net.stgl.demo;

public class PerformanceMeasurer {

    private long avg_i = 0;
    private long count = 0;

    private long worst = 0;
    private long best = Long.MAX_VALUE;

    private long m = 0;

    public PerformanceMeasurer(){

    }

    public void begin(){
        m = System.currentTimeMillis();
    }

    public long end(){
        long t = System.currentTimeMillis()-m;
        if (t > worst){
            worst = t;
        }
        if (t < best){
            best = t;
        }

        avg_i+=t;
        count++;
        return t;
    }

    public void showResults(){
        System.out.println("Average performance: " + avg_i/count);
        System.out.println("Worst case performance: " + worst);
        System.out.println("Best case performance: " + best);
    }
}
