package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.TextureBatch;
import net.stgl.texture.TextureManager;
import net.stgl.texture.TextureRegion;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class Demo5 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 26, false, false);

        BufferedImage rifleman = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");
        BufferedImage rifleman2 = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");

        DynamicTextureAtlas atlas = new DynamicTextureAtlas(100, 32, 32);
        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        TextureBatch textureBatch = new TextureBatch(graphics, atlas, 20000);

        TextureRegion _rifleman = atlas.getTextureRegion(rifleman);
        _rifleman = atlas.getTextureRegion(rifleman2);

        while (!frame.shouldClose()) {
            p.begin();

            graphics.clear();

            textureBatch.begin();

            textureBatch.drawTextureRegion(64, 64, _rifleman);

            textureBatch.flush();

            graphics.renderText(monogram_12, 0, 100 - 60, "FPS: " + 1000f / last, new Vector4f(1, 1, 1, 1));

            frame.update();
            last = p.end();
        }

        atlas.getAtlas().dispose();
        monogram_12.dispose();

        frame.dispose();

        p.showResults();


    }
}