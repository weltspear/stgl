package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import org.joml.Vector4f;

import java.io.IOException;

public class Demo14 {
    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig());

        frame.show();
        ErrorCheck.enableDebugging();
        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");


        for (int i = 0; i < 100; i++) {
            Texture t = new Texture(32, 567);
            System.out.println(t.getId());
            t.dispose();
            STGLFont monogram_12 = FontManager.deriveFont("monogram", (int) (Math.random()*100), false, false);
            System.out.println(monogram_12.getId());
            monogram_12.dispose();

        }
        ErrorCheck.checkErrorGL();

        Texture _t = new Texture(TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png"));

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        STGLGraphics graphics = frame.getGraphics();

        frame.addKeyboardListener(new KeyboardListener() {
            @Override
            public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
                if (key == KeyPress.KEY_1){
                    for (int i = 0; i < 15; i++) {
                        Texture t = new Texture(32, 567);
                        System.out.println(t.getId());
                        t.dispose();
                        ErrorCheck.checkErrorGL();
                        STGLFont _monogram_12 = FontManager.deriveFont("monogram", (int) (Math.random()*100), false, false);
                        System.out.println(_monogram_12.getId());
                        _monogram_12.dispose();
                    }
                }
                if (key == KeyPress.KEY_2){
                    System.out.println(_t.getId());
                    System.out.println(monogram_12.getId());
                    ErrorCheck.checkErrorGL();
                }

                if (key == KeyPress.KEY_3){
                    System.out.println("FUCLK");
                    _t.dispose();
                    monogram_12.dispose();
                    ErrorCheck.checkErrorGL();
                }
            }

            @Override
            public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void charTyped(int codepoint) {

            }
        });


        while (!frame.shouldClose()){
            graphics.clear();

            ErrorCheck.checkErrorGL();
            graphics.renderText(monogram_12, 50, 50, "TEST", new Vector4f(1,1,1,1));

            graphics.drawTexture(89, 89, _t);
            ErrorCheck.checkErrorGL();

            frame.update();
        }

        monogram_12.dispose();

        frame.dispose();
    }
}