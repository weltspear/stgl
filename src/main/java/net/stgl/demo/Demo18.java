package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.vidmode.Monitor;
import net.stgl.vidmode.Monitors;
import net.stgl.vidmode.VideoMode;

import java.util.ArrayList;

public class Demo18 {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame(300, 300, "Test", new MainFrame.FrameConfig());

        ArrayList<Monitor> monitors = Monitors.getMonitors();
        for (Monitor monitor: monitors){
            System.out.println(monitor.name());
            for (VideoMode videoMode: monitor.videoModes()){
                System.out.println(videoMode.width() + "x" + videoMode.height() + " " +videoMode.refreshRate());
            }
        }

        mainFrame.dispose();
    }
}
