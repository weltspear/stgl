package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.shader.Shader;
import net.stgl.shader.ShaderProgram;
import net.stgl.vertex.VertexBuffer;

import java.io.IOException;

import static org.lwjgl.opengl.GL32.*;

public class Demo32 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();
        frame.addWindowResizeListener((width, height) -> System.out.println(width+" "+height));

        ErrorCheck.enableDebugging();
        ErrorCheck.checkErrorGL();

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        String vsrc = """
                #version 150 core
                in vec2 pos;
                out vec2 _pos;
                
                void main(){
                    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
                    _pos = pos;
                }
                
                """;

        Shader vshader = new Shader(vsrc, GL_VERTEX_SHADER);

        String fsrc = """
                #version 150 core
                in vec2 _pos;
                out vec4 outColor;
                uniform float t;
                
                void main(){
                    outColor = vec4(_pos.x+sin(t), _pos.y+sin(t), cos(t)*sin(t), 1.0);
                }
                """;

        Shader fshader = new Shader(fsrc, GL_FRAGMENT_SHADER);

        ShaderProgram program = new ShaderProgram();
        program.attachShader(vshader);
        program.attachShader(fshader);
        program.link();
        program.useThis();

        VertexBuffer vbo = new VertexBuffer();
        VertexBuffer.VertexArrays vao = vbo.createVertexArrays();

        vbo.bind();
        vao.bind();
        vao.vertexAttribPointer(program.getAttributeLocation("pos"),
                2, GL_FLOAT, false, 0, 0);
        //vao.enable();

        int x = 0;
        int y = 0;
        int width = frame.getWidth();
        int height = frame.getHeight();

        vbo.bufferData(new float[]{graphics.toHardwareX(x), graphics.toHardwareY(y),
                graphics.toHardwareX(x+width), graphics.toHardwareY(y),
                graphics.toHardwareX(x), graphics.toHardwareY(y+height),
                graphics.toHardwareX(x+width), graphics.toHardwareY(y+height)}, GL_STATIC_DRAW);

        float t = 0;

        int t_loc = program.getUniformLocation("t");
        glUniform1f(t_loc, t);
        vbo.drawArrays(GL_TRIANGLE_STRIP, 0, 4);

        ErrorCheck.checkErrorGL();


        while (!frame.shouldClose()){
            p.begin();

            vbo.bind();
            vbo.drawArrays(GL_TRIANGLE_STRIP, 0, 4);
            glUniform1f(t_loc, t);
            t+=1/10000f;

            frame.update();
            p.end();
        }


        frame.dispose();

        p.showResults();
    }
}
