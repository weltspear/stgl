package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.color.STGLColors;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;

import java.io.IOException;

public class Demo22 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();
        graphics.defineScissorBox(0,0,300,300);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            graphics.partiallyFillCircle(60, 60, 4, 15, 30, 20,STGLColors.GRAY);

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
