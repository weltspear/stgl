package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLSlider;

import java.awt.*;
import java.io.IOException;

public class Demo35 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4));

        frame.show();

        ErrorCheck.checkErrorGL();

        PerformanceMeasurer p = new PerformanceMeasurer();

        ComponentManager componentManager = new ComponentManager(frame, 4);

        STGLSlider scrollbar = new STGLSlider(100, 25, 100, 25);
        scrollbar.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        scrollbar.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        scrollbar.setSliderSpeedWheel(2);
        componentManager.addComponent(scrollbar);


        while (!frame.shouldClose()){
            p.begin();

            frame.getGraphics().clear();
            componentManager.paint();

            frame.update();
            p.end();
        }


        frame.dispose();

        p.showResults();
    }
}
