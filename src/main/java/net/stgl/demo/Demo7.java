package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLEntry;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;

public class Demo7 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        ComponentManager componentManager = new ComponentManager(frame, 4);
        //componentManager.enableDirectDrawing();

        STGLEntry entry = new STGLEntry(50, 50, 300, 40, "Type here");

        entry.setFont(monogram_12);

        entry.setForeground(Utils.colorToVec(new Color(122, 82, 45)));
        entry.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveBackgroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveForegroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        entry.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        entry.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));

        componentManager.addComponent(entry);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  new Vector4f(1, 1, 1, 1));

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();
        componentManager.dispose();

        frame.dispose();

        p.showResults();


    }
}
