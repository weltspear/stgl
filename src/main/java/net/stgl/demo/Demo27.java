package net.stgl.demo;

import net.stgl.Framebuffer;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import org.joml.Vector4f;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Demo27 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 26, false, false);

        // drawing stuff to texture
        Texture rifleman = new Texture(TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png"));
        Framebuffer riflemanTarget = new Framebuffer(rifleman);
        STGLGraphics rGraphics = new STGLGraphics(riflemanTarget);
        rGraphics.fillCircle(0, 0, 20, 6, new Vector4f(1,1,1,1));
        rGraphics.dispose();

        // read bytes
        ByteBuffer rfl2 = riflemanTarget.readPixels();
        riflemanTarget.dispose();

        // write them to vram again
        rifleman.dispose();
        rifleman = new Texture(32, 32, rfl2);

        STGLGraphics graphics = frame.getGraphics();

        while (!frame.shouldClose()) {

            graphics.clear();
            graphics.drawTexture(50, 50, rifleman);

            frame.update();
        }

        rifleman.dispose();
        monogram_12.dispose();

        frame.dispose();


    }
}
