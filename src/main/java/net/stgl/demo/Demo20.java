package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.TextureManager;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLIconButton;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Demo20 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();
        graphics.defineScissorBox(0,0,300,300);

        ComponentManager componentManager = new ComponentManager(frame);
        componentManager.enableDirectDrawing();

        BufferedImage img = null;

        try{
            img = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");
        }catch (Exception ignored){

        }

        STGLIconButton button = new STGLIconButton(36,36,new STGLImage(img));
        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        componentManager.addComponent(button);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            componentManager.paint();

            frame.update();
            last = p.end();
        }

        monogram_12.dispose();

        frame.dispose();

        p.showResults();
    }
}
