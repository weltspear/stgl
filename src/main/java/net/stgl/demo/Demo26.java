package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.Utils;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLDesktopPanel;
import net.stgl.ui.STGLPopupMenu;
import net.stgl.ui.iframe.STGLInternalFrame;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class Demo26 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_15 = FontManager.deriveFont("monogram", 15, false, false);

        ErrorCheck.checkErrorGL();

        float last = 0;

        ComponentManager componentManager = new ComponentManager(frame);
        //componentManager.enableDirectDrawing();

        STGLDesktopPanel desktopPanel = new STGLDesktopPanel(0, 0, 832, 576);
        componentManager.addComponent(desktopPanel);

        STGLInternalFrame internalFrame2 = new STGLInternalFrame(150, 50, 400, 400, 20);

        internalFrame2.addMouseListener(new MouseListener() {
            @Override
            public void mouseMoved(double x, double y) {

            }

            @Override
            public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
                if (mouseButton == MousePress.MOUSE_BUTTON_2){
                    internalFrame2.invokeLater(() -> {
                        ArrayList<STGLPopupMenu.PopupMenuEntry> entries = new ArrayList<>();
                        entries.add(new STGLPopupMenu.PopupMenuEntry("Test buttton bla bla", () -> System.out.println("A")));
                        entries.add(new STGLPopupMenu.PopupMenuEntry("bla bla", () -> System.out.println("A")));
                        entries.add(new STGLPopupMenu.PopupMenuEntry("Blabla", () -> System.out.println("A")));
                        STGLPopupMenu popupMenu = new STGLPopupMenu((int) x, (int) y, monogram_15, entries);
                        popupMenu.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
                        popupMenu.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
                        popupMenu.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
                        popupMenu.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
                        popupMenu.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
                        popupMenu.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
                        internalFrame2.addComponent(popupMenu);
                    });
                }
            }

            @Override
            public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {

            }

            @Override
            public void mouseWheelMoved(double xoffset, double yoffset) {

            }
        });

        internalFrame2.setForeground(Utils.colorToVec(new Color(255, 255, 255)));
        internalFrame2.setBorderColor(Utils.colorToVec(new Color(89, 89, 89)));
        internalFrame2.setBackground(Utils.colorToVec(new Color(215, 215, 215)));
        internalFrame2.setTopPanelBackground(Utils.colorToVec(new Color(16, 29, 58)));
        internalFrame2.setFont(monogram_15);
        internalFrame2.setTitle("Window ");
        desktopPanel.addInternalFrame(internalFrame2);


        PerformanceMeasurer p = new PerformanceMeasurer();

        while (!frame.shouldClose()) {
            p.begin();

            frame.getGraphics().fillCircle(0, 0, 20, 50, new Vector4f(1, 0, 1, 1));
            componentManager.paint();

            frame.update();
            last = p.end();
            frame.getGraphics().clear();
        }

        monogram_15.dispose();

        frame.dispose();

        p.showResults();
    }
}