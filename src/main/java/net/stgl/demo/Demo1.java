package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.texture.StaticTextureAtlas;
import net.stgl.texture.TextureBatch;
import net.stgl.texture.TextureManager;
import net.stgl.texture.TextureRegion;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

// Fonts and images used in this example are not provided within library's files
public class Demo1 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4));

        AtomicInteger soldier_count = new AtomicInteger(1000);

        frame.addKeyboardListener(new KeyboardListener() {
            @Override
            public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
                if (key == KeyPress.KEY_ESCAPE){
                    frame.close();
                }
                else if (key == KeyPress.KEY_PAGE_UP)
                    soldier_count.addAndGet(1000);
                else if (key == KeyPress.KEY_PAGE_DOWN)
                    if (soldier_count.get() - 1000 > 0)
                        soldier_count.addAndGet(-1000);
            }

            @Override
            public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void charTyped(int codepoint) {

            }
        });

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 26, false, false);

        ArrayList<BufferedImage> loadtoatlas = new ArrayList<>();
        BufferedImage rifleman = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");

        for (int i = 0; i<25; i++){
            loadtoatlas.add(rifleman);
        }

        StaticTextureAtlas atlas = new StaticTextureAtlas(loadtoatlas);
        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        TextureBatch textureBatch = new TextureBatch(graphics, atlas, 80000);

        TextureRegion _rifleman = atlas.getTextureRegions().get(0);

        Vector4f cvec = new Vector4f(1, 1, 1, 1);

        Random random = new Random();

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            textureBatch.begin();

            for (int i = 0; i < soldier_count.get(); i++)
                textureBatch.drawTextureRegion( random.nextInt(842)-10, random.nextInt(576), _rifleman);

            textureBatch.flush();

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  cvec);
            graphics.renderText( monogram_12,832-200, 100-60, "Soldier count: " + soldier_count.get(),  cvec);

            frame.update();
            last = p.end();
        }

        atlas.getAtlas().dispose();
        monogram_12.dispose();

        frame.dispose();

        p.showResults();


    }

}