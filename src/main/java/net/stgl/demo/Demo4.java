package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.FontManager;
import net.stgl.texture.StaticTextureAtlas;
import net.stgl.texture.TextureManager;
import net.stgl.texture.TextureRegion;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

// Fonts and images used in this example are not provided within library's files
public class Demo4 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();

        FontManager.loadFont("monogram", "fonts/monogram-extended.ttf");

        STGLFont monogram_12 = FontManager.deriveFont("monogram", 12, false, false);

        ArrayList<BufferedImage> loadtoatlas = new ArrayList<>();
        BufferedImage rifleman = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");

        for (int i = 0; i<2; i++){
            loadtoatlas.add(rifleman);
        }

        StaticTextureAtlas atlas = new StaticTextureAtlas(loadtoatlas);
        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        TextureRegion _rifleman = atlas.getTextureRegions().get(1);

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();

            graphics.drawTextureRegion(64, 64, _rifleman, new Vector4f(1,1,1,1));

            graphics.renderText( monogram_12,0, 100-60, "FPS: " + 1000f / last,  new Vector4f(1, 1, 1, 1));

            frame.update();
            last = p.end();
        }

        atlas.getAtlas().dispose();
        monogram_12.dispose();

        frame.dispose();

        p.showResults();

    }

}