package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class Demo28 {

    public static void main(String[] args) throws IOException {
        ErrorCheck.enableDebugging();
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4));

        BufferedImage rifleman = TextureManager.loadBufferedImage("tassets/rifleman_idle_1.png");
        frame.show();

        ErrorCheck.checkErrorGL();

        float last = 0;

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();

        Texture t = new Texture(rifleman, new Texture.TextureOptions().setMagFiltering(Texture.Filtering.LINEAR).setMinFiltering(Texture.Filtering.LINEAR_MIPMAPS_LINEAR));

        Random random = new Random();

        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();
            graphics.miscDrawTextureBlur(50, 50, t, 1/32f, 1/32f);
            ErrorCheck.checkErrorGL();

            frame.update();
            last = p.end();
        }

        frame.dispose();

        p.showResults();


    }

}