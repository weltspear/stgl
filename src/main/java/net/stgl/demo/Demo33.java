package net.stgl.demo;

import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.vidmode.Monitors;
import org.joml.Vector4f;

import java.io.IOException;

public class Demo33 {

    public static void main(String[] args) throws IOException {
        MainFrame frame = new MainFrame(832, 576, "STGL",
                new MainFrame.FrameConfig().enableMSAA(4).disableVSync());

        frame.show();
        frame.addWindowResizeListener((width, height) -> System.out.println(width+" "+height));

        ErrorCheck.checkErrorGL();

        PerformanceMeasurer p = new PerformanceMeasurer();

        STGLGraphics graphics = frame.getGraphics();
        // to test whether everything breaks
        frame.setFullscreen(Monitors.getPrimaryVideoMode(), Monitors.getPrimaryMonitor());
        frame.setWindowed(832, 576);


        while (!frame.shouldClose()){
            p.begin();

            graphics.clear();
            graphics.fillRect(0,0, frame.getWidth(), frame.getHeight(), new Vector4f(1,0,0,1));

            frame.update();
            p.end();
        }


        frame.dispose();

        p.showResults();
    }
}
