package net.stgl;

import net.stgl.event.*;
import net.stgl.state.GlobalState;
import net.stgl.texture.Texture;
import net.stgl.vidmode.Monitor;
import net.stgl.vidmode.Monitors;
import net.stgl.vidmode.VideoMode;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.system.MemoryUtil.NULL;

@SuppressWarnings("unused")
public class MainFrame {

    public class KeyboardListenerManager implements GLFWKeyCallbackI{
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (!keyboardListeners.isEmpty()){
                for (KeyboardListener listener: keyboardListeners){
                    if (action == GLFW_RELEASE){
                        listener.keyReleased(glfwConstant2KeyPress(key), scancode, mods);
                    }
                    else if (action == GLFW_PRESS){
                        listener.keyPressed(glfwConstant2KeyPress(key), scancode, mods);
                    }
                    else if (action == GLFW_REPEAT){
                        listener.keyPressedRepeat(glfwConstant2KeyPress(key), scancode, mods);
                    }
                }
            }
        }

        private KeyPress glfwConstant2KeyPress(int key){
            if (key== KeyPress.KEY_UNKNOWN.getGLFWConstant()) { return KeyPress.KEY_UNKNOWN;}
            if (key== KeyPress.KEY_SPACE.getGLFWConstant()) { return KeyPress.KEY_SPACE;}
            if (key== KeyPress.KEY_APOSTROPHE.getGLFWConstant()) { return KeyPress.KEY_APOSTROPHE;}
            if (key== KeyPress.KEY_COMMA.getGLFWConstant()) { return KeyPress.KEY_COMMA;}
            if (key== KeyPress.KEY_MINUS.getGLFWConstant()) { return KeyPress.KEY_MINUS;}
            if (key== KeyPress.KEY_PERIOD.getGLFWConstant()) { return KeyPress.KEY_PERIOD;}
            if (key== KeyPress.KEY_SLASH.getGLFWConstant()) { return KeyPress.KEY_SLASH;}
            if (key== KeyPress.KEY_0.getGLFWConstant()) { return KeyPress.KEY_0;}
            if (key== KeyPress.KEY_1.getGLFWConstant()) { return KeyPress.KEY_1;}
            if (key== KeyPress.KEY_2.getGLFWConstant()) { return KeyPress.KEY_2;}
            if (key== KeyPress.KEY_3.getGLFWConstant()) { return KeyPress.KEY_3;}
            if (key== KeyPress.KEY_4.getGLFWConstant()) { return KeyPress.KEY_4;}
            if (key== KeyPress.KEY_5.getGLFWConstant()) { return KeyPress.KEY_5;}
            if (key== KeyPress.KEY_6.getGLFWConstant()) { return KeyPress.KEY_6;}
            if (key== KeyPress.KEY_7.getGLFWConstant()) { return KeyPress.KEY_7;}
            if (key== KeyPress.KEY_8.getGLFWConstant()) { return KeyPress.KEY_8;}
            if (key== KeyPress.KEY_9.getGLFWConstant()) { return KeyPress.KEY_9;}
            if (key== KeyPress.KEY_SEMICOLON.getGLFWConstant()) { return KeyPress.KEY_SEMICOLON;}
            if (key== KeyPress.KEY_EQUAL.getGLFWConstant()) { return KeyPress.KEY_EQUAL;}
            if (key== KeyPress.KEY_A.getGLFWConstant()) { return KeyPress.KEY_A;}
            if (key== KeyPress.KEY_B.getGLFWConstant()) { return KeyPress.KEY_B;}
            if (key== KeyPress.KEY_C.getGLFWConstant()) { return KeyPress.KEY_C;}
            if (key== KeyPress.KEY_D.getGLFWConstant()) { return KeyPress.KEY_D;}
            if (key== KeyPress.KEY_E.getGLFWConstant()) { return KeyPress.KEY_E;}
            if (key== KeyPress.KEY_F.getGLFWConstant()) { return KeyPress.KEY_F;}
            if (key== KeyPress.KEY_G.getGLFWConstant()) { return KeyPress.KEY_G;}
            if (key== KeyPress.KEY_H.getGLFWConstant()) { return KeyPress.KEY_H;}
            if (key== KeyPress.KEY_I.getGLFWConstant()) { return KeyPress.KEY_I;}
            if (key== KeyPress.KEY_J.getGLFWConstant()) { return KeyPress.KEY_J;}
            if (key== KeyPress.KEY_K.getGLFWConstant()) { return KeyPress.KEY_K;}
            if (key== KeyPress.KEY_L.getGLFWConstant()) { return KeyPress.KEY_L;}
            if (key== KeyPress.KEY_M.getGLFWConstant()) { return KeyPress.KEY_M;}
            if (key== KeyPress.KEY_N.getGLFWConstant()) { return KeyPress.KEY_N;}
            if (key== KeyPress.KEY_O.getGLFWConstant()) { return KeyPress.KEY_O;}
            if (key== KeyPress.KEY_P.getGLFWConstant()) { return KeyPress.KEY_P;}
            if (key== KeyPress.KEY_Q.getGLFWConstant()) { return KeyPress.KEY_Q;}
            if (key== KeyPress.KEY_R.getGLFWConstant()) { return KeyPress.KEY_R;}
            if (key== KeyPress.KEY_S.getGLFWConstant()) { return KeyPress.KEY_S;}
            if (key== KeyPress.KEY_T.getGLFWConstant()) { return KeyPress.KEY_T;}
            if (key== KeyPress.KEY_U.getGLFWConstant()) { return KeyPress.KEY_U;}
            if (key== KeyPress.KEY_V.getGLFWConstant()) { return KeyPress.KEY_V;}
            if (key== KeyPress.KEY_W.getGLFWConstant()) { return KeyPress.KEY_W;}
            if (key== KeyPress.KEY_X.getGLFWConstant()) { return KeyPress.KEY_X;}
            if (key== KeyPress.KEY_Y.getGLFWConstant()) { return KeyPress.KEY_Y;}
            if (key== KeyPress.KEY_Z.getGLFWConstant()) { return KeyPress.KEY_Z;}
            if (key== KeyPress.KEY_LEFT_BRACKET.getGLFWConstant()) { return KeyPress.KEY_LEFT_BRACKET;}
            if (key== KeyPress.KEY_BACKSLASH.getGLFWConstant()) { return KeyPress.KEY_BACKSLASH;}
            if (key== KeyPress.KEY_RIGHT_BRACKET.getGLFWConstant()) { return KeyPress.KEY_RIGHT_BRACKET;}
            if (key== KeyPress.KEY_GRAVE_ACCENT.getGLFWConstant()) { return KeyPress.KEY_GRAVE_ACCENT;}
            if (key== KeyPress.KEY_WORLD_1.getGLFWConstant()) { return KeyPress.KEY_WORLD_1;}
            if (key== KeyPress.KEY_WORLD_2.getGLFWConstant()) { return KeyPress.KEY_WORLD_2;}
            if (key== KeyPress.KEY_ESCAPE.getGLFWConstant()) { return KeyPress.KEY_ESCAPE;}
            if (key== KeyPress.KEY_ENTER.getGLFWConstant()) { return KeyPress.KEY_ENTER;}
            if (key== KeyPress.KEY_TAB.getGLFWConstant()) { return KeyPress.KEY_TAB;}
            if (key== KeyPress.KEY_BACKSPACE.getGLFWConstant()) { return KeyPress.KEY_BACKSPACE;}
            if (key== KeyPress.KEY_INSERT.getGLFWConstant()) { return KeyPress.KEY_INSERT;}
            if (key== KeyPress.KEY_DELETE.getGLFWConstant()) { return KeyPress.KEY_DELETE;}
            if (key== KeyPress.KEY_RIGHT.getGLFWConstant()) { return KeyPress.KEY_RIGHT;}
            if (key== KeyPress.KEY_LEFT.getGLFWConstant()) { return KeyPress.KEY_LEFT;}
            if (key== KeyPress.KEY_DOWN.getGLFWConstant()) { return KeyPress.KEY_DOWN;}
            if (key== KeyPress.KEY_UP.getGLFWConstant()) { return KeyPress.KEY_UP;}
            if (key== KeyPress.KEY_PAGE_UP.getGLFWConstant()) { return KeyPress.KEY_PAGE_UP;}
            if (key== KeyPress.KEY_PAGE_DOWN.getGLFWConstant()) { return KeyPress.KEY_PAGE_DOWN;}
            if (key== KeyPress.KEY_HOME.getGLFWConstant()) { return KeyPress.KEY_HOME;}
            if (key== KeyPress.KEY_END.getGLFWConstant()) { return KeyPress.KEY_END;}
            if (key== KeyPress.KEY_CAPS_LOCK.getGLFWConstant()) { return KeyPress.KEY_CAPS_LOCK;}
            if (key== KeyPress.KEY_SCROLL_LOCK.getGLFWConstant()) { return KeyPress.KEY_SCROLL_LOCK;}
            if (key== KeyPress.KEY_NUM_LOCK.getGLFWConstant()) { return KeyPress.KEY_NUM_LOCK;}
            if (key== KeyPress.KEY_PRINT_SCREEN.getGLFWConstant()) { return KeyPress.KEY_PRINT_SCREEN;}
            if (key== KeyPress.KEY_PAUSE.getGLFWConstant()) { return KeyPress.KEY_PAUSE;}
            if (key== KeyPress.KEY_F1.getGLFWConstant()) { return KeyPress.KEY_F1;}
            if (key== KeyPress.KEY_F2.getGLFWConstant()) { return KeyPress.KEY_F2;}
            if (key== KeyPress.KEY_F3.getGLFWConstant()) { return KeyPress.KEY_F3;}
            if (key== KeyPress.KEY_F4.getGLFWConstant()) { return KeyPress.KEY_F4;}
            if (key== KeyPress.KEY_F5.getGLFWConstant()) { return KeyPress.KEY_F5;}
            if (key== KeyPress.KEY_F6.getGLFWConstant()) { return KeyPress.KEY_F6;}
            if (key== KeyPress.KEY_F7.getGLFWConstant()) { return KeyPress.KEY_F7;}
            if (key== KeyPress.KEY_F8.getGLFWConstant()) { return KeyPress.KEY_F8;}
            if (key== KeyPress.KEY_F9.getGLFWConstant()) { return KeyPress.KEY_F9;}
            if (key== KeyPress.KEY_F10.getGLFWConstant()) { return KeyPress.KEY_F10;}
            if (key== KeyPress.KEY_F11.getGLFWConstant()) { return KeyPress.KEY_F11;}
            if (key== KeyPress.KEY_F12.getGLFWConstant()) { return KeyPress.KEY_F12;}
            if (key== KeyPress.KEY_F13.getGLFWConstant()) { return KeyPress.KEY_F13;}
            if (key== KeyPress.KEY_F14.getGLFWConstant()) { return KeyPress.KEY_F14;}
            if (key== KeyPress.KEY_F15.getGLFWConstant()) { return KeyPress.KEY_F15;}
            if (key== KeyPress.KEY_F16.getGLFWConstant()) { return KeyPress.KEY_F16;}
            if (key== KeyPress.KEY_F17.getGLFWConstant()) { return KeyPress.KEY_F17;}
            if (key== KeyPress.KEY_F18.getGLFWConstant()) { return KeyPress.KEY_F18;}
            if (key== KeyPress.KEY_F19.getGLFWConstant()) { return KeyPress.KEY_F19;}
            if (key== KeyPress.KEY_F20.getGLFWConstant()) { return KeyPress.KEY_F20;}
            if (key== KeyPress.KEY_F21.getGLFWConstant()) { return KeyPress.KEY_F21;}
            if (key== KeyPress.KEY_F22.getGLFWConstant()) { return KeyPress.KEY_F22;}
            if (key== KeyPress.KEY_F23.getGLFWConstant()) { return KeyPress.KEY_F23;}
            if (key== KeyPress.KEY_F24.getGLFWConstant()) { return KeyPress.KEY_F24;}
            if (key== KeyPress.KEY_F25.getGLFWConstant()) { return KeyPress.KEY_F25;}
            if (key== KeyPress.KEY_KP_0.getGLFWConstant()) { return KeyPress.KEY_KP_0;}
            if (key== KeyPress.KEY_KP_1.getGLFWConstant()) { return KeyPress.KEY_KP_1;}
            if (key== KeyPress.KEY_KP_2.getGLFWConstant()) { return KeyPress.KEY_KP_2;}
            if (key== KeyPress.KEY_KP_3.getGLFWConstant()) { return KeyPress.KEY_KP_3;}
            if (key== KeyPress.KEY_KP_4.getGLFWConstant()) { return KeyPress.KEY_KP_4;}
            if (key== KeyPress.KEY_KP_5.getGLFWConstant()) { return KeyPress.KEY_KP_5;}
            if (key== KeyPress.KEY_KP_6.getGLFWConstant()) { return KeyPress.KEY_KP_6;}
            if (key== KeyPress.KEY_KP_7.getGLFWConstant()) { return KeyPress.KEY_KP_7;}
            if (key== KeyPress.KEY_KP_8.getGLFWConstant()) { return KeyPress.KEY_KP_8;}
            if (key== KeyPress.KEY_KP_9.getGLFWConstant()) { return KeyPress.KEY_KP_9;}
            if (key== KeyPress.KEY_KP_DECIMAL.getGLFWConstant()) { return KeyPress.KEY_KP_DECIMAL;}
            if (key== KeyPress.KEY_KP_DIVIDE.getGLFWConstant()) { return KeyPress.KEY_KP_DIVIDE;}
            if (key== KeyPress.KEY_KP_MULTIPLY.getGLFWConstant()) { return KeyPress.KEY_KP_MULTIPLY;}
            if (key== KeyPress.KEY_KP_SUBTRACT.getGLFWConstant()) { return KeyPress.KEY_KP_SUBTRACT;}
            if (key== KeyPress.KEY_KP_ADD.getGLFWConstant()) { return KeyPress.KEY_KP_ADD;}
            if (key== KeyPress.KEY_KP_ENTER.getGLFWConstant()) { return KeyPress.KEY_KP_ENTER;}
            if (key== KeyPress.KEY_KP_EQUAL.getGLFWConstant()) { return KeyPress.KEY_KP_EQUAL;}
            if (key== KeyPress.KEY_LEFT_SHIFT.getGLFWConstant()) { return KeyPress.KEY_LEFT_SHIFT;}
            if (key== KeyPress.KEY_LEFT_CONTROL.getGLFWConstant()) { return KeyPress.KEY_LEFT_CONTROL;}
            if (key== KeyPress.KEY_LEFT_ALT.getGLFWConstant()) { return KeyPress.KEY_LEFT_ALT;}
            if (key== KeyPress.KEY_LEFT_SUPER.getGLFWConstant()) { return KeyPress.KEY_LEFT_SUPER;}
            if (key== KeyPress.KEY_RIGHT_SHIFT.getGLFWConstant()) { return KeyPress.KEY_RIGHT_SHIFT;}
            if (key== KeyPress.KEY_RIGHT_CONTROL.getGLFWConstant()) { return KeyPress.KEY_RIGHT_CONTROL;}
            if (key== KeyPress.KEY_RIGHT_ALT.getGLFWConstant()) { return KeyPress.KEY_RIGHT_ALT;}
            if (key== KeyPress.KEY_RIGHT_SUPER.getGLFWConstant()) { return KeyPress.KEY_RIGHT_SUPER;}
            if (key== KeyPress.KEY_MENU.getGLFWConstant()) { return KeyPress.KEY_MENU;}
            if (key== KeyPress.KEY_LAST.getGLFWConstant()) { return KeyPress.KEY_LAST;}
            return KeyPress.KEY_UNKNOWN;
        }
    }

    public class MouseMovedListenerManager implements GLFWCursorPosCallbackI {

        @Override
        public void invoke(long window, double xpos, double ypos) {
            if (!mouseListeners.isEmpty()){
                for (MouseListener listener: mouseListeners){
                    listener.mouseMoved(xpos, ypos);
                }
            }
        }
    }

    public class MouseClickedListenerManager implements GLFWMouseButtonCallbackI {

        @Override
        public void invoke(long window, int button, int action, int mods) {
            if (!mouseListeners.isEmpty()){
                for (MouseListener listener: mouseListeners){

                    double[] xpos = new double[1];
                    double[] ypos = new double[1];

                    glfwGetCursorPos(window, xpos, ypos);

                    if (action == GLFW_PRESS)
                        listener.mousePressed(glfwConstant2mousePress(button), mods, xpos[0], ypos[0]);
                    else listener.mouseReleased(glfwConstant2mousePress(button), mods, xpos[0], ypos[0]);
                }
            }
        }

        private MousePress glfwConstant2mousePress(int button){
            if (button== MousePress.MOUSE_BUTTON_1.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_1;}
            if (button== MousePress.MOUSE_BUTTON_2.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_2;}
            if (button== MousePress.MOUSE_BUTTON_3.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_3;}
            if (button== MousePress.MOUSE_BUTTON_4.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_4;}
            if (button== MousePress.MOUSE_BUTTON_5.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_5;}
            if (button== MousePress.MOUSE_BUTTON_6.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_6;}
            if (button== MousePress.MOUSE_BUTTON_7.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_7;}
            if (button== MousePress.MOUSE_BUTTON_8.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_8;}
            if (button== MousePress.MOUSE_BUTTON_LAST.getGLFWConstant()) { return MousePress.MOUSE_BUTTON_LAST;}
            return null;
        }
    }

    public class CharTypedListenerManager implements GLFWCharCallbackI{
        @Override
        public void invoke(long window, int codepoint) {
            if (!keyboardListeners.isEmpty()){
                for (KeyboardListener listener: keyboardListeners){
                    listener.charTyped(codepoint);
                }
            }
        }
    }

    public class MouseWheelMovedListenerManager implements GLFWScrollCallbackI{

        @Override
        public void invoke(long window, double xoffset, double yoffset) {
            if (!mouseListeners.isEmpty()){
                for (MouseListener listener: mouseListeners){
                    listener.mouseWheelMoved(xoffset, yoffset);
                }
            }
        }
    }

    public class WindowResizeListenerManager implements GLFWFramebufferSizeCallbackI{

        @Override
        public void invoke(long window, int width, int height) {
            if (width != 0 && height != 0 /*fullscreen related check*/) {
                Framebuffer.useDefaultFrameBuffer();
                glViewport(0, 0, width, height);
                MainFrame.this.width = width;
                MainFrame.this.height = height;
                Framebuffer.DEFAULT_FRAMEBUFFER.externalResize(width, height);
                if (!windowResizeListeners.isEmpty()) {
                    for (WindowResizeListener windowResizeListener : windowResizeListeners) {
                        windowResizeListener.windowResize(width, height);
                    }
                }
            }
        }
    }

    private final long window;
    private int width;
    private int height;

    private final STGLGraphics graphics;

    private boolean isFullscreen = false;

    private final ArrayList<KeyboardListener> keyboardListeners = new ArrayList<>();
    private final ArrayList<MouseListener> mouseListeners = new ArrayList<>();
    private final ArrayList<WindowResizeListener> windowResizeListeners = new ArrayList<>();
    private final KeyboardListenerManager keyboardListenerManager = new KeyboardListenerManager();
    private final MouseClickedListenerManager mouseClickedListenerManager = new MouseClickedListenerManager();
    private final MouseMovedListenerManager mouseMovedListenerManager = new MouseMovedListenerManager();
    private final CharTypedListenerManager charTypedListenerManager = new CharTypedListenerManager();

    private final MouseWheelMovedListenerManager mouseWheelMovedListenerManager = new MouseWheelMovedListenerManager();

    private final WindowResizeListenerManager windowResizeListenerManager = new WindowResizeListenerManager();

    public static class FrameConfig{
        private int MSAASamples = -1;
        private boolean MSAAEnabled = false;

        private boolean vsync = true;

        private boolean resizable = true;

        private ByteBuffer icon = null;
        private int icon_width = -1;
        private int icon_height = -1;

        public FrameConfig(){

        }

        /***
         * Enable MSAA anti-aliasing
         */
        public FrameConfig enableMSAA(int samples){
            MSAASamples = samples;
            MSAAEnabled = true;
            return this;
        }

        public FrameConfig disableVSync(){
            vsync = false;
            return this;
        }

        public FrameConfig setResizable(boolean resizable){
            this.resizable = resizable;
            return this;
        }

        public FrameConfig setIcon(ByteBuffer image, int width, int height){
            icon = image;
            this.icon_width = width;
            this.icon_height = height;
            return this;
        }

        public FrameConfig setIcon(BufferedImage image){
            return setIcon(Texture.toByteBuffer(image), image.getWidth(), image.getHeight());
        }

    }

    public MainFrame(int width, int height, String title, FrameConfig frameConfig){
        this.width = width;
        this.height = height;

        // Set up an error callback.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, frameConfig.resizable?GLFW_TRUE:GLFW_FALSE); // the window will be resizable

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        if (frameConfig.MSAAEnabled){
            // MSAA Anti aliasing
            glfwWindowHint(GLFW_SAMPLES, frameConfig.MSAASamples);
        }


        // Create the window
        window = glfwCreateWindow(width, height, title, NULL, NULL);

        if (frameConfig.icon != null){
            GLFWImage image = GLFWImage.malloc();
            image.set(frameConfig.icon_width, frameConfig.icon_height, frameConfig.icon);
            GLFWImage.Buffer images = GLFWImage.malloc(1);
            images.put(0, image);

            glfwSetWindowIcon(window, images);

            images.free();
            image.free();
        }

        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        if (frameConfig.vsync)
            glfwSwapInterval(1);
        else glfwSwapInterval(0);

        // Initialize OpenGL
        GL.createCapabilities();

        Framebuffer.DEFAULT_FRAMEBUFFER = new Framebuffer(width, height, 0);

        graphics = new STGLGraphics(this);

        // Now enable it in OpenGL if necessary
        if (frameConfig.MSAAEnabled)
            glEnable(GL_MULTISAMPLE);

        // Enable blending
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Add listener managers
        glfwSetKeyCallback(window, keyboardListenerManager);
        glfwSetCursorPosCallback(window, mouseMovedListenerManager);
        glfwSetMouseButtonCallback(window, mouseClickedListenerManager);
        glfwSetCharCallback(window, charTypedListenerManager );
        glfwSetScrollCallback(window,  mouseWheelMovedListenerManager);
        glfwSetFramebufferSizeCallback(window, windowResizeListenerManager);

        glViewport(0,0,width,height);
    }

    public void show(){
        // Make the window visible
        glfwShowWindow(window);
    }

    public void addKeyboardListener(KeyboardListener keyboardListener){
        keyboardListeners.add(keyboardListener);
    }

    public void removeKeyboardListener(KeyboardListener keyboardListener){
        keyboardListeners.remove(keyboardListener);
    }

    public void clearKeyboardListeners(){
        keyboardListeners.clear();
    }

    public void addMouseListener(MouseListener keyboardListener){
        mouseListeners.add(keyboardListener);
    }

    public void removeMouseListener(MouseListener keyboardListener){
        mouseListeners.remove(keyboardListener);
    }

    public void clearMouseListeners(){
        mouseListeners.clear();
    }

    public void addWindowResizeListener(WindowResizeListener windowResizeListener){
        windowResizeListeners.add(windowResizeListener);
    }

    public void removeWindowResizeListener(WindowResizeListener windowResizeListener){
        windowResizeListeners.remove(windowResizeListener);
    }

    public void close(){
        glfwSetWindowShouldClose(window, true);
    }

    /***
     * This is messed up if user display scaling enabled on windows
     */
    public void setWindowPos(int x, int y){
        glfwSetWindowPos(window, x, y);
    }

    /***
     * After using the window dispose it
     * <br>
     * (WARNING: For some reason glfwDestroyWindow and
     * glfwTerminate causes an OpenGL error I didn't find a way to fix it)
     */
    public void dispose(){
        GlobalState.defaultResourceManager.dispose();
        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    public boolean shouldClose(){
        return glfwWindowShouldClose(window);
    }

    public void update(){
        graphics.useCorrectRenderingTarget();
        glfwSwapBuffers(window); // swap the color buffers

        // Poll for window events.
        glfwPollEvents();
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public STGLGraphics getGraphics() {
        return graphics;
    }

    public void setFullscreen(VideoMode videoMode, Monitor monitor){
        isFullscreen = true;
        Monitor m = Monitors.getPrimaryMonitor();
        glfwSetWindowMonitor(window, m.monitor(), 0, 0, videoMode.width(), videoMode.height(), videoMode.refreshRate());
    }

    public void setWindowed(int width, int height){
        glfwSetWindowMonitor(window, NULL, 32, 32, width, height, GLFW_DONT_CARE);
        isFullscreen = false;
    }

    public boolean isFullscreen() {
        return isFullscreen;
    }

    private boolean showCursor = false;

    public boolean isShowCursor() {
        return showCursor;
    }

    public void setShowCursor(boolean showCursor) {
        this.showCursor = showCursor;
        if (showCursor)
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        else
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}
