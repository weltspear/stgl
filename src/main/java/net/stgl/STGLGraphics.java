package net.stgl;

import net.stgl.color.STGLColors;
import net.stgl.debug.ErrorCheck;
import net.stgl.font.STGLFont;
import net.stgl.font.Glyph;
import net.stgl.image.STGLImage;
import net.stgl.shader.Shader;
import net.stgl.shader.ShaderProgram;
import net.stgl.state.GlobalState;
import net.stgl.state.GlobalTextureSlotManager;
import net.stgl.text.STGLTextFormatter;
import net.stgl.texture.ArrayTexture;
import net.stgl.texture.ArrayTextureRegion;
import net.stgl.texture.ITexture;
import net.stgl.texture.TextureRegion;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector2d;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL32;

import java.util.List;

import static org.lwjgl.opengl.GL32.*;

/***
 * This directly uses OpenGL calls to draw stuff you want it to draw.
 */
@SuppressWarnings("unused")
public class STGLGraphics {

    private final MainFrame frame;

    // primitive stuff
    private final ShaderProgram primitive_shader_program;
    private final Framebuffer framebuffer;
    @SuppressWarnings("all")
    private Shader vshader_primitive;
    @SuppressWarnings("all")
    private Shader fshader_primitive;
    private final int primitive_vao;

    // textured stuff

    private final ShaderProgram textured_shader_program;

    @SuppressWarnings("all")
    private final Shader vshader_texture;
    @SuppressWarnings("all")
    private final Shader fshader_texture;

    private final int textured_vao;

    // gradient stuff

    private final ShaderProgram gradient_shader_program;
    @SuppressWarnings("all")

    private final Shader vshader_gradient;
    @SuppressWarnings("all")
    private final Shader fshader_gradient;

    private final int gradient_vao;

    private final int texture_array_vao;

    // font stuff

    private final ShaderProgram font_shader_program;

    @SuppressWarnings("all")
    private final Shader vshader_font;
    @SuppressWarnings("all")
    private final Shader fshader_font;

    // texture array stuff

    private final ShaderProgram tarray_shader_program;
    private final Shader vshader_tarray;
    @SuppressWarnings("all")
    private final Shader fshader_tarray;

    private final int font_vao;

    // blur texture stuff

    private final ShaderProgram blur_textured_shader_program;

    private final Shader vshader_blur_texture;
    private final Shader fshader_blur_texture;

    private final int blur_textured_vao;

    // global
    private final int global_vbo;

    @SuppressWarnings("all")
    private final int global_ebo;

    private final int utexture_color;

    private final int utexture_array_color;

    private final int utexture_array_sampler2d;
    private final int uprimitive_color;
    private final int utext_color;

    private final int utexture_sampler2d;
    private final int ufont_sampler2d;

    private final int utexture_blur_sampler2d;

    private final int utexture_blur_blur_u;
    private final int utexture_blur_blur_v;

    private boolean doScissors = false;
    private int scissorX = 0;
    private int scissorY = 0;
    private int scissorWidth = 0;
    private int scissorHeight = 0;
    private Vector2i translationVector = new Vector2i(0, 0);

    private STGLGraphics parent = null;

    public STGLGraphics(MainFrame frame, Framebuffer framebuffer){
        this.frame = frame;
        this.framebuffer = framebuffer;

        // global vbo

        global_vbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);

        global_ebo = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, global_ebo);

        // primitive shader and stuff

        if (GlobalState.defaultResourceManager.getShaderProgram("primitive") == null) {
            primitive_shader_program = new ShaderProgram();
            vshader_primitive = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_primitive.vert", GL_VERTEX_SHADER);
            fshader_primitive = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_primitive.frag", GL_FRAGMENT_SHADER);
            primitive_shader_program.attachShader(vshader_primitive);
            primitive_shader_program.attachShader(fshader_primitive);
            primitive_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("stgl_primitive", primitive_shader_program);
        }
        else{
            primitive_shader_program = GlobalState.defaultResourceManager.getShaderProgram("primitve");
            vshader_primitive = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_primitive.vert", GL_VERTEX_SHADER);
            fshader_primitive = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_primitive.frag", GL_FRAGMENT_SHADER);
        }

        primitive_vao = glGenVertexArrays();
        glBindVertexArray(primitive_vao);
        int pos = glGetAttribLocation(primitive_shader_program.getId(), "pos");
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(pos);

        //

        if (GlobalState.defaultResourceManager.getShaderProgram("texture") == null) {
            textured_shader_program = new ShaderProgram();
            vshader_texture = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_texture.vert", GL_VERTEX_SHADER);
            fshader_texture = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_texture.frag", GL_FRAGMENT_SHADER);
            textured_shader_program.attachShader(vshader_texture);
            textured_shader_program.attachShader(fshader_texture);
            textured_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("texture", textured_shader_program);
        }
        else {
            textured_shader_program = GlobalState.defaultResourceManager.getShaderProgram("texture");
            vshader_texture = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_texture.vert", GL_VERTEX_SHADER);
            fshader_texture = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_texture.frag", GL_FRAGMENT_SHADER);
        }

        textured_vao = glGenVertexArrays();
        glBindVertexArray(textured_vao);
        pos = glGetAttribLocation(textured_shader_program.getId(), "pos");
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 4 * 4, 0);
        glEnableVertexAttribArray(pos);

        int texpos = glGetAttribLocation(textured_shader_program.getId(), "texpos");
        glVertexAttribPointer(texpos, 2, GL_FLOAT, false, 4 * 4, 2 * 4);
        glEnableVertexAttribArray(texpos);

        // gradient stuff

        if (GlobalState.defaultResourceManager.getShaderProgram("gradient") == null) {
            gradient_shader_program = new ShaderProgram();
            vshader_gradient = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_gradient.vert", GL_VERTEX_SHADER);
            fshader_gradient = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_gradient.frag", GL_FRAGMENT_SHADER);
            gradient_shader_program.attachShader(vshader_gradient);
            gradient_shader_program.attachShader(fshader_gradient);
            gradient_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("gradient", gradient_shader_program);
        }
        else{
            gradient_shader_program = GlobalState.defaultResourceManager.getShaderProgram("gradient");
            vshader_gradient = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_gradient.vert", GL_VERTEX_SHADER);
            fshader_gradient = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_gradient.frag", GL_FRAGMENT_SHADER);
        }

        gradient_vao = glGenVertexArrays();
        glBindVertexArray(gradient_vao);
        pos = glGetAttribLocation(gradient_shader_program.getId(), "pos");
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 6 * 4, 0);
        glEnableVertexAttribArray(pos);

        int color = glGetAttribLocation(gradient_shader_program.getId(), "color");
        glVertexAttribPointer(color, 4, GL_FLOAT, false, 6 * 4, 2 * 4);
        glEnableVertexAttribArray(color);

        // font stuff

        if (GlobalState.defaultResourceManager.getShaderProgram("font") == null) {
            font_shader_program = new ShaderProgram();
            vshader_font = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_font.vert", GL_VERTEX_SHADER);
            fshader_font = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_font.frag", GL_FRAGMENT_SHADER);
            font_shader_program.attachShader(vshader_font);
            font_shader_program.attachShader(fshader_font);
            font_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("font", font_shader_program);
        }
        else{
            font_shader_program = GlobalState.defaultResourceManager.getShaderProgram("font");
            vshader_font = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_font.vert", GL_VERTEX_SHADER);
            fshader_font = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_font.frag", GL_FRAGMENT_SHADER);
        }

        font_vao = glGenVertexArrays();
        glBindVertexArray(font_vao);
        pos = glGetAttribLocation(font_shader_program.getId(), "pos");
        glVertexAttribPointer(pos, 4, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(pos);

        //

        utexture_color = glGetUniformLocation(textured_shader_program.getId(), "color");
        uprimitive_color = glGetUniformLocation(primitive_shader_program.getId(), "color");
        utext_color = glGetUniformLocation(font_shader_program.getId(), "textColor");

        ufont_sampler2d = glGetUniformLocation(font_shader_program.getId(), "text");
        utexture_sampler2d = glGetUniformLocation(textured_shader_program.getId(), "tex");

        ErrorCheck.checkErrorGL();

        // tarray

        if (GlobalState.defaultResourceManager.getShaderProgram("tarray") == null) {
            tarray_shader_program = new ShaderProgram();
            vshader_tarray = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_texture_array.vert", GL_VERTEX_SHADER);
            fshader_tarray = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_texture_array.frag", GL_FRAGMENT_SHADER);
            tarray_shader_program.attachShader(vshader_tarray);
            tarray_shader_program.attachShader(fshader_tarray);
            tarray_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("tarray", tarray_shader_program);
        }
        else{
            tarray_shader_program = GlobalState.defaultResourceManager.getShaderProgram("tarray");
            vshader_tarray = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_texture_array.vert", GL_VERTEX_SHADER);
            fshader_tarray = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_texture_array.frag", GL_FRAGMENT_SHADER);
        }

        texture_array_vao = glGenVertexArrays();

        ErrorCheck.checkErrorGL();
        glBindVertexArray(texture_array_vao);
        ErrorCheck.checkErrorGL();
        pos = glGetAttribLocation(tarray_shader_program.getId(), "pos");
        ErrorCheck.checkErrorGL();
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 5 * 4, 0);
        ErrorCheck.checkErrorGL();
        glEnableVertexAttribArray(pos);

        ErrorCheck.checkErrorGL();

        texpos = glGetAttribLocation(tarray_shader_program.getId(), "texpos");
        glVertexAttribPointer(texpos, 2, GL_FLOAT, false, 5 * 4, 2 * 4);
        glEnableVertexAttribArray(texpos);

        ErrorCheck.checkErrorGL();

        int layer = glGetAttribLocation(tarray_shader_program.getId(), "layer");
        glVertexAttribPointer(layer, 1, GL_FLOAT, false, 5 * 4, 4 * 4);
        glEnableVertexAttribArray(layer);

        utexture_array_color = glGetUniformLocation(tarray_shader_program.getId(), "color");
        utexture_array_sampler2d = glGetUniformLocation(tarray_shader_program.getId(), "tex");

        ErrorCheck.checkErrorGL();

        // texture_blur

        if (GlobalState.defaultResourceManager.getShaderProgram("texture_blur") == null) {
            blur_textured_shader_program = new ShaderProgram();
            vshader_blur_texture = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_blur.vert", GL_VERTEX_SHADER);
            fshader_blur_texture = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_blur.frag", GL_FRAGMENT_SHADER);
            blur_textured_shader_program.attachShader(vshader_blur_texture);
            blur_textured_shader_program.attachShader(fshader_blur_texture);
            blur_textured_shader_program.link();
            GlobalState.defaultResourceManager.storeShaderProgram("texture_blur", blur_textured_shader_program);
        }
        else {
            blur_textured_shader_program = GlobalState.defaultResourceManager.getShaderProgram("texture_blur");
            vshader_blur_texture = GlobalState.defaultResourceManager.loadShader("/shaders/vshader_blur.vert", GL_VERTEX_SHADER);
            fshader_blur_texture = GlobalState.defaultResourceManager.loadShader("/shaders/fshader_blur.frag", GL_FRAGMENT_SHADER);
        }

        blur_textured_vao = glGenVertexArrays();
        glBindVertexArray(blur_textured_vao);
        pos = glGetAttribLocation(blur_textured_shader_program.getId(), "pos");
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 4 * 4, 0);
        glEnableVertexAttribArray(pos);
        ErrorCheck.checkErrorGL();

        texpos = glGetAttribLocation(blur_textured_shader_program.getId(), "texpos");
        glVertexAttribPointer(texpos, 2, GL_FLOAT, false, 4 * 4, 2 * 4);
        glEnableVertexAttribArray(texpos);
        utexture_blur_sampler2d = glGetUniformLocation(blur_textured_shader_program.getId(), "tex");
        utexture_blur_blur_u = glGetUniformLocation(blur_textured_shader_program.getId(), "blurSizeU");
        utexture_blur_blur_v = glGetUniformLocation(blur_textured_shader_program.getId(), "blurSizeV");
        ErrorCheck.checkErrorGL();
    }

    private STGLGraphics(STGLGraphics parent){
        this.framebuffer = parent.framebuffer != null ? parent.framebuffer : Framebuffer.DEFAULT_FRAMEBUFFER;
        this.frame = null;
        this.primitive_shader_program = parent.primitive_shader_program;
        this.primitive_vao = parent.primitive_vao;
        this.textured_shader_program = parent.textured_shader_program;
        this.vshader_texture = parent.vshader_texture;
        this.fshader_texture = parent.fshader_texture;
        this.textured_vao = parent.textured_vao;
        this.gradient_shader_program = parent.gradient_shader_program;
        this.vshader_gradient = parent.vshader_gradient;
        this.fshader_gradient = parent.fshader_gradient;
        this.gradient_vao = parent.gradient_vao;
        this.font_shader_program = parent.font_shader_program;
        this.vshader_font = parent.vshader_font;
        this.fshader_font = parent.fshader_font;
        this.font_vao = parent.font_vao;
        this.global_vbo = parent.global_vbo;
        this.global_ebo = parent.global_ebo;
        this.utexture_color = parent.utexture_color;
        this.uprimitive_color = parent.uprimitive_color;
        this.utext_color = parent.utext_color;
        this.utexture_sampler2d = parent.utexture_sampler2d;
        this.ufont_sampler2d = parent.ufont_sampler2d;
        this.tarray_shader_program = parent.tarray_shader_program;
        this.vshader_tarray = parent.vshader_tarray;
        this.fshader_tarray = parent.fshader_tarray;
        this.texture_array_vao = parent.texture_array_vao;
        this.utexture_array_color = parent.utexture_array_color;
        this.utexture_array_sampler2d = parent.utexture_array_sampler2d;
        this.blur_textured_shader_program = parent.blur_textured_shader_program;
        this.vshader_blur_texture = parent.vshader_blur_texture;
        this.fshader_blur_texture = parent.fshader_blur_texture;
        this.blur_textured_vao = parent.blur_textured_vao;
        this.utexture_blur_sampler2d = parent.utexture_blur_sampler2d;
        this.utexture_blur_blur_u = parent.utexture_blur_blur_u;
        this.utexture_blur_blur_v = parent.utexture_blur_blur_v;
        this.parent = parent;
    }

    public STGLGraphics(MainFrame frame){
        this(frame, null);
    }

    public STGLGraphics(Framebuffer buffer){
        this(null, buffer);
    }

    private int getCorrectMaxWidth(){
        return frame != null ? frame.getWidth() : framebuffer.getWidth();
    }

    private int getCorrectMaxHeight(){
        return frame != null ? frame.getHeight() : framebuffer.getHeight();
    }

    /***
     * Binds correct Framebuffer, sets up correct scissor box and viewport.
     */
    public void useCorrectRenderingTarget(){
        if (framebuffer == null){
            Framebuffer.useDefaultFrameBuffer();
            glViewport(0,0,frame.getWidth(),frame.getHeight());
        }
        else{
            framebuffer.useThis();
        }

        if (doScissors){
            activateScissorBoxGL(scissorX, scissorY, scissorWidth, scissorHeight);
        }
        else{
            glDisable(GL_SCISSOR_TEST);
            if (parent != null)
                parent.useCorrectRenderingTarget();
        }
    }

    /***
     * @return Translation vector from framebuffer origin
     */
    private Vector2i getTranslationVectorOrigin(){
        if (parent != null) {
            Vector2i v = new Vector2i();
            v.add(translationVector).add(parent.getTranslationVectorOrigin());
            return v;
        }
        return translationVector;
    }

    public float toHardwareX(int x){
        return toHardwareX((float) x);
    }

    public float toHardwareY(int y){
        return toHardwareY((float) y);
    }

    public float toHardwareX(float x){
        return ((x+getTranslationVectorOrigin().x)/(getCorrectMaxWidth()/2f))-1.0f;
    }

    public float toHardwareY(float y){
        return -((y+getTranslationVectorOrigin().y)/(getCorrectMaxHeight()/2f))+1.0f;
    }

    public void drawLine(int x1, int y1, int x2, int y2, Vector4f color, int thickness){
        useCorrectRenderingTarget();
        if (thickness == 1) {
            primitive_shader_program.useThis();
            glBindVertexArray(primitive_vao);

            glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

            float[] vertices = new float[]{
                    toHardwareX(x1), toHardwareY(y1),
                    toHardwareX(x2), toHardwareY(y2),
            };

            glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
            glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                    , GL32.GL_STREAM_DRAW);

            glDrawArrays(GL_LINES, 0, 2);
        }
        else{
            Vector2d initial = new Vector2d(x2-x1, y2-y1);
            Vector2d normalized = new Vector2d(initial.x, initial.y).div(initial.length());

            // orthogonal vector to initial
            @SuppressWarnings("all")
            Vector2d to_next_line_start = new Vector2d(-normalized.y , normalized.x).mul(thickness);

            Vector2d nextp1 = new Vector2d(x1, y1).add(to_next_line_start);
            Vector2d nextp2 = new Vector2d(x2, y2).add(to_next_line_start);

            primitive_shader_program.useThis();
            glBindVertexArray(primitive_vao);

            glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

            float[] vertices = new float[]{
                    toHardwareX(x1), toHardwareY(y1),
                    toHardwareX((float) nextp1.x), toHardwareY((float) nextp1.y),
                    toHardwareX(x2), toHardwareY(y2),
                    toHardwareX((float) nextp2.x), toHardwareY((float) nextp2.y),
            };

            glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
            glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                    , GL32.GL_STREAM_DRAW);

            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        }

    }

    public void drawLine(int x1, int y1, int x2, int y2, Vector4f color){
        useCorrectRenderingTarget();
        drawLine(x1, y1, x2, y2, color, 1);
    }

    public void fillRect(int x, int y, int width, int height, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y),
                toHardwareX(x+width), toHardwareY(y),
                toHardwareX(x), toHardwareY(y+height),
                toHardwareX(x+width), toHardwareY(y+height),
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    }

    public void fillRectGradient(int x, int y, int width, int height, Vector4f color1, Vector4f color2){
        useCorrectRenderingTarget();
        gradient_shader_program.useThis();
        glBindVertexArray(gradient_vao);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), color1.x, color1.y, color1.z, color1.w,
                toHardwareX(x+width), toHardwareY(y), color2.x, color2.y, color2.z, color2.w,
                toHardwareX(x), toHardwareY(y+height), color1.x, color1.y, color1.z, color1.w,
                toHardwareX(x+width), toHardwareY(y+height), color2.x, color2.y, color2.z, color2.w,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    }

    public void drawTextureScaled(int x, int y, ITexture t, Vector4f color, int scale_width, int scale_height){
        useCorrectRenderingTarget();
        textured_shader_program.useThis();
        glBindVertexArray(textured_vao);

        glUniform4f(utexture_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), 0.0f, 0.0f,
                toHardwareX(x+scale_width), toHardwareY(y), 1.0f, 0.0f,
                toHardwareX(x), toHardwareY(y+scale_height), 0.0f, 1.0f,
                toHardwareX(x+scale_width), toHardwareY(y+scale_height), 1.0f, 1.0f,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        int slot = GlobalTextureSlotManager.useTexture(t.getId());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_sampler2d, slot);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();

    }

    public void drawArrayTextureScaled(int x, int y, int layer, ArrayTexture t, Vector4f color, int scale_width, int scale_height){
        if (layer >= t.getLayers()){
            throw new IllegalArgumentException("layer is greater than layers");
        }

        useCorrectRenderingTarget();
        tarray_shader_program.useThis();
        glBindVertexArray(texture_array_vao);

        glUniform4f(utexture_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), 0.0f, 0.0f, layer,
                toHardwareX(x+scale_width), toHardwareY(y), 1.0f, 0.0f, layer,
                toHardwareX(x), toHardwareY(y+scale_height), 0.0f, 1.0f,layer,
                toHardwareX(x+scale_width), toHardwareY(y+scale_height), 1.0f, 1.0f,layer,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        int slot = GlobalTextureSlotManager.useTexture(t.getId());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_sampler2d, slot);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();

    }

    public void drawArrayTexture(int x, int y, int layer, ArrayTexture t, Vector4f color){
        drawArrayTextureScaled(x, y, layer, t, color, t.getWidth(), t.getHeight());
    }

    public void drawArrayTexture(int x, int y, int layer, ArrayTexture t){
        drawArrayTexture(x, y, layer, t, STGLColors.WHITE);
    }

    public void drawTexture(int x, int y, ITexture t, Vector4f color){
        drawTextureScaled(x, y, t, color, t.getWidth(), t.getHeight());
    }

    public void drawTexture(int x, int y, ITexture t){
        drawTextureScaled(x, y, t, STGLColors.WHITE, t.getWidth(), t.getHeight());
    }

    public void drawImage(int x, int y, @NotNull STGLImage image){
        drawTextureScaled(x, y, image.getTexture(), new Vector4f(1,1,1,1), image.getTexture().getWidth(), image.getTexture().getHeight());
    }

    public void drawImageScaled(int x, int y, @NotNull STGLImage image, int width, int height){
        drawTextureScaled(x, y, image.getTexture(), new Vector4f(1,1,1,1), width, height);
    }

    public void drawTextureRegionScaled(int x, int y, @NotNull TextureRegion t, @NotNull Vector4f color, int scale_width, int scale_height){
        useCorrectRenderingTarget();
        textured_shader_program.useThis();
        glBindVertexArray(textured_vao);

        int slot = GlobalTextureSlotManager.useTexture(t.getTextureResource().getId());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_sampler2d, slot);

        glUniform4f(utexture_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), t.getTextureOffsetX1(), t.getTextureOffsetY1(),
                toHardwareX(x+scale_width), toHardwareY(y), t.getTextureOffsetX2(), t.getTextureOffsetY1(),
                toHardwareX(x), toHardwareY(y+scale_height), t.getTextureOffsetX1(), t.getTextureOffsetY2(),
                toHardwareX(x+scale_width), toHardwareY(y+scale_height), t.getTextureOffsetX2(), t.getTextureOffsetY2(),
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();
    }

    public void drawTextureRegionScaled(int x, int y, @NotNull ArrayTextureRegion t, @NotNull Vector4f color, int scale_width, int scale_height){
        useCorrectRenderingTarget();
        tarray_shader_program.useThis();
        glBindVertexArray(texture_array_vao);

        int slot = GlobalTextureSlotManager.useTexture(t.getArrayTexture().getId(), GL_TEXTURE_2D_ARRAY);
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_array_sampler2d, slot);

        glUniform4f(utexture_array_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), t.getTextureOffsetX1(), t.getTextureOffsetY1(), t.getLayer(),
                toHardwareX(x+scale_width), toHardwareY(y), t.getTextureOffsetX2(), t.getTextureOffsetY1(), t.getLayer(),
                toHardwareX(x), toHardwareY(y+scale_height), t.getTextureOffsetX1(), t.getTextureOffsetY2(),t.getLayer(),
                toHardwareX(x+scale_width), toHardwareY(y+scale_height), t.getTextureOffsetX2(), t.getTextureOffsetY2(), t.getLayer(),
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();
    }

    public void drawTextureRegion(int x, int y, TextureRegion t, Vector4f color){
        drawTextureRegionScaled(x, y, t, color, t.getWidth(), t.getHeight());
    }

    public void drawTextureRegion(int x, int y, ArrayTextureRegion t, Vector4f color){
        drawTextureRegionScaled(x, y, t, color, t.getWidth(), t.getHeight());
    }

    /***
     * If Framebuffer has MSAA enabled blit it to non MSAA Framebuffer and call this function
     */
    public void drawFramebufferTexture(int x, int y, @NotNull Framebuffer t, Vector4f color, boolean fixBlending){
        useCorrectRenderingTarget();

        if (fixBlending) {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        }

        if (t.isMSAAEnabled()){
            throw new RuntimeException("MSAA Framebuffer cannot be drawn");
        }

        textured_shader_program.useThis();
        glBindVertexArray(textured_vao);

        glUniform4f(utexture_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), 0.0f, 1.0f,
                toHardwareX(x+t.getWidth()), toHardwareY(y), 1.0f, 1.0f,
                toHardwareX(x), toHardwareY(y+t.getHeight()), 0.0f, 0.0f,
                toHardwareX(x+t.getWidth()), toHardwareY(y+t.getHeight()), 1.0f, 0.0f,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        int slot = GlobalTextureSlotManager.useTexture(t.getFramebufferTextureID());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_sampler2d, slot);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();

        if (fixBlending){
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

    }

    /***
     * If Framebuffer has MSAA enabled blit it to non MSAA Framebuffer and call this function
     */
    public void drawFramebufferTexture(int x, int y, @NotNull Framebuffer t, Vector4f color){
        drawFramebufferTexture(x, y, t, color, true);
    }

    /***
     * Draws a texture and applies blur
     */
    public void miscDrawTextureBlur(int x, int y, ITexture t, int scale_width, int scale_height, float blur_size_u,
                                    float blur_size_v){
        useCorrectRenderingTarget();
        blur_textured_shader_program.useThis();
        glBindVertexArray(blur_textured_vao);

        glUniform1f(utexture_blur_blur_u, blur_size_u);
        glUniform1f(utexture_blur_blur_v, blur_size_v);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), 0.0f, 0.0f,
                toHardwareX(x+scale_width), toHardwareY(y), 1.0f, 0.0f,
                toHardwareX(x), toHardwareY(y+scale_height), 0.0f, 1.0f,
                toHardwareX(x+scale_width), toHardwareY(y+scale_height), 1.0f, 1.0f,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        int slot = GlobalTextureSlotManager.useTexture(t.getId());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_blur_sampler2d, slot);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();
    }

    /***
     * Draws a texture and applies blur
     */
    public void miscDrawTextureBlur(int x, int y, ITexture t, float blur_size_u,
                                    float blur_size_v){
        miscDrawTextureBlur(x, y, t, t.getWidth(), t.getHeight(), blur_size_u, blur_size_v);
    }

    /***
     * If Framebuffer has MSAA enabled blit it to non MSAA Framebuffer and call this function
     */
    public void miscDrawFramebufferTextureBlur(int x, int y, @NotNull Framebuffer t, float blur_size_u,
                                               float blur_size_v , boolean fixBlending){
        useCorrectRenderingTarget();

        if (fixBlending) {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        }

        if (t.isMSAAEnabled()){
            throw new RuntimeException("MSAA Framebuffer cannot be drawn");
        }

        blur_textured_shader_program.useThis();
        glBindVertexArray(blur_textured_vao);

        glUniform1f(utexture_blur_blur_u, blur_size_u);
        glUniform1f(utexture_blur_blur_v, blur_size_v);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y), 0.0f, 1.0f,
                toHardwareX(x+t.getWidth()), toHardwareY(y), 1.0f, 1.0f,
                toHardwareX(x), toHardwareY(y+t.getHeight()), 0.0f, 0.0f,
                toHardwareX(x+t.getWidth()), toHardwareY(y+t.getHeight()), 1.0f, 0.0f,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        int slot = GlobalTextureSlotManager.useTexture(t.getFramebufferTextureID());
        GlobalTextureSlotManager.useCorrectTextureUniform(utexture_blur_sampler2d, slot);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        ErrorCheck.checkErrorGL();

        if (fixBlending){
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

    }

    public void drawRect(int x, int y, int width, int height, @NotNull Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x), toHardwareY(y),
                toHardwareX(x+width), toHardwareY(y),

                toHardwareX(x), toHardwareY(y),
                toHardwareX(x), toHardwareY(y+height),

                toHardwareX(x+width), toHardwareY(y),
                toHardwareX(x+width), toHardwareY(y+height),

                toHardwareX(x), toHardwareY(y+height),
                toHardwareX(x+width), toHardwareY(y+height),
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_LINES, 0, 8);

    }

    public void drawRect(int x, int y, int width, int height, int thickness, Vector4f color){
        this.drawLine(x, y, x+width, y, color, thickness);
        this.drawLine(x, y, x, y+height, color, thickness);
        this.drawLine(x-thickness, y+height, x+width, y+height, color, thickness);
        this.drawLine(x+width, y, x+width, y+height, color, thickness);
    }

    public void fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[]{
                toHardwareX(x1), toHardwareY(y1),
                toHardwareX(x2), toHardwareY(y2),
                toHardwareX(x3), toHardwareY(y3),
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLES, 0, 3);

    }

    public void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, Vector4f color){
        useCorrectRenderingTarget();
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        fillTriangle(x1, y1, x2, y2, x3, y3, color);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    }

    public void fillTriangleGradient(int x1, int y1, int x2, int y2, int x3, int y3, Vector4f color1, Vector4f color2){
        useCorrectRenderingTarget();
        gradient_shader_program.useThis();
        glBindVertexArray(gradient_vao);

        float[] vertices = new float[]{
                toHardwareX(x1), toHardwareY(y1), color1.x, color1.y, color1.z, color1.w,
                toHardwareX(x2), toHardwareY(y2), color2.x, color2.y, color2.z, color2.w,
                toHardwareX(x3), toHardwareY(y3), color2.x, color2.y, color2.z, color2.w,
        };

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLES, 0, 3);

    }

    public void fillCircle(int x, int y, int points, int r, Vector4f color){
        fillEllipse(x, y, points, r, r, color);
    }

    public void drawCircle(int x, int y, int points, int r, Vector4f color){
        drawEllipse(x, y, points, r, r, color);
    }

    public void fillEllipse(int x, int y, int points, int xr, int yr, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[(points+2)*2];
        vertices[0] = toHardwareX(x);
        vertices[1] = toHardwareY(y);

        float angle_step = (float) (2*Math.PI/points);

        for (int p = 0; p <= points; p++){
            int i = 2+p*2;
            vertices[i] = toHardwareX((float) (xr*Math.cos(angle_step*(p-1))+x));
            vertices[i+1] = toHardwareY((float) (yr*Math.sin(angle_step*(p-1))+y));
        }

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_FAN, 0, points+2);

    }

    public void drawEllipse(int x, int y, int points, int xr, int yr, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[(points+1)*2];

        float angle_step = (float) (2*Math.PI/points);

        for (int p = 0; p <= points; p++){
            int i = p*2;
            vertices[i] = toHardwareX((float) (xr*Math.cos(angle_step*(p-1))+x));
            vertices[i+1] = toHardwareY((float) (yr*Math.sin(angle_step*(p-1))+y));
        }

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_LINE_STRIP, 0, points+1);

    }

    /***
     * @param start_point 0 <= start_point < points
     * @param end_point points < end_point < points
     */
    public void partiallyFillEllipse(int x, int y, int start_point, int end_point, int points, int xr, int yr, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[(end_point-start_point+2)*2];
        vertices[0] = toHardwareX(x);
        vertices[1] = toHardwareY(y);

        float angle_step = (float) (2*Math.PI/points);

        for (int p = start_point; p <= end_point; p++){
            int i = 2+(p-start_point)*2;
            vertices[i] = toHardwareX((float) (xr*Math.cos(angle_step*(p-1))+x));
            vertices[i+1] = toHardwareY((float) (yr*Math.sin(angle_step*(p-1))+y));
        }

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_FAN, 0, end_point-start_point+2);

    }

    /***
     * @param start_point 0 <= start_point < points
     * @param end_point points < end_point < points
     */
    public void partiallyFillCircle(int x, int y, int start_point, int end_point, int points, int r, Vector4f color){
        partiallyFillEllipse(x, y, start_point, end_point, points, r, r, color);
    }

    /***
     * Points have to be specified in order from right to left or from left to right
     */
    public void fillPolygon(List<Vector2i> points, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        Vector2i v = new Vector2i();
        for (Vector2i p : points){
            v.add(p);
        }
        v.div(points.size());

        float[] vertices = new float[(points.size()+1)*2];
        vertices[0] = toHardwareX(v.x);
        vertices[1] = toHardwareY(v.y);

        for (int p = 0; p < points.size(); p++){
            int i = (p+1)*2;
            vertices[i] = toHardwareX((float) (points.get(p).x));
            vertices[i+1] = toHardwareY((float) (points.get(p).y));
        }

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_TRIANGLE_FAN, 0, points.size()+1);
    }

    /***
     * Points have to be specified in order from right to left or from left to right
     */
    public void drawPolygon(List<Vector2i> points, Vector4f color){
        useCorrectRenderingTarget();
        primitive_shader_program.useThis();
        glBindVertexArray(primitive_vao);

        glUniform4f(uprimitive_color, color.x, color.y, color.z, color.w);

        float[] vertices = new float[(points.size()+1)*2];

        for (int p = 0; p < points.size(); p++){
            int i = (p)*2;
            vertices[i] = toHardwareX((float) (points.get(p).x));
            vertices[i+1] = toHardwareY((float) (points.get(p).y));
        }

        vertices[points.size()*2] = toHardwareX((float)points.get(0).x);
        vertices[points.size()*2+1] = toHardwareY((float)points.get(0).y);

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                , GL32.GL_STREAM_DRAW);

        glDrawArrays(GL_LINE_STRIP, 0, points.size()+1);
    }


    /***
     * Text is rendered upwards
     * @param origin_x x will be set to this coordinate if newline is encountered
     * @return last (x,y)
     */
    public Vector2i renderText(STGLFont font, int x, int y, String text, Vector4f color, int origin_x){

        useCorrectRenderingTarget();
        font_shader_program.useThis();
        glBindVertexArray(font_vao);
        ErrorCheck.checkErrorGL();

        glUniform3f(utext_color, color.x, color.y, color.z);

        int slot = GlobalTextureSlotManager.useTexture(font.getId(), GL_TEXTURE_2D);
        GlobalTextureSlotManager.useCorrectTextureUniform(ufont_sampler2d, slot);

        float[] vertices = new float[16*text.length()];
        int i = 0;

        for (char c: text.toCharArray()){
            Glyph glyph = font.getChars().get(c);

            if (c == ' '){
                x += font.getMaxWidth();
                continue;
            }

            if (c == '\n'){
                y += font.getMaxHeight();
                x = origin_x;
                continue;
            }

            if (glyph == null){
                throw new RuntimeException("Glyph for char '" + c + "' missing");
            }

            int xpos = x + glyph.getBearing().x;
            int ypos = y - glyph.getBearing().y;

            int width = glyph.getSize().x;
            int height = glyph.getSize().y;

            vertices[i*16] = toHardwareX(xpos);
            vertices[i*16+1] = toHardwareY(ypos);
            vertices[i*16+2] = glyph.getXHardwareOffset();
            vertices[i*16+3] = glyph.getYHardwareOffset();

            vertices[i*16+4] = toHardwareX(xpos+width);
            vertices[i*16+5] = toHardwareY(ypos);
            vertices[i*16+6] = glyph.getXHardwareOffsetMax();
            vertices[i*16+7] = glyph.getYHardwareOffset();

            vertices[i*16+8] = toHardwareX(xpos);
            vertices[i*16+9] = toHardwareY(ypos+height);
            vertices[i*16+10] = glyph.getXHardwareOffset();
            vertices[i*16+11] = glyph.getYHardwareOffsetMax();

            vertices[i*16+12] = toHardwareX(xpos+width);
            vertices[i*16+13] = toHardwareY(ypos+height);
            vertices[i*16+14] = glyph.getXHardwareOffsetMax();
            vertices[i*16+15] = glyph.getYHardwareOffsetMax();

            ErrorCheck.checkErrorGL();

            x += (glyph.getAdvance()>>6);
            i++;
        }

        glBindBuffer(GL_ARRAY_BUFFER, global_vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STREAM_DRAW);

        for (int _i = 0; _i < text.length(); _i++)
            glDrawArrays(GL_TRIANGLE_STRIP, _i*4, 4);

        ErrorCheck.checkErrorGL();
        return new Vector2i(x,y);
    }

    public Vector2i renderText(STGLFont font, int x, int y, String text, Vector4f color){
        return renderText(font, x, y, text, color, x);
    }

    /***
     * @see STGLTextFormatter
     */
    public void renderFormattedText(STGLFont m, STGLFont bold, STGLFont italics, STGLFont stroke, String text, int x, int y){
        List<STGLTextFormatter.Token> tokens = STGLTextFormatter.formattedTokens(text);

        STGLFont current_font = m;
        Vector4f current_color = STGLColors.WHITE;
        int cur_x = x;
        int cur_y = y;
        for (STGLTextFormatter.Token t: tokens){
            if (t.tt() != STGLTextFormatter.TokenType.TEXT){
                switch (t.tt()){
                    case COLOR_BLUE -> current_color = STGLColors.BLUE;
                    case COLOR_CYAN -> current_color = STGLColors.CYAN;
                    case COLOR_RED -> current_color = STGLColors.RED;
                    case COLOR_GRAY -> current_color = STGLColors.GRAY;
                    case COLOR_GREEN -> current_color = STGLColors.GREEN;
                    case COLOR_DARK_GRAY -> current_color = STGLColors.DARK_GRAY;
                    case COLOR_PINK -> current_color = STGLColors.PINK;
                    case COLOR_MAGENTA -> current_color = STGLColors.MAGENTA;
                    case COLOR_LIGHT_GRAY -> current_color = STGLColors.LIGHT_GRAY;
                    case COLOR_ORANGE -> current_color = STGLColors.ORANGE;
                    case COLOR_WHITE -> current_color = STGLColors.WHITE;
                    case BOLD -> current_font = bold;
                    case ITALIC -> current_font = italics;
                    case STROKE -> current_font = stroke;
                    case RESET -> {
                        current_font = m;
                        current_color = STGLColors.WHITE;
                    }
                }
                continue;
            }

            Vector2i v = this.renderText(current_font, cur_x, cur_y, t.t(), current_color, x);
            cur_x = v.x;
            cur_y = v.y;
        }
    }

    /***
     * Clears the framebuffer
     */
    public void clear(){
        useCorrectRenderingTarget();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
    }

    /***
     * Sets clear color
     */
    public void setClearColor(float red, float green, float blue, float alpha){
        useCorrectRenderingTarget();
        glClearColor(red, green, blue, alpha);
    }

    public ShaderProgram getTexturedShaderProgram() {
        return textured_shader_program;
    }

    public ShaderProgram getTextureArrayShaderProgram() {
        return tarray_shader_program;
    }

    public int getTexturedVAO() {
        return textured_vao;
    }

    public int getTextureArrayVAO() {
        return texture_array_vao;
    }

    /***
     * @return uniform location of <code>vec4 color</code> in <code>fshader_texture.frag</code>
     */
    public int getUniformTextureColor() {
        return utexture_color;
    }

    /***
     * @return uniform location of <code>vec4 color</code> in <code>fshader_texture_array.frag</code>
     */
    public int getUniformArrayTextureColor() {
        return utexture_array_color;
    }

    public int getGraphicsVertexBufferObject() {
        return global_vbo;
    }

    public int getFramebufferID(){
        if (framebuffer != null){
            return framebuffer.getId();
        }
        return 0;
    }

    public Framebuffer getFramebuffer(){
        return framebuffer;
    }

    public void dispose(){
        if (parent != null){
            return;
        }

        glDeleteVertexArrays(font_vao);
        glDeleteVertexArrays(textured_vao);
        glDeleteVertexArrays(gradient_vao);
        glDeleteVertexArrays(primitive_vao);

        glDeleteBuffers(global_vbo);
        glDeleteBuffers(global_ebo);

        ErrorCheck.checkErrorGL();
    }

    /***
     * @return uniform location of <code>sampler2d tex</code> in <code>fshader_texture.frag</code>
     */
    public int getUniformTextureSampler2d(){
        return utexture_sampler2d;
    }

    /***
     * @return uniform location of <code>sampler2d tex</code> in <code>fshader_texture_array.frag</code>
     */
    public int getUniformArrayTextureSampler2d(){
        return utexture_array_sampler2d;
    }

    /***
     * Defines a scissor box outside which objects will not be rendered
     * <code>x</code>,<code>y</code> parameters are coordinates in parent's coordinate space
     */
    public void defineScissorBox(int x, int y, int width, int height){
        doScissors = true;
        scissorX = x;
        scissorY = y;
        scissorWidth = width;
        scissorHeight = height;
    }

    private void activateScissorBoxGL(int x, int y, int width, int height){
        int frheight;

        int x1 = x;
        int x2 = x + width;
        int y1 = y;
        int y2 = y + height;

        if (parent != null){
            x1 += parent.getTranslationVectorOrigin().x;
            y1 += parent.getTranslationVectorOrigin().y;
            x2 += parent.getTranslationVectorOrigin().x;
            y2 += parent.getTranslationVectorOrigin().y;

            if (parent.doScissors) {
                // child's scissor box cannot go further than parent's do

                if (x1 > parent.scissorX + parent.scissorWidth){
                    x1 = parent.scissorX + parent.scissorWidth;
                }

                if (x1 < parent.scissorX){
                    x1 = parent.scissorX;
                }

                if (y1 > parent.scissorY + parent.scissorHeight){
                    y1 = parent.scissorY + parent.scissorHeight;
                }

                if (y1 < parent.scissorY){
                    y1 = parent.scissorY;
                }

                //

                if (x2 > parent.scissorX + parent.scissorWidth){
                    x2 = parent.scissorX + parent.scissorWidth;
                }

                if (x2 < parent.scissorX){
                    x2 = parent.scissorX;
                }

                if (y2 > parent.scissorY + parent.scissorHeight){
                    y2 = parent.scissorY + parent.scissorHeight;
                }

                if (y2 < parent.scissorY){
                    y2 = parent.scissorY;
                }

            }
        }

        if (framebuffer == null){
            frheight = Framebuffer.DEFAULT_FRAMEBUFFER.getHeight();
        }
        else {
            frheight = framebuffer.getHeight();
        }

        int _width = x2-x1;
        int _height = y2-y1;

        int tr_y = frheight-(y1+_height);
        glEnable(GL_SCISSOR_TEST);
        glScissor(x1, tr_y, _width, _height);
    }

    public void disableScissorBox(){
        doScissors = false;
    }

    public void setTranslationVector(@NotNull Vector2i translationVector){
        this.translationVector = translationVector;
    }

    /***
     * @return child graphics object which is restricted by parent's scissor box and
     * origin of this graphics is transformed parent coordinate. Child graphics don't
     * own any native resources (it inherits resources from parent).
     * @see STGLGraphics#defineScissorBox(int, int, int, int)
     * @see STGLGraphics#setTranslationVector(Vector2i)
     */
    public STGLGraphics childGraphics(){
        return new STGLGraphics(this);
    }
}
