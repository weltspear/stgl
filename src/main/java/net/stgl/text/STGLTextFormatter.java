package net.stgl.text;

import java.util.ArrayList;
import java.util.List;

public class STGLTextFormatter {

    public enum TokenType{
        COLOR_BLUE,
        COLOR_RED,
        COLOR_GREEN,
        COLOR_CYAN,
        COLOR_GRAY,
        COLOR_WHITE,
        COLOR_DARK_GRAY,
        COLOR_LIGHT_GRAY,
        COLOR_PINK,
        COLOR_ORANGE,
        COLOR_MAGENTA,
        BOLD,
        ITALIC,
        STROKE,
        RESET,

        TEXT
    }

    public record Token(TokenType tt, String t){}

    public static List<Token> formattedTokens(String text){
        ArrayList<Token> tokens = new ArrayList<>();

        StringBuilder current_text = new StringBuilder();

        for (int i = 0; i < text.length(); i++){
            if (text.charAt(i) == '$'){
                if (i+1 < text.length()){
                    if (text.charAt(i+1) == '$'){
                        i+=2;
                        StringBuilder modifier = new StringBuilder();
                        while (i < text.length()){
                            if (text.charAt(i) == ' '){
                                break;
                            }
                            if (text.charAt(i) == '|'){
                                i++;
                                break;
                            }
                            modifier.append(text.charAt(i));
                            i++;
                        }

                        tokens.add(new Token(TokenType.TEXT, current_text.toString()));
                        current_text = new StringBuilder();

                        switch (modifier.toString()){
                            case "b" -> tokens.add(new Token(TokenType.COLOR_BLUE, ""));
                            case "r" -> tokens.add(new Token(TokenType.COLOR_RED, ""));
                            case "g" -> tokens.add(new Token(TokenType.COLOR_GREEN, ""));
                            case "c" -> tokens.add(new Token(TokenType.COLOR_CYAN, ""));
                            case "gr" -> tokens.add(new Token(TokenType.COLOR_GRAY, ""));
                            case "dgr" -> tokens.add(new Token(TokenType.COLOR_DARK_GRAY, ""));
                            case "lgr" -> tokens.add(new Token(TokenType.COLOR_LIGHT_GRAY, ""));
                            case "p" -> tokens.add(new Token(TokenType.COLOR_PINK, ""));
                            case "o" -> tokens.add(new Token(TokenType.COLOR_ORANGE, ""));
                            case "m" -> tokens.add(new Token(TokenType.COLOR_MAGENTA, ""));
                            case "bb" -> tokens.add(new Token(TokenType.BOLD, ""));
                            case "ii" -> tokens.add(new Token(TokenType.ITALIC, ""));
                            case "s" -> tokens.add(new Token(TokenType.STROKE, ""));
                            case "rr" -> tokens.add(new Token(TokenType.RESET, ""));
                        }

                        i--;
                        continue;
                    }
                }
            }
            current_text.append(text.charAt(i));
        }

        tokens.add(new Token(TokenType.TEXT, current_text.toString()));
        return tokens;

    }
}
