package net.stgl.shader;
import org.lwjgl.BufferUtils;

import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL32.*;

@SuppressWarnings("unused")
public class ShaderProgram {

    private final int id;

    public ShaderProgram(){
        id = glCreateProgram();
    }

    public void attachShader(Shader shader){
        glAttachShader(id, shader.getId());
    }

    public void dispose(){
        glDeleteProgram(id);
    }

    public void useThis(){
        glUseProgram(id);
    }

    public void link(){
        glLinkProgram(id);

        IntBuffer intBuffer = BufferUtils.createIntBuffer(1);
        glGetProgramiv(id, GL_LINK_STATUS, intBuffer);
        if (intBuffer.get() != 1){
            throw new RuntimeException(glGetProgramInfoLog(id));
        }
    }

    public int getId() {
        return id;
    }

    public int getAttributeLocation(String name){
        return glGetAttribLocation(id, name);
    }

    public int getUniformLocation(String uniform_name){
        return glGetUniformLocation(id, uniform_name);
    }
}
