package net.stgl.shader;

import org.lwjgl.opengl.GL32;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;

import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.system.MemoryStack.stackPush;

public class ShaderManager {

    public static int compileShader(String shader, int type){
        int _shader = GL32.glCreateShader(type);
        glShaderSource(_shader, shader);
        glCompileShader(_shader);

        try (MemoryStack stack = stackPush() ) {
            IntBuffer status = stack.mallocInt(1);

            glGetShaderiv(_shader, GL_COMPILE_STATUS, status);
            if (status.get() == GL_TRUE){

            }
            else{
                throw new RuntimeException(glGetShaderInfoLog(_shader, 512));
            }
        }

        return _shader;
    }

    /***
     * Loads shader from resources folder
     */
    @SuppressWarnings("all")
    public static Shader loadShader(String path, int type){
        try (InputStream i = ShaderManager.class.getResourceAsStream(path)){
            return new Shader(new String(i.readAllBytes(), StandardCharsets.UTF_8), type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /***
     * Loads shader from resources folder
     */
    @SuppressWarnings("all")
    public static Shader loadShader(String path, int type, String add_to_front){
        try (InputStream i = ShaderManager.class.getResourceAsStream(path)){
            return new Shader(add_to_front + new String(i.readAllBytes(), StandardCharsets.UTF_8), type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
