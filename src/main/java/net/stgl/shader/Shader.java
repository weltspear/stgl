package net.stgl.shader;
import static org.lwjgl.opengl.GL32.*;

@SuppressWarnings("unused")
public class Shader {
    private final int id;
    public Shader(String src, int type){
        id = ShaderManager.compileShader(src, type);
    }

    public int getId(){
        return id;
    }

    public void dispose(){
        glDeleteShader(id);
    }
}
