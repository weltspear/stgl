package net.stgl.texture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class TextureManager {

    /***
     * Loads texture from resources folder
     */
    public static Texture load(String img_path) throws IOException {
        return new Texture(loadBufferedImage(img_path));
    }

    public static BufferedImage loadBufferedImage(String img_path) throws IOException {
        return ImageIO.read(Objects.requireNonNull(TextureManager.class.getClassLoader().getResource(img_path)));
    }
}
