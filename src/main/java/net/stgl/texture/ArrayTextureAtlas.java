package net.stgl.texture;

import java.awt.image.BufferedImage;

public class ArrayTextureAtlas {

    private final ArrayTexture arrayTexture;

    private final BufferedImage[] imagesAtlas;
    private final int twidth;
    private final int theight;

    private int i = 0;

    private final int textureRegionAmount;

    public ArrayTextureAtlas(int texture_region_amount, int twidth, int theight, Texture.TextureOptions textureOptions){
        this.twidth = twidth;
        this.theight = theight;
        arrayTexture = new ArrayTexture(twidth, theight, texture_region_amount, textureOptions);
        imagesAtlas = new BufferedImage[texture_region_amount];
        textureRegionAmount = texture_region_amount;
    }

    public ArrayTextureAtlas(int texture_region_amount, int twidth, int theight){
        this(texture_region_amount, twidth, theight, Texture.DEFAULT_TEXTURE_OPTIONS);
    }


    public ArrayTextureRegion getTextureRegion(BufferedImage img){
        int _i = 0;
        for (BufferedImage image : imagesAtlas){
            if (image == img){
                return arrayTexture.getSubTexture(0, 0, arrayTexture.getWidth(), arrayTexture.getHeight(), _i);
            }
            _i++;
        }

        if (i == textureRegionAmount){
            i = 0;
        }

        arrayTexture.writeImage(i, img);
        imagesAtlas[i] = img;
        i++;
        return arrayTexture.getSubTexture(0, 0, arrayTexture.getWidth(), arrayTexture.getHeight(), i-1);
    }

    public ArrayTexture getArrayTexture() {
        return arrayTexture;
    }

    public int getWidth() {
        return twidth;
    }

    public int getHeight() {
        return theight;
    }
}
