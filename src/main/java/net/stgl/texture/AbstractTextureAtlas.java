package net.stgl.texture;

public abstract class AbstractTextureAtlas {

    public abstract Texture getAtlas();
}
