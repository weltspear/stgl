package net.stgl.texture;

/***
 * Implementor of this interface holds an OpenGL texture.
 */
public interface ITexture {

    int getId();
    int getWidth();
    int getHeight();

    void dispose();
}
