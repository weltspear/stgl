package net.stgl.texture;

public class ArrayTextureRegion {

    private final int offsetX;
    private final int offsetY;
    private final int layer;
    private final int width;
    private final int height;
    private final ArrayTexture arrayTexture;

    public ArrayTextureRegion(ArrayTexture arrayTexture, int offset_x, int offset_y, int layer, int width, int height){
        this.offsetX = offset_x;
        this.offsetY = offset_y;
        this.layer = layer;
        this.width = width;
        this.height = height;
        this.arrayTexture = arrayTexture;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /***
     * Pixel x coordinate of start of texture region in pixels
     */
    @SuppressWarnings("unused")
    public int getOffsetX() {
        return offsetX;
    }

    /***
     * Pixel y coordinate of start of texture region in pixels
     */
    @SuppressWarnings("unused")
    public int getOffsetY() {
        return offsetY;
    }

    @SuppressWarnings("unused")
    public ArrayTexture getArrayTexture() {
        return arrayTexture;
    }

    /***
     * X coordinate of start of texture region in "texture device coordinates"
     */
    public float getTextureOffsetX1(){
        return (float) offsetX/arrayTexture.getWidth();
    }

    /***
     * X coordinate of end of texture region in "texture device coordinates"
     */
    public float getTextureOffsetX2(){
        return ((float) offsetX+width)/arrayTexture.getWidth();
    }

    /***
     * Y coordinate of start of texture region in "texture device coordinates"
     */
    public float getTextureOffsetY1(){
        return (float) offsetY/arrayTexture.getHeight();
    }

    /***
     * Y coordinate of end of texture region in "texture device coordinates"
     */
    public float getTextureOffsetY2(){
        return ((float) offsetY+height)/arrayTexture.getHeight();
    }

    public int getLayer() {
        return layer;
    }
}
