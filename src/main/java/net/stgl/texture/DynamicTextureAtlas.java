package net.stgl.texture;

import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalTextureSlotManager;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;

public class DynamicTextureAtlas extends AbstractTextureAtlas{

    private final Texture atlas;

    private final BufferedImage[] imagesAtlas;
    private final int twidth;
    private final int theight;
    private final int pixelOffset;

    private int i = 0;

    private final int textureRegionAmount;

    public DynamicTextureAtlas(int texture_region_amount, int twidth, int theight, Texture.TextureOptions textureOptions, int pixel_offset){
        this.twidth = twidth;
        this.theight = theight;
        pixelOffset = pixel_offset;
        atlas = new Texture((twidth+pixelOffset)*texture_region_amount, theight+pixelOffset*2, textureOptions);
        imagesAtlas = new BufferedImage[texture_region_amount];
        textureRegionAmount = texture_region_amount;
    }

    public DynamicTextureAtlas(int texture_region_amount, int twidth, int theight){
        this(texture_region_amount, twidth, theight, Texture.DEFAULT_TEXTURE_OPTIONS, 0);
    }

    public TextureRegion getTextureRegion(BufferedImage img){
        int _i = 0;
        for (BufferedImage image : imagesAtlas){
            if (image == img){
                return new TextureRegion(atlas, twidth, theight, _i*(twidth+pixelOffset), pixelOffset);
            }
            _i++;
        }

        if (i == textureRegionAmount){
            i = 0;
        }

        GlobalTextureSlotManager.useTexture(atlas.getId());
        ByteBuffer buffer = Texture.toByteBuffer(img);
        glTexSubImage2D(GL_TEXTURE_2D, 0, i*(twidth+pixelOffset), pixelOffset, twidth, theight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        imagesAtlas[i] = img;
        ErrorCheck.checkErrorGL();
        TextureRegion _t = new TextureRegion(atlas, twidth, theight, i*(twidth+pixelOffset), 0);
        i++;
        return _t;
    }


    @Override
    public Texture getAtlas() {
        return atlas;
    }
}
