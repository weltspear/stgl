package net.stgl.texture;

import net.stgl.debug.ErrorCheck;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;

public class StaticTextureAtlas extends AbstractTextureAtlas{
    private final ArrayList<TextureRegion> textureRegions = new ArrayList<>();
    private final Texture atlas;
    private final int pixelOffset;

    public StaticTextureAtlas(ArrayList<BufferedImage> bufferedImages, Texture.TextureOptions textureOptions, int pixelOffset){
        this.pixelOffset = pixelOffset;

        int mwidth = 0;
        int mheight = 0;

        for (BufferedImage bufferedImage: bufferedImages){
            if (bufferedImage.getWidth() > mwidth){
                mwidth = bufferedImage.getWidth();
            }
            if (bufferedImage.getHeight() > mheight){
                mheight = bufferedImage.getHeight();
            }
        }

        atlas = new Texture((mwidth+pixelOffset)*bufferedImages.size(), mheight+pixelOffset*2, textureOptions);

        int i = 0;

        for (BufferedImage bufferedImage: bufferedImages){
            ByteBuffer buffer = Texture.toByteBuffer(bufferedImage);

            glTexSubImage2D(GL_TEXTURE_2D, 0, i*(mwidth+pixelOffset), pixelOffset, bufferedImage.getWidth(), bufferedImage.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, buffer);

            ErrorCheck.checkErrorGL();

            textureRegions.add(new TextureRegion(atlas, bufferedImage.getWidth(), bufferedImage.getHeight(), i*(mwidth+pixelOffset), pixelOffset));

            i++;
        }

    }

    public StaticTextureAtlas(ArrayList<BufferedImage> bufferedImages){
        this(bufferedImages, Texture.DEFAULT_TEXTURE_OPTIONS, 0);
    }

    public ArrayList<TextureRegion> getTextureRegions() {
        return textureRegions;
    }

    public Texture getAtlas() {
        return atlas;
    }
}
