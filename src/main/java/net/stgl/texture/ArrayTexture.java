package net.stgl.texture;

import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalTextureSlotManager;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL32.*;

/***
 * OpenGL array texture
 */
public class ArrayTexture implements AutoCloseable {

    private final int id;

    private boolean disposeProtection = false;

    private final int width;
    private final int height;

    private final int layers;

    private final Texture.TextureOptions textureOptions;

    public ArrayTexture(int width, int height, int layers, Texture.TextureOptions textureOptions){
        this.width = width;
        this.height = height;
        this.layers = layers;
        this.textureOptions = textureOptions;
        id = GlobalTextureSlotManager.generateValidTextureId(GL_TEXTURE_2D_ARRAY);
        GlobalTextureSlotManager.useTexture(id, GL_TEXTURE_2D_ARRAY);
        ErrorCheck.checkErrorGL();

        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        float[] bcolor = { 1.0f, 0.0f, 0.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BORDER_COLOR, bcolor);

        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, width, height, layers, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

        if (textureOptions.getMinFiltering().isUsingMipmap()){
            glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
        }

        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, textureOptions.getMinFiltering().getConstant());
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, textureOptions.getMagFiltering().getConstant());
        ErrorCheck.checkErrorGL();
    }

    public void writeImage(int layer, ByteBuffer data){
        if (!(layer < layers)){
            throw new IllegalArgumentException("layer is greater than layers");
        }

        GlobalTextureSlotManager.useTexture(id, GL_TEXTURE_2D_ARRAY);
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, layer,
                width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);
        ErrorCheck.checkErrorGL();
    }

    public void writeImage(int layer, BufferedImage data){
        writeImage(layer, Texture.toByteBuffer(data));
    }

    public ArrayTextureRegion getSubTexture(int offset_x, int offset_y,
                                       int width, int height, int layer){
        if (offset_x+width-1 < this.width && offset_y+height-1 < this.height && layer < layers)
            return new ArrayTextureRegion(this, offset_x, offset_y, layer, width, height);
        else throw new RuntimeException("Incorrect parameters provided");
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /***
     * @return amount of layers
     */
    public int getLayers() {
        return layers;
    }

    public void dispose(){
        if (disposeProtection){
            throw new RuntimeException("A texture cannot be disposed more than one time.");
        }
        glDeleteTextures(id);
        disposeProtection = true;
    }
    @Override
    public void close() throws Exception {
        dispose();
    }
}
