package net.stgl.texture;

import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalTextureSlotManager;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL32;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

public class TextureBatch {
    private STGLGraphics graphics;
    private final AbstractTextureAtlas atlas;

    private final FloatBuffer buffer;

    private int textureCount = 0;

    public TextureBatch(STGLGraphics graphics, AbstractTextureAtlas atlas, int max_textures){
        this.graphics = graphics;
        this.atlas = atlas;
        buffer = BufferUtils.createFloatBuffer(max_textures*4*4);
    }

    private void vertex(float a, float b, float c, float d){
        buffer.put(a);
        buffer.put(b);
        buffer.put(c);
        buffer.put(d);
    }

    public void begin(){
        textureCount = 0;
        buffer.clear();
    }

    public void drawTextureRegion(int x, int y, TextureRegion region){
        drawTextureRegionScaled(x, y, region, region.getWidth(), region.getHeight());
    }

    public void drawTextureRegionScaled(int x, int y, TextureRegion region, int scaled_width, int scaled_height){
        vertex(graphics.toHardwareX(x), graphics.toHardwareY(y), region.getTextureOffsetX1(), region.getTextureOffsetY1());
        vertex(graphics.toHardwareX(x+scaled_width), graphics.toHardwareY(y), region.getTextureOffsetX2(), region.getTextureOffsetY1());
        vertex(graphics.toHardwareX(x), graphics.toHardwareY(y+scaled_height), region.getTextureOffsetX1(), region.getTextureOffsetY2());
        vertex(graphics.toHardwareX(x+scaled_width), graphics.toHardwareY(y+scaled_height), region.getTextureOffsetX2(), region.getTextureOffsetY2());
        textureCount++;
    }

    public void flush(){
        graphics.useCorrectRenderingTarget();
        graphics.getTexturedShaderProgram().useThis();
        glBindVertexArray(graphics.getTexturedVAO());

        glUniform4f(graphics.getUniformTextureColor(), 1, 1, 1, 1);
        buffer.flip();

        glBindBuffer(GL_ARRAY_BUFFER, graphics.getGraphicsVertexBufferObject());
        glBufferData(GL32.GL_ARRAY_BUFFER, buffer
                , GL_STREAM_DRAW);
        int __i = GlobalTextureSlotManager.useTexture(atlas.getAtlas().getId());
        GlobalTextureSlotManager.useCorrectTextureUniform(graphics.getUniformTextureSampler2d(), __i);

        for (int i = 0; i < textureCount; i++)
            glDrawArrays(GL_TRIANGLE_STRIP, i*4, 4);
        ErrorCheck.checkErrorGL();
    }

    public void setGraphics(STGLGraphics graphics) {
        this.graphics = graphics;
    }
}
