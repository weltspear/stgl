package net.stgl.texture;

public class TextureRegion {

    private final ITexture textureResource;
    private final int width;
    private final int height;
    private final int offsetX;
    private final int offsetY;

    public TextureRegion(ITexture textureResource, int width, int height, int offset_x, int offset_y){
        this.textureResource = textureResource;
        this.width = width;
        this.height = height;
        offsetX = offset_x;
        offsetY = offset_y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /***
     * Pixel x coordinate of start of texture region in pixels
     */
    @SuppressWarnings("unused")
    public int getOffsetX() {
        return offsetX;
    }

    /***
     * Pixel y coordinate of start of texture region in pixels
     */
    @SuppressWarnings("unused")
    public int getOffsetY() {
        return offsetY;
    }

    @SuppressWarnings("unused")
    public ITexture getTextureResource() {
        return textureResource;
    }

    /***
     * X coordinate of start of texture region in "texture device coordinates"
     */
    public float getTextureOffsetX1(){
        return (float) offsetX/textureResource.getWidth();
    }

    /***
     * X coordinate of end of texture region in "texture device coordinates"
     */
    public float getTextureOffsetX2(){
        return ((float) offsetX+width)/textureResource.getWidth();
    }

    /***
     * Y coordinate of start of texture region in "texture device coordinates"
     */
    public float getTextureOffsetY1(){
        return (float) offsetY/textureResource.getHeight();
    }

    /***
     * Y coordinate of end of texture region in "texture device coordinates"
     */
    public float getTextureOffsetY2(){
        return ((float) offsetY+height)/textureResource.getHeight();
    }
}
