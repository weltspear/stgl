package net.stgl.texture;

import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalTextureSlotManager;
import org.jetbrains.annotations.Nullable;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL32.*;

public class Texture implements AutoCloseable, ITexture {
    private int id;
    private boolean disposeProtection = false;

    private final int width;
    private final int height;

    public static final TextureOptions DEFAULT_TEXTURE_OPTIONS = new TextureOptions().setMagFiltering(Filtering.LINEAR).setMinFiltering(Filtering.LINEAR_MIPMAPS_LINEAR);

    public enum Filtering{
        LINEAR(GL_LINEAR, false),
        NEAREST(GL_NEAREST, false),

        LINEAR_MIPMAPS_LINEAR(GL_LINEAR_MIPMAP_LINEAR, true),
        LINEAR_MIPMAPS_NEAREST(GL_LINEAR_MIPMAP_NEAREST, true),
        NEAREST_MIPMAPS_NEAREST(GL_NEAREST_MIPMAP_NEAREST, true),
        NEAREST_MIPMAPS_LINEAR(GL_NEAREST_MIPMAP_LINEAR, true);

        private final int constant;
        private final boolean isUsingMipmap;

        Filtering(int constant, boolean is_using_mipmap){
            this.constant = constant;
            isUsingMipmap = is_using_mipmap;
        }

        public int getConstant() {
            return constant;
        }

        public boolean isUsingMipmap() {
            return isUsingMipmap;
        }
    }

    public enum Clamping{
        REPEAT(GL_REPEAT),
        MIRRORED_REPEAT(GL_MIRRORED_REPEAT),
        CLAMP_TO_EDGE(GL_CLAMP_TO_EDGE),
        CLAMP_TO_BORDER(GL_CLAMP_TO_BORDER),
        ;

        private final int constant;

        Clamping(int constant){
            this.constant = constant;
        }

        public int getConstant() {
            return constant;
        }
    }

    public static class TextureOptions{

        private Filtering mag_filtering = Filtering.LINEAR;
        private Filtering min_filtering = Filtering.LINEAR;

        private Clamping clamping_u = Clamping.CLAMP_TO_BORDER;
        private Clamping clamping_v = Clamping.CLAMP_TO_BORDER;

        private float[] bcolor = new float[] {0,0,0,0};

        public TextureOptions(){

        }

        public TextureOptions setMagFiltering(Filtering mag_filtering) {
            if (mag_filtering.isUsingMipmap){
                throw new IllegalArgumentException("Mag filtering cannot use mipmaps");
            }
            this.mag_filtering = mag_filtering;
            return this;
        }

        public TextureOptions setMinFiltering(Filtering min_filtering) {
            this.min_filtering = min_filtering;
            return this;
        }

        public Filtering getMagFiltering() {
            return mag_filtering;
        }

        public Filtering getMinFiltering() {
            return min_filtering;
        }

        public TextureOptions setClampingU(Clamping clamping_u) {
            this.clamping_u = clamping_u;
            return this;
        }

        public TextureOptions setClampingV(Clamping clamping_v) {
            this.clamping_v = clamping_v;
            return this;
        }

        public Clamping getClampingU() {
            return clamping_u;
        }

        public Clamping getClampingV() {
            return clamping_v;
        }

        public TextureOptions setBorderColor(float r, float g, float b, float a){
            bcolor = new float[]{r,g,b,a};
            return this;
        }
    }

    public static ByteBuffer toByteBuffer(BufferedImage image){
        int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
        ByteBuffer buffer = ByteBuffer.allocateDirect(image.getWidth() * image.getHeight() * 4);

        for(int h = 0; h < image.getHeight(); h++) {
            for(int w = 0; w < image.getWidth(); w++) {
                int pixel = pixels[h * image.getWidth() + w];

                buffer.put((byte) ((pixel >> 16) & 0xFF));
                buffer.put((byte) ((pixel >> 8) & 0xFF));
                buffer.put((byte) (pixel & 0xFF));
                buffer.put((byte) ((pixel >> 24) & 0xFF));
            }
        }

        buffer.flip();
        return buffer;
    }

    /***
     * Creates a texture which will be stored in V-RAM. Should be disposed after use.
     */
    public Texture(BufferedImage image){
        width = image.getWidth();
        height = image.getHeight();

        ByteBuffer buffer = toByteBuffer(image);

        init(buffer, DEFAULT_TEXTURE_OPTIONS);
    }

    public Texture(BufferedImage image, TextureOptions textureOptions){
        width = image.getWidth();
        height = image.getHeight();

        ByteBuffer buffer = toByteBuffer(image);

        init(buffer, textureOptions);
    }

    public Texture(int width, int height){
        this.width = width;
        this.height = height;

        init(null, DEFAULT_TEXTURE_OPTIONS);
    }

    public Texture(int width, int height, TextureOptions textureOptions){
        this.width = width;
        this.height = height;

        init(null, textureOptions);
    }

    public Texture(int width, int height, ByteBuffer byteBuffer, TextureOptions textureOptions){
        this.width = width;
        this.height = height;

        init(byteBuffer, textureOptions);
    }

    public Texture(int width, int height, int id){
        this.width = width;
        this.height = height;
        this.id = id;
    }

    public Texture(int width, int height, ByteBuffer byteBuffer){
        this.width = width;
        this.height = height;

        init(byteBuffer, DEFAULT_TEXTURE_OPTIONS);
    }

    private void init(@Nullable ByteBuffer buffer, TextureOptions textureOptions){
        id = GlobalTextureSlotManager.generateValidTextureId();
        GlobalTextureSlotManager.useTexture(id);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, textureOptions.clamping_u.constant);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, textureOptions.clamping_v.constant);

        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, textureOptions.bcolor);

        if (buffer == null)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height,
                    0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height,
                    0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

        if (textureOptions.min_filtering.isUsingMipmap){
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureOptions.min_filtering.constant);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureOptions.mag_filtering.constant);

        ErrorCheck.checkErrorGL();
    }

    public void dispose(){
        if (disposeProtection){
            throw new RuntimeException("A texture cannot be disposed more than one time.");
        }
        glDeleteTextures(id);
        disposeProtection = true;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public void close() {
        dispose();
    }

    public int getId() {
        return id;
    }

    public TextureRegion getSubTexture(int offset_x, int offset_y, int width, int height){
        if (offset_x+width-1 < this.width && offset_y+height-1 < this.height)
            return new TextureRegion(this, width, height, offset_x, offset_y);
        else throw new RuntimeException("Incorrect parameters provided");
    }

    public void writeImage(int offset_x, int offset_y, ByteBuffer buffer, int width, int height){
        GlobalTextureSlotManager.useTexture(id);
        glTexSubImage2D(GL_TEXTURE_2D, 0, offset_x, offset_y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    }

    public void writeImage(int offset_x, int offset_y, BufferedImage bufferedImage){
        ByteBuffer buf = Texture.toByteBuffer(bufferedImage);
        writeImage(offset_x, offset_y, buf, bufferedImage.getWidth(), bufferedImage.getHeight());
    }
}
