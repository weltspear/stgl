package net.stgl;

import org.joml.Vector4f;

import java.awt.*;
import java.nio.ByteBuffer;

public class Utils {

    public static Vector4f colorToVec(Color c){
        return new Vector4f(c.getRed()/255f, c.getGreen()/255f, c.getBlue()/255f, c.getAlpha()/255f);
    }

    public static String unicodeCodepointToString(int codepoint){
        byte[] bytes = ByteBuffer.allocate(4).putInt(codepoint).array();
        return new String(new byte[]{bytes[3]});
    }
}
