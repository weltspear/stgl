package net.stgl;

import static org.lwjgl.opengl.GL32.*;

public class BlendUtils {

    public static void setCorrectFBOBlending(){
        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void setDefaultBlending(){
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}
