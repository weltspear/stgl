package net.stgl;

import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalState;
import net.stgl.state.GlobalTextureSlotManager;
import net.stgl.texture.Texture;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL32.*;

public class Framebuffer {
    private int id;
    private int framebuffer_texture;

    private int width;
    private int height;

    private boolean msaaEnabled;

    private boolean usesExternalTexture;

    private final int samples;

    Framebuffer(int width, int height, int framebuffer){
        id = framebuffer;
        this.width = width;
        this.height = height;
        framebuffer_texture = 0;
        msaaEnabled = false;
        usesExternalTexture = false;
        samples = 0;
    }

    public Framebuffer(int width, int height, boolean enableMSAA, int samples){
        id = glGenFramebuffers();
        glBindFramebuffer(GL_FRAMEBUFFER, id);
        GlobalState.framebuffer = id;
        msaaEnabled = enableMSAA;
        this.samples = samples;

        if (enableMSAA) {
            framebuffer_texture = GlobalTextureSlotManager.generateValidTextureId(GL_TEXTURE_2D_MULTISAMPLE);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, framebuffer_texture);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA8, width, height, true);

            glEnable(GL_MULTISAMPLE);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, framebuffer_texture, 0);
            GlobalTextureSlotManager.restoreSlot(0);
        }
        else{
            framebuffer_texture = GlobalTextureSlotManager.generateValidTextureId();
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, framebuffer_texture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height,
                    0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffer_texture, 0);
            ErrorCheck.checkErrorGL();
            GlobalTextureSlotManager.restoreSlot(0);
        }
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
            switch (glCheckFramebufferStatus(GL_FRAMEBUFFER)){
                case GL_FRAMEBUFFER_UNDEFINED -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_UNDEFINED");
                case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
                case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
                case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
                case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
                case GL_FRAMEBUFFER_UNSUPPORTED -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_UNSUPPORTED");
            }
            throw new RuntimeException("Failed to create framebuffer");
        }
        glBindFramebuffer(GL_FRAMEBUFFER,0);
        GlobalState.framebuffer = 0;
        ErrorCheck.checkErrorGL();

        this.width = width;
        this.height = height;
        usesExternalTexture = false;
    }

    public Framebuffer(Texture t){
        id = glGenFramebuffers();
        framebuffer_texture = t.getId();
        glBindFramebuffer(GL_FRAMEBUFFER, id);
        GlobalState.framebuffer = id;
        msaaEnabled = false;
        samples = 0;
        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, t.getId());
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffer_texture, 0);
        GlobalTextureSlotManager.restoreSlot(0);

        switch (glCheckFramebufferStatus(GL_FRAMEBUFFER)){
            case GL_FRAMEBUFFER_UNDEFINED -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_UNDEFINED");
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
            case GL_FRAMEBUFFER_UNSUPPORTED -> throw new RuntimeException("Failed to create framebuffer, GL_FRAMEBUFFER_UNSUPPORTED");
        }

        glBindFramebuffer(GL_FRAMEBUFFER,0);
        GlobalState.framebuffer = 0;
        ErrorCheck.checkErrorGL();

        this.width = t.getWidth();
        this.height = t.getHeight();
        usesExternalTexture = true;

    }

    public Framebuffer(int width, int height){
        this(width, height, false, 0);
    }

    public void useThis(){
        if (GlobalState.framebuffer != id) {
            glBindFramebuffer(GL_FRAMEBUFFER, id);
            glViewport(0,0,width,height);
            GlobalState.framebuffer = id;
        }
    }

    public void setClearColor(Vector4f color){
        glClearColor(color.x, color.y, color.z, color.w);
    }

    public void clear(){
        useThis();
        glClear(GL_COLOR_BUFFER_BIT);
    }

    public static void useDefaultFrameBuffer(){
        if (GlobalState.framebuffer != 0) {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            GlobalState.framebuffer = 0;
        }
    }

    /***
     * If Framebuffer was constructed using an external texture it will not be disposed.
     */
    public void dispose(){
        glDeleteFramebuffers(id);
        if (!usesExternalTexture)
            glDeleteTextures(framebuffer_texture);
    }

    public void blitThisToDefaultFrameBuffer(){
        glBindFramebuffer(GL_READ_FRAMEBUFFER, id);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glBlitFramebuffer(0, 0, width, height, 0, 0, width, height,
                GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }

    public void blitThisToOtherFrameBuffer(int framebuffer_id, int dstx, int dsty, int dstx1, int dsty1){
        glBindFramebuffer(GL_READ_FRAMEBUFFER, id);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer_id);
        glBlitFramebuffer(0, 0, width, height, dstx, dsty, dstx1, dsty1,
                GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }

    // fixme
    public void blitThisToOtherFrameBuffer(int framebuffer_id){
        glBindFramebuffer(GL_READ_FRAMEBUFFER, id);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer_id);
        glBlitFramebuffer(0, 0, width, height, 0, 0, width, height,
                GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getId() {
        return id;
    }

    public int getFramebufferTextureID() {
        return framebuffer_texture;
    }

    /***
     *
     * @return do not dispose this texture, it is managed by Framebuffer
     */
    public Texture getFramebufferTexture(){
        return new Texture(width, height, id);
    }

    public boolean isMSAAEnabled() {
        return msaaEnabled;
    }

    public static Framebuffer DEFAULT_FRAMEBUFFER = null;

    public ByteBuffer readPixels(){
        GlobalState.framebuffer = id;
        glBindFramebuffer(GL_FRAMEBUFFER, id);

        ByteBuffer image = BufferUtils.createByteBuffer(width*height*4);
        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image);
        image.flip();

        GlobalState.framebuffer =0;
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return image;
    }

    /***
     * Changes size parameters of this Framebuffer object
     * <br>NOTE: this does not actually resize the FBO it only makes STGL "aware" of a resize.
     */
    public void externalResize(int nwidth, int nheight){
        width = nwidth;
        height = nheight;
    }

    /***
     * Creates a new FBO with new width and height
     */
    public void recreateFramebuffer(int nwidth, int nheight){
        dispose();
        Framebuffer nframebuffer = new Framebuffer(nwidth, nheight, msaaEnabled, samples);
        this.id = nframebuffer.id;
        this.width = nwidth;
        this.height = nheight;
        this.usesExternalTexture = false;
        this.framebuffer_texture = nframebuffer.framebuffer_texture;
    }

    /***
     * Creates a new FBO with new width and height
     */
    public void recreateFramebuffer(Texture t){
        dispose();
        Framebuffer nframebuffer = new Framebuffer(t);
        this.id = nframebuffer.id;
        this.width = t.getWidth();
        this.height = t.getHeight();
        this.msaaEnabled = false;
        this.usesExternalTexture = true;
        this.framebuffer_texture = nframebuffer.framebuffer_texture;
    }

}
