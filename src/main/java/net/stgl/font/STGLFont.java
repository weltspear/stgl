package net.stgl.font;

import net.stgl.texture.ITexture;

import java.util.HashMap;

import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glIsTexture;


public class STGLFont implements ITexture {

    public class FontMetrics{
        public int getCharWidth(char c){
            if (c == ' '){
                return mwidth;
            }
            return chars.get(c).getAdvance()>>6;
        }

        public int getCharHeight(char c){
            return chars.get(c).getSize().y;
        }

        public int getStringWidth(String s){
            int width = 0;
            for (char c: s.toCharArray()){
                width+=getCharWidth(c);
            }
            return width;
        }

        public int getMaxCharWidth(){
            return mwidth;
        }

        public int getMaxCharHeight(){
            return mheight;
        }
    }

    private final String fontPath;
    private final HashMap<Character, Glyph> chars;
    private final int size;
    private final String familyName;
    private int textureAtlas;
    private final int mwidth;
    private final int mheight;
    private final int twidth;

    private final boolean bold;
    private final boolean italics;

    private boolean disposeProtection = false;

    /***
     * @param twidth Texture atlas width
     */
    public STGLFont(String font_path,
                    HashMap<Character, Glyph> chars, int size, String family_name, int texture_atlas,
                    int mwidth, int mheight, int twidth,
                    boolean bold, boolean italics){
        fontPath = font_path;
        this.chars = chars;
        this.size = size;
        familyName = family_name;
        textureAtlas = texture_atlas;
        this.mwidth = mwidth;
        this.mheight = mheight;
        this.twidth = twidth;
        this.bold = bold;
        this.italics = italics;
    }

    public HashMap<Character, Glyph> getChars() {
        return chars;
    }

    public int getSize() {
        return size;
    }

    public String getFamilyName() {
        return familyName;
    }

    public int getTextureAtlas() {
        return textureAtlas;
    }

    public int getMaxWidth() {
        return mwidth;
    }

    public int getMaxHeight() {
        return mheight;
    }

    public int getTextureAtlasWidth() {
        return twidth;
    }

    public void dispose(){
        /*if (disposeProtection){
            throw new RuntimeException("A font cannot be disposed more than one time.");
        }
        glDeleteTextures(textureAtlas);
        disposeProtection = true;*/
        throw new RuntimeException("Fonts may not be disposed");
    }

    /***
     * @return Atlas width
     */
    @Override
    public int getId() {
        return textureAtlas;
    }

    /***
     * @return Atlas width
     */
    @Override
    public int getWidth() {
        return twidth;
    }

    /***
     * @return Atlas height
     */
    @Override
    public int getHeight() {
        return mheight;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalics() {
        return italics;
    }

    public FontMetrics getFontMetrics(){
        return new FontMetrics();
    }
}
