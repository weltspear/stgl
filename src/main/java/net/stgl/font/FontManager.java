package net.stgl.font;

import net.stgl.debug.ErrorCheck;
import net.stgl.state.GlobalTextureSlotManager;
import org.joml.Vector2i;
import org.lwjgl.PointerBuffer;
import org.lwjgl.util.freetype.FT_Bitmap;
import org.lwjgl.util.freetype.FT_BitmapGlyph;
import org.lwjgl.util.freetype.FT_Face;
import org.lwjgl.util.freetype.FT_Glyph;

import java.nio.ByteBuffer;
import java.util.HashMap;

import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.util.freetype.FreeType.*;

public class FontManager {
    private static final PointerBuffer library;

    static {
        library = PointerBuffer.allocateDirect(1);

        FT_Init_FreeType(library);
    }

    private record StoredFontData(String path, ByteBuffer data){

    }
    private static HashMap<String, StoredFontData> fontDataMap = new HashMap<>();

    public static void loadFont(String key, String path){
        fontDataMap.put(key, new StoredFontData(path, null));
    }

    public static void loadFont(String key, ByteBuffer data){
        fontDataMap.put(key, new StoredFontData(null, data));
    }

    private record DerivedFontInformation(String key, int size, boolean bold, boolean italics){}

    private static HashMap<DerivedFontInformation, STGLFont> cachedFonts = new HashMap<>();

    public static STGLFont deriveFont(String key, int size, boolean bold, /*currently doesn't work*/ boolean italics){
        var fontInfo = new DerivedFontInformation(key, size, bold, italics);
        if (cachedFonts.containsKey(fontInfo)){
            return cachedFonts.get(fontInfo);
        }
        else{
            if (!fontDataMap.containsKey(key)) {
                throw new RuntimeException("Font was not loaded before deriving");
            }
            else{
                var fontStored = fontDataMap.get(key);
                STGLFont createdFont = createFont(fontStored.path, fontStored.data, size, bold, italics);
                cachedFonts.put(fontInfo, createdFont);
                return createdFont;
            }
        }
    }

    /***
     * Creates font with provided style settings
     * @param font_path This is not a path to a resource in resources folder, this is a path from your sources root
     * @param italics Currently isn't supported
     */
    @SuppressWarnings("all")
    private static STGLFont createFont(String font_path, ByteBuffer data, int size, boolean bold, /*currently doesn't work*/ boolean italics){
        PointerBuffer face = PointerBuffer.allocateDirect(1);
        if (data == null) {
            if (FT_New_Face(library.get(0), font_path, 0, face) != 0) {
                throw new RuntimeException("Failed to load font " + font_path);
            }
        }
        else{
            if (FT_New_Memory_Face(library.get(0), data, 0, face) != 0){
                throw new RuntimeException("Failed to load font " + font_path);
            }
        }

        FT_Face ftFace = FT_Face.create(face.get(0));

        // dynamic width
        FT_Set_Pixel_Sizes(ftFace, 0, size);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // remove alignment restriction

        HashMap<Character, Glyph> chars = new HashMap<>();

        int mwidth = 0;
        int mheight = 0;
        int twidth = 0;

        int max_advance = 0;

        for (/*unsigned*/ char i = 0; i < 128; i++){
            if (FT_Load_Char(ftFace, i, FT_LOAD_RENDER) != 0)
            {
                throw new RuntimeException("Failed to load glyph");
            }

            int gwidth = ftFace.glyph().bitmap().width();
            int gheight = ftFace.glyph().bitmap().rows();
            int gadvance = (int) ftFace.glyph().advance().x() >> 6;

            if (bold){
                PointerBuffer ftGlyphPtr = PointerBuffer.allocateDirect(1);
                if (FT_Get_Glyph(ftFace.glyph(), ftGlyphPtr) == 1){
                    throw new RuntimeException("Failed to load glyph");
                }
                FT_Glyph ftGlyph = FT_Glyph.create(ftGlyphPtr.get(0));
                FT_Glyph_To_Bitmap(ftGlyphPtr, FT_RENDER_MODE_NORMAL, null, true);
                FT_BitmapGlyph ftBitmapGlyph = FT_BitmapGlyph.create(ftGlyphPtr.get(0));
                FT_Bitmap bitmap = ftBitmapGlyph.bitmap();

                FT_Bitmap_Embolden(library.get(0), bitmap, 32, 32);

                gwidth = bitmap.width();
                gheight = bitmap.rows();
                gadvance = (int) ftFace.glyph().advance().x() >> 6;
            }

            if (gwidth > mwidth){
                mwidth = gwidth;
            }
            if (gheight > mwidth){
                mheight = gheight;
            }
            if (max_advance < gadvance){
                max_advance = gadvance;
            }
        }

        twidth = mwidth*128;

        int texture_atlas = GlobalTextureSlotManager.generateValidTextureId();
        ErrorCheck.checkErrorGL();
        GlobalTextureSlotManager.useTexture(texture_atlas);
        ErrorCheck.checkErrorGL();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, twidth, mheight, 0, GL_RED, GL_UNSIGNED_BYTE, 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        ErrorCheck.checkErrorGL();

        for (/*unsigned*/ char i = 0; i < 128; i++){
            if (FT_Load_Char(ftFace, i, FT_LOAD_RENDER) == 1)
            {
                throw new RuntimeException("Failed to load glyph");
            }

            PointerBuffer ftGlyphPtr = PointerBuffer.allocateDirect(1);
            if (FT_Get_Glyph(ftFace.glyph(), ftGlyphPtr) == 1){
                throw new RuntimeException("Failed to load glyph");
            }
            FT_Glyph ftGlyph = FT_Glyph.create(ftGlyphPtr.get(0));


            ByteBuffer glyph_;

            if (bold) {
                FT_Glyph_To_Bitmap(ftGlyphPtr, FT_RENDER_MODE_NORMAL, null, true);
                FT_BitmapGlyph ftBitmapGlyph = FT_BitmapGlyph.create(ftGlyphPtr.get(0));
                FT_Bitmap bitmap = ftBitmapGlyph.bitmap();

                FT_Bitmap_Embolden(library.get(0), bitmap, 32, 32);

                glyph_ = bitmap.buffer(bitmap.rows()*bitmap.width());

                if (glyph_ == null){
                    continue;
                }

                ErrorCheck.checkErrorGL();

                glTexSubImage2D(GL_TEXTURE_2D, 0, i*mwidth, 0, bitmap.width(), bitmap.rows(), GL_RED, GL_UNSIGNED_BYTE, glyph_);
                ErrorCheck.checkErrorGL();

                Glyph glyph = new Glyph(new Vector2i(bitmap.width(), bitmap.rows()),
                        new Vector2i(ftBitmapGlyph.left(), ftBitmapGlyph.top()),
                        (int) ftFace.glyph().advance().x(), i*mwidth, mwidth, mheight, twidth);

                chars.put(i, glyph);
            }
            else{
                glyph_ = ftFace.glyph().bitmap().buffer(ftFace.glyph().bitmap().rows()*ftFace.glyph().bitmap().width());

                if (glyph_ == null){
                    continue;
                }
                ErrorCheck.checkErrorGL();

                glTexSubImage2D(GL_TEXTURE_2D, 0, i*mwidth, 0, ftFace.glyph().bitmap().width(), ftFace.glyph().bitmap().rows(), GL_RED, GL_UNSIGNED_BYTE, glyph_);
                ErrorCheck.checkErrorGL();

                Glyph glyph = new Glyph(new Vector2i(ftFace.glyph().bitmap().width(), ftFace.glyph().bitmap().rows()),
                        new Vector2i(ftFace.glyph().bitmap_left(), ftFace.glyph().bitmap_top()),
                        (int) ftFace.glyph().advance().x(), i*mwidth, mwidth, mheight, twidth);

                chars.put(i, glyph);
            }
        }

        ErrorCheck.checkErrorGL();

        String name = ftFace.family_nameString();

        FT_Done_Face(ftFace);

        return new STGLFont(font_path,chars, size, name, texture_atlas, max_advance,
                mheight, twidth, bold, italics);
    }
}
