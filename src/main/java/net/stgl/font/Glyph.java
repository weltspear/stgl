package net.stgl.font;

import org.joml.Vector2i;

public class Glyph {
    private final Vector2i bearing;
    private final Vector2i size;
    private final /*unsigned*/ int advance;
    private final int xoffset;
    private final int mwidth;
    private final int mheight;
    private final int twidth;

    public Glyph(Vector2i size, Vector2i bearing, int advance, int xoffset, int mwidth, int mheight, int twidth){
        this.bearing = bearing;
        this.size = size;
        this.advance = advance;
        this.xoffset = xoffset;
        this.mwidth = mwidth;
        this.mheight = mheight;
        this.twidth = twidth;
    }

    public Vector2i getBearing() {
        return bearing;
    }

    public Vector2i getSize() {
        return size;
    }

    public int getAdvance() {
        return advance;
    }

    public float getXHardwareOffset(){
        return (float)xoffset/(float)twidth;
    }

    public float getYHardwareOffset(){
        return 0;
    }

    public float getXHardwareOffsetMax(){
        return ((float)xoffset+size.x)/ twidth;
    }

    public float getYHardwareOffsetMax(){
        return (float) size.y /mheight;
    }


}
