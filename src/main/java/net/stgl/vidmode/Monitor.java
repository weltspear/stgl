package net.stgl.vidmode;

import java.util.ArrayList;

public record Monitor(long monitor, ArrayList<VideoMode> videoModes, String name) {

}
