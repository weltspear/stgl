package net.stgl.vidmode;

public record VideoMode(int width, int height, int refreshRate) {

}
