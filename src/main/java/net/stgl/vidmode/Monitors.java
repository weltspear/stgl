package net.stgl.vidmode;

import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWVidMode;

import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;

public class Monitors {

    public static ArrayList<Monitor> getMonitors() {
        ArrayList<Monitor> _monitors = new ArrayList<>();

        PointerBuffer monitors = glfwGetMonitors(); // GLFWmonitor**

        for (int i = 0; i < monitors.capacity(); i++) {
            String name = glfwGetMonitorName(monitors.get(i));
            ArrayList<VideoMode> videoModes = new ArrayList<>();
            for (GLFWVidMode vidmode : glfwGetVideoModes(monitors.get(i))) {
                videoModes.add(new VideoMode(vidmode.width(), vidmode.height(), vidmode.refreshRate()));
            }
            Monitor monitor = new Monitor(monitors.get(i), videoModes, name);
            _monitors.add(monitor);
        }

        return _monitors;
    }

    public static VideoMode getPrimaryVideoMode(){
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        return new VideoMode(vidmode.width(), vidmode.height(), vidmode.refreshRate());
    }

    public static Monitor getPrimaryMonitor(){
        long mon = glfwGetPrimaryMonitor();
        String name = glfwGetMonitorName(mon);
        ArrayList<VideoMode> videoModes = new ArrayList<>();
        for (GLFWVidMode vidmode : glfwGetVideoModes(mon)) {
            videoModes.add(new VideoMode(vidmode.width(), vidmode.height(), vidmode.refreshRate()));
        }
        return new Monitor(mon, videoModes, name);
    }
}
