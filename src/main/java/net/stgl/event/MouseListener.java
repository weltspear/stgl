package net.stgl.event;

public interface MouseListener {

    void mouseMoved(double x, double y);

    /***
     * GLFW_PRESS
     */
    void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y);

    /***
     * GLFW_RELEASE
     */
    void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y);

    void mouseWheelMoved(double xoffset, double yoffset);
}
