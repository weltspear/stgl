package net.stgl.event;

public interface KeyboardListener {

    /***
     * GLFW_PRESS
     */
    void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers);

    /***
     * GLFW_RELEASE
     */
    void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers);

    /***
     * GLFW_REPEAT
     */
    void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers);

    /***
     * @param codepoint Unicode codepoint of character
     */
    void charTyped(int codepoint);
}
