package net.stgl.event;

public interface ActionListener {

    void action();
}
