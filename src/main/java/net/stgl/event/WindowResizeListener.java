package net.stgl.event;

public interface WindowResizeListener {
    void windowResize(int width, int height);
}
