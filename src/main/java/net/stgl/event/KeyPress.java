package net.stgl.event;

import org.lwjgl.glfw.GLFW;

/***
 * This enum abstracts away keypress constants
 */
public enum KeyPress {
    KEY_UNKNOWN(GLFW.GLFW_KEY_UNKNOWN),
    KEY_SPACE(GLFW.GLFW_KEY_SPACE),
    KEY_APOSTROPHE(GLFW.GLFW_KEY_APOSTROPHE),
    KEY_COMMA(GLFW.GLFW_KEY_COMMA),
    KEY_MINUS(GLFW.GLFW_KEY_MINUS),
    KEY_PERIOD(GLFW.GLFW_KEY_PERIOD),
    KEY_SLASH(GLFW.GLFW_KEY_SLASH),
    KEY_0(GLFW.GLFW_KEY_0),
    KEY_1(GLFW.GLFW_KEY_1),
    KEY_2(GLFW.GLFW_KEY_2),
    KEY_3(GLFW.GLFW_KEY_3),
    KEY_4(GLFW.GLFW_KEY_4),
    KEY_5(GLFW.GLFW_KEY_5),
    KEY_6(GLFW.GLFW_KEY_6),
    KEY_7(GLFW.GLFW_KEY_7),
    KEY_8(GLFW.GLFW_KEY_8),
    KEY_9(GLFW.GLFW_KEY_9),
    KEY_SEMICOLON(GLFW.GLFW_KEY_SEMICOLON),
    KEY_EQUAL(GLFW.GLFW_KEY_EQUAL),
    KEY_A(GLFW.GLFW_KEY_A),
    KEY_B(GLFW.GLFW_KEY_B),
    KEY_C(GLFW.GLFW_KEY_C),
    KEY_D(GLFW.GLFW_KEY_D),
    KEY_E(GLFW.GLFW_KEY_E),
    KEY_F(GLFW.GLFW_KEY_F),
    KEY_G(GLFW.GLFW_KEY_G),
    KEY_H(GLFW.GLFW_KEY_H),
    KEY_I(GLFW.GLFW_KEY_I),
    KEY_J(GLFW.GLFW_KEY_J),
    KEY_K(GLFW.GLFW_KEY_K),
    KEY_L(GLFW.GLFW_KEY_L),
    KEY_M(GLFW.GLFW_KEY_M),
    KEY_N(GLFW.GLFW_KEY_N),
    KEY_O(GLFW.GLFW_KEY_O),
    KEY_P(GLFW.GLFW_KEY_P),
    KEY_Q(GLFW.GLFW_KEY_Q),
    KEY_R(GLFW.GLFW_KEY_R),
    KEY_S(GLFW.GLFW_KEY_S),
    KEY_T(GLFW.GLFW_KEY_T),
    KEY_U(GLFW.GLFW_KEY_U),
    KEY_V(GLFW.GLFW_KEY_V),
    KEY_W(GLFW.GLFW_KEY_W),
    KEY_X(GLFW.GLFW_KEY_X),
    KEY_Y(GLFW.GLFW_KEY_Y),
    KEY_Z(GLFW.GLFW_KEY_Z),
    KEY_LEFT_BRACKET(GLFW.GLFW_KEY_LEFT_BRACKET),
    KEY_BACKSLASH(GLFW.GLFW_KEY_BACKSLASH),
    KEY_RIGHT_BRACKET(GLFW.GLFW_KEY_RIGHT_BRACKET),
    KEY_GRAVE_ACCENT(GLFW.GLFW_KEY_GRAVE_ACCENT),
    KEY_WORLD_1(GLFW.GLFW_KEY_WORLD_1),
    KEY_WORLD_2(GLFW.GLFW_KEY_WORLD_2),
    KEY_ESCAPE(GLFW.GLFW_KEY_ESCAPE),
    KEY_ENTER(GLFW.GLFW_KEY_ENTER),
    KEY_TAB(GLFW.GLFW_KEY_TAB),
    KEY_BACKSPACE(GLFW.GLFW_KEY_BACKSPACE),
    KEY_INSERT(GLFW.GLFW_KEY_INSERT),
    KEY_DELETE(GLFW.GLFW_KEY_DELETE),
    KEY_RIGHT(GLFW.GLFW_KEY_RIGHT),
    KEY_LEFT(GLFW.GLFW_KEY_LEFT),
    KEY_DOWN(GLFW.GLFW_KEY_DOWN),
    KEY_UP(GLFW.GLFW_KEY_UP),
    KEY_PAGE_UP(GLFW.GLFW_KEY_PAGE_UP),
    KEY_PAGE_DOWN(GLFW.GLFW_KEY_PAGE_DOWN),
    KEY_HOME(GLFW.GLFW_KEY_HOME),
    KEY_END(GLFW.GLFW_KEY_END),
    KEY_CAPS_LOCK(GLFW.GLFW_KEY_CAPS_LOCK),
    KEY_SCROLL_LOCK(GLFW.GLFW_KEY_SCROLL_LOCK),
    KEY_NUM_LOCK(GLFW.GLFW_KEY_NUM_LOCK),
    KEY_PRINT_SCREEN(GLFW.GLFW_KEY_PRINT_SCREEN),
    KEY_PAUSE(GLFW.GLFW_KEY_PAUSE),
    KEY_F1(GLFW.GLFW_KEY_F1),
    KEY_F2(GLFW.GLFW_KEY_F2),
    KEY_F3(GLFW.GLFW_KEY_F3),
    KEY_F4(GLFW.GLFW_KEY_F4),
    KEY_F5(GLFW.GLFW_KEY_F5),
    KEY_F6(GLFW.GLFW_KEY_F6),
    KEY_F7(GLFW.GLFW_KEY_F7),
    KEY_F8(GLFW.GLFW_KEY_F8),
    KEY_F9(GLFW.GLFW_KEY_F9),
    KEY_F10(GLFW.GLFW_KEY_F10),
    KEY_F11(GLFW.GLFW_KEY_F11),
    KEY_F12(GLFW.GLFW_KEY_F12),
    KEY_F13(GLFW.GLFW_KEY_F13),
    KEY_F14(GLFW.GLFW_KEY_F14),
    KEY_F15(GLFW.GLFW_KEY_F15),
    KEY_F16(GLFW.GLFW_KEY_F16),
    KEY_F17(GLFW.GLFW_KEY_F17),
    KEY_F18(GLFW.GLFW_KEY_F18),
    KEY_F19(GLFW.GLFW_KEY_F19),
    KEY_F20(GLFW.GLFW_KEY_F20),
    KEY_F21(GLFW.GLFW_KEY_F21),
    KEY_F22(GLFW.GLFW_KEY_F22),
    KEY_F23(GLFW.GLFW_KEY_F23),
    KEY_F24(GLFW.GLFW_KEY_F24),
    KEY_F25(GLFW.GLFW_KEY_F25),
    KEY_KP_0(GLFW.GLFW_KEY_KP_0),
    KEY_KP_1(GLFW.GLFW_KEY_KP_1),
    KEY_KP_2(GLFW.GLFW_KEY_KP_2),
    KEY_KP_3(GLFW.GLFW_KEY_KP_3),
    KEY_KP_4(GLFW.GLFW_KEY_KP_4),
    KEY_KP_5(GLFW.GLFW_KEY_KP_5),
    KEY_KP_6(GLFW.GLFW_KEY_KP_6),
    KEY_KP_7(GLFW.GLFW_KEY_KP_7),
    KEY_KP_8(GLFW.GLFW_KEY_KP_8),
    KEY_KP_9(GLFW.GLFW_KEY_KP_9),
    KEY_KP_DECIMAL(GLFW.GLFW_KEY_KP_DECIMAL),
    KEY_KP_DIVIDE(GLFW.GLFW_KEY_KP_DIVIDE),
    KEY_KP_MULTIPLY(GLFW.GLFW_KEY_KP_MULTIPLY),
    KEY_KP_SUBTRACT(GLFW.GLFW_KEY_KP_SUBTRACT),
    KEY_KP_ADD(GLFW.GLFW_KEY_KP_ADD),
    KEY_KP_ENTER(GLFW.GLFW_KEY_KP_ENTER),
    KEY_KP_EQUAL(GLFW.GLFW_KEY_KP_EQUAL),
    KEY_LEFT_SHIFT(GLFW.GLFW_KEY_LEFT_SHIFT),
    KEY_LEFT_CONTROL(GLFW.GLFW_KEY_LEFT_CONTROL),
    KEY_LEFT_ALT(GLFW.GLFW_KEY_LEFT_ALT),
    KEY_LEFT_SUPER(GLFW.GLFW_KEY_LEFT_SUPER),
    KEY_RIGHT_SHIFT(GLFW.GLFW_KEY_RIGHT_SHIFT),
    KEY_RIGHT_CONTROL(GLFW.GLFW_KEY_RIGHT_CONTROL),
    KEY_RIGHT_ALT(GLFW.GLFW_KEY_RIGHT_ALT),
    KEY_RIGHT_SUPER(GLFW.GLFW_KEY_RIGHT_SUPER),
    KEY_MENU(GLFW.GLFW_KEY_MENU),
    KEY_LAST(GLFW.GLFW_KEY_LAST),
    ;
    private final int glfw_constant;

    KeyPress(int glfw_constant){
        this.glfw_constant = glfw_constant;
    }

    public int getGLFWConstant() {
        return glfw_constant;
    }

    public static KeyPress fromString(String str){
        if ("KEY_UNKNOWN".equals( str)) { return KeyPress.KEY_UNKNOWN;}
        if ("KEY_SPACE".equals( str)) { return KeyPress.KEY_SPACE;}
        if ("KEY_APOSTROPHE".equals( str)) { return KeyPress.KEY_APOSTROPHE;}
        if ("KEY_COMMA".equals( str)) { return KeyPress.KEY_COMMA;}
        if ("KEY_MINUS".equals( str)) { return KeyPress.KEY_MINUS;}
        if ("KEY_PERIOD".equals( str)) { return KeyPress.KEY_PERIOD;}
        if ("KEY_SLASH".equals( str)) { return KeyPress.KEY_SLASH;}
        if ("KEY_0".equals( str)) { return KeyPress.KEY_0;}
        if ("KEY_1".equals( str)) { return KeyPress.KEY_1;}
        if ("KEY_2".equals( str)) { return KeyPress.KEY_2;}
        if ("KEY_3".equals( str)) { return KeyPress.KEY_3;}
        if ("KEY_4".equals( str)) { return KeyPress.KEY_4;}
        if ("KEY_5".equals( str)) { return KeyPress.KEY_5;}
        if ("KEY_6".equals( str)) { return KeyPress.KEY_6;}
        if ("KEY_7".equals( str)) { return KeyPress.KEY_7;}
        if ("KEY_8".equals( str)) { return KeyPress.KEY_8;}
        if ("KEY_9".equals( str)) { return KeyPress.KEY_9;}
        if ("KEY_SEMICOLON".equals( str)) { return KeyPress.KEY_SEMICOLON;}
        if ("KEY_EQUAL".equals( str)) { return KeyPress.KEY_EQUAL;}
        if ("KEY_A".equals( str)) { return KeyPress.KEY_A;}
        if ("KEY_B".equals( str)) { return KeyPress.KEY_B;}
        if ("KEY_C".equals( str)) { return KeyPress.KEY_C;}
        if ("KEY_D".equals( str)) { return KeyPress.KEY_D;}
        if ("KEY_E".equals( str)) { return KeyPress.KEY_E;}
        if ("KEY_F".equals( str)) { return KeyPress.KEY_F;}
        if ("KEY_G".equals( str)) { return KeyPress.KEY_G;}
        if ("KEY_H".equals( str)) { return KeyPress.KEY_H;}
        if ("KEY_I".equals( str)) { return KeyPress.KEY_I;}
        if ("KEY_J".equals( str)) { return KeyPress.KEY_J;}
        if ("KEY_K".equals( str)) { return KeyPress.KEY_K;}
        if ("KEY_L".equals( str)) { return KeyPress.KEY_L;}
        if ("KEY_M".equals( str)) { return KeyPress.KEY_M;}
        if ("KEY_N".equals( str)) { return KeyPress.KEY_N;}
        if ("KEY_O".equals( str)) { return KeyPress.KEY_O;}
        if ("KEY_P".equals( str)) { return KeyPress.KEY_P;}
        if ("KEY_Q".equals( str)) { return KeyPress.KEY_Q;}
        if ("KEY_R".equals( str)) { return KeyPress.KEY_R;}
        if ("KEY_S".equals( str)) { return KeyPress.KEY_S;}
        if ("KEY_T".equals( str)) { return KeyPress.KEY_T;}
        if ("KEY_U".equals( str)) { return KeyPress.KEY_U;}
        if ("KEY_V".equals( str)) { return KeyPress.KEY_V;}
        if ("KEY_W".equals( str)) { return KeyPress.KEY_W;}
        if ("KEY_X".equals( str)) { return KeyPress.KEY_X;}
        if ("KEY_Y".equals( str)) { return KeyPress.KEY_Y;}
        if ("KEY_Z".equals( str)) { return KeyPress.KEY_Z;}
        if ("KEY_LEFT_BRACKET".equals( str)) { return KeyPress.KEY_LEFT_BRACKET;}
        if ("KEY_BACKSLASH".equals( str)) { return KeyPress.KEY_BACKSLASH;}
        if ("KEY_RIGHT_BRACKET".equals( str)) { return KeyPress.KEY_RIGHT_BRACKET;}
        if ("KEY_GRAVE_ACCENT".equals( str)) { return KeyPress.KEY_GRAVE_ACCENT;}
        if ("KEY_WORLD_1".equals( str)) { return KeyPress.KEY_WORLD_1;}
        if ("KEY_WORLD_2".equals( str)) { return KeyPress.KEY_WORLD_2;}
        if ("KEY_ESCAPE".equals( str)) { return KeyPress.KEY_ESCAPE;}
        if ("KEY_ENTER".equals( str)) { return KeyPress.KEY_ENTER;}
        if ("KEY_TAB".equals( str)) { return KeyPress.KEY_TAB;}
        if ("KEY_BACKSPACE".equals( str)) { return KeyPress.KEY_BACKSPACE;}
        if ("KEY_INSERT".equals( str)) { return KeyPress.KEY_INSERT;}
        if ("KEY_DELETE".equals( str)) { return KeyPress.KEY_DELETE;}
        if ("KEY_RIGHT".equals( str)) { return KeyPress.KEY_RIGHT;}
        if ("KEY_LEFT".equals( str)) { return KeyPress.KEY_LEFT;}
        if ("KEY_DOWN".equals( str)) { return KeyPress.KEY_DOWN;}
        if ("KEY_UP".equals( str)) { return KeyPress.KEY_UP;}
        if ("KEY_PAGE_UP".equals( str)) { return KeyPress.KEY_PAGE_UP;}
        if ("KEY_PAGE_DOWN".equals( str)) { return KeyPress.KEY_PAGE_DOWN;}
        if ("KEY_HOME".equals( str)) { return KeyPress.KEY_HOME;}
        if ("KEY_END".equals( str)) { return KeyPress.KEY_END;}
        if ("KEY_CAPS_LOCK".equals( str)) { return KeyPress.KEY_CAPS_LOCK;}
        if ("KEY_SCROLL_LOCK".equals( str)) { return KeyPress.KEY_SCROLL_LOCK;}
        if ("KEY_NUM_LOCK".equals( str)) { return KeyPress.KEY_NUM_LOCK;}
        if ("KEY_PRINT_SCREEN".equals( str)) { return KeyPress.KEY_PRINT_SCREEN;}
        if ("KEY_PAUSE".equals( str)) { return KeyPress.KEY_PAUSE;}
        if ("KEY_F1".equals( str)) { return KeyPress.KEY_F1;}
        if ("KEY_F2".equals( str)) { return KeyPress.KEY_F2;}
        if ("KEY_F3".equals( str)) { return KeyPress.KEY_F3;}
        if ("KEY_F4".equals( str)) { return KeyPress.KEY_F4;}
        if ("KEY_F5".equals( str)) { return KeyPress.KEY_F5;}
        if ("KEY_F6".equals( str)) { return KeyPress.KEY_F6;}
        if ("KEY_F7".equals( str)) { return KeyPress.KEY_F7;}
        if ("KEY_F8".equals( str)) { return KeyPress.KEY_F8;}
        if ("KEY_F9".equals( str)) { return KeyPress.KEY_F9;}
        if ("KEY_F10".equals( str)) { return KeyPress.KEY_F10;}
        if ("KEY_F11".equals( str)) { return KeyPress.KEY_F11;}
        if ("KEY_F12".equals( str)) { return KeyPress.KEY_F12;}
        if ("KEY_F13".equals( str)) { return KeyPress.KEY_F13;}
        if ("KEY_F14".equals( str)) { return KeyPress.KEY_F14;}
        if ("KEY_F15".equals( str)) { return KeyPress.KEY_F15;}
        if ("KEY_F16".equals( str)) { return KeyPress.KEY_F16;}
        if ("KEY_F17".equals( str)) { return KeyPress.KEY_F17;}
        if ("KEY_F18".equals( str)) { return KeyPress.KEY_F18;}
        if ("KEY_F19".equals( str)) { return KeyPress.KEY_F19;}
        if ("KEY_F20".equals( str)) { return KeyPress.KEY_F20;}
        if ("KEY_F21".equals( str)) { return KeyPress.KEY_F21;}
        if ("KEY_F22".equals( str)) { return KeyPress.KEY_F22;}
        if ("KEY_F23".equals( str)) { return KeyPress.KEY_F23;}
        if ("KEY_F24".equals( str)) { return KeyPress.KEY_F24;}
        if ("KEY_F25".equals( str)) { return KeyPress.KEY_F25;}
        if ("KEY_KP_0".equals( str)) { return KeyPress.KEY_KP_0;}
        if ("KEY_KP_1".equals( str)) { return KeyPress.KEY_KP_1;}
        if ("KEY_KP_2".equals( str)) { return KeyPress.KEY_KP_2;}
        if ("KEY_KP_3".equals( str)) { return KeyPress.KEY_KP_3;}
        if ("KEY_KP_4".equals( str)) { return KeyPress.KEY_KP_4;}
        if ("KEY_KP_5".equals( str)) { return KeyPress.KEY_KP_5;}
        if ("KEY_KP_6".equals( str)) { return KeyPress.KEY_KP_6;}
        if ("KEY_KP_7".equals( str)) { return KeyPress.KEY_KP_7;}
        if ("KEY_KP_8".equals( str)) { return KeyPress.KEY_KP_8;}
        if ("KEY_KP_9".equals( str)) { return KeyPress.KEY_KP_9;}
        if ("KEY_KP_DECIMAL".equals( str)) { return KeyPress.KEY_KP_DECIMAL;}
        if ("KEY_KP_DIVIDE".equals( str)) { return KeyPress.KEY_KP_DIVIDE;}
        if ("KEY_KP_MULTIPLY".equals( str)) { return KeyPress.KEY_KP_MULTIPLY;}
        if ("KEY_KP_SUBTRACT".equals( str)) { return KeyPress.KEY_KP_SUBTRACT;}
        if ("KEY_KP_ADD".equals( str)) { return KeyPress.KEY_KP_ADD;}
        if ("KEY_KP_ENTER".equals( str)) { return KeyPress.KEY_KP_ENTER;}
        if ("KEY_KP_EQUAL".equals( str)) { return KeyPress.KEY_KP_EQUAL;}
        if ("KEY_LEFT_SHIFT".equals( str)) { return KeyPress.KEY_LEFT_SHIFT;}
        if ("KEY_LEFT_CONTROL".equals( str)) { return KeyPress.KEY_LEFT_CONTROL;}
        if ("KEY_LEFT_ALT".equals( str)) { return KeyPress.KEY_LEFT_ALT;}
        if ("KEY_LEFT_SUPER".equals( str)) { return KeyPress.KEY_LEFT_SUPER;}
        if ("KEY_RIGHT_SHIFT".equals( str)) { return KeyPress.KEY_RIGHT_SHIFT;}
        if ("KEY_RIGHT_CONTROL".equals( str)) { return KeyPress.KEY_RIGHT_CONTROL;}
        if ("KEY_RIGHT_ALT".equals( str)) { return KeyPress.KEY_RIGHT_ALT;}
        if ("KEY_RIGHT_SUPER".equals( str)) { return KeyPress.KEY_RIGHT_SUPER;}
        if ("KEY_MENU".equals( str)) { return KeyPress.KEY_MENU;}
        if ("KEY_LAST".equals( str)) { return KeyPress.KEY_LAST;}
        return KEY_UNKNOWN;
    }
}
