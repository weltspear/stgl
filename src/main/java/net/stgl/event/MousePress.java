package net.stgl.event;

import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;

/***
 * This enum abstracts away mouse press constants
 */
public enum MousePress {
    MOUSE_BUTTON_1(GLFW.GLFW_MOUSE_BUTTON_1),
    MOUSE_BUTTON_2(GLFW.GLFW_MOUSE_BUTTON_2),
    MOUSE_BUTTON_3(GLFW.GLFW_MOUSE_BUTTON_3),
    MOUSE_BUTTON_4(GLFW.GLFW_MOUSE_BUTTON_4),
    MOUSE_BUTTON_5(GLFW.GLFW_MOUSE_BUTTON_5),
    MOUSE_BUTTON_6(GLFW.GLFW_MOUSE_BUTTON_6),
    MOUSE_BUTTON_7(GLFW.GLFW_MOUSE_BUTTON_7),
    MOUSE_BUTTON_8(GLFW.GLFW_MOUSE_BUTTON_8),
    MOUSE_BUTTON_LAST(GLFW.GLFW_MOUSE_BUTTON_LAST),
    ;
    private final int glfw_constant;

    MousePress(int glfw_constant){
        this.glfw_constant = glfw_constant;
    }

    public int getGLFWConstant() {
        return glfw_constant;
    }

    public static @Nullable MousePress fromString(String str){
        if ("MOUSE_BUTTON_1".equals( str)) { return MousePress.MOUSE_BUTTON_1;}
        if ("MOUSE_BUTTON_2".equals( str)) { return MousePress.MOUSE_BUTTON_2;}
        if ("MOUSE_BUTTON_3".equals( str)) { return MousePress.MOUSE_BUTTON_3;}
        if ("MOUSE_BUTTON_4".equals( str)) { return MousePress.MOUSE_BUTTON_4;}
        if ("MOUSE_BUTTON_5".equals( str)) { return MousePress.MOUSE_BUTTON_5;}
        if ("MOUSE_BUTTON_6".equals( str)) { return MousePress.MOUSE_BUTTON_6;}
        if ("MOUSE_BUTTON_7".equals( str)) { return MousePress.MOUSE_BUTTON_7;}
        if ("MOUSE_BUTTON_8".equals( str)) { return MousePress.MOUSE_BUTTON_8;}
        if ("MOUSE_BUTTON_LAST".equals( str)) { return MousePress.MOUSE_BUTTON_LAST;}
        if ("MOUSE_BUTTON_LEFT".equals( str)) { return MousePress.MOUSE_BUTTON_1;}
        if ("MOUSE_BUTTON_RIGHT".equals( str)) { return MousePress.MOUSE_BUTTON_2;}
        if ("MOUSE_BUTTON_MIDDLE".equals( str)) { return MousePress.MOUSE_BUTTON_3;}
        return null;
    }
}
