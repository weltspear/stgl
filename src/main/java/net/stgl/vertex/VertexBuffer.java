package net.stgl.vertex;
import java.nio.*;

import static org.lwjgl.opengl.GL32.*;

/***
 * Abstraction around OpenGL's VBO
 */
public class VertexBuffer {

    private final int vbo;

    public VertexBuffer(){
        vbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
    }

    public void bind(){
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
    }

    public int getVBO() {
        return vbo;
    }

    public void bufferData(float[] vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(int[] vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(long[] vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(double[] vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(short[] vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(IntBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(FloatBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(ByteBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(ShortBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(LongBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void bufferData(DoubleBuffer vertices, int usage){
        glBufferData(GL_ARRAY_BUFFER, vertices, usage);
    }

    public void drawArrays(int mode, int first, int count){
        glDrawArrays(mode, first, count);
    }

    public class VertexArrays{

        private final int vao;
        private VertexArrays(){
            VertexBuffer.this.bind();
            vao = glGenVertexArrays();
            bind();
        }

        public void bind(){
            glBindVertexArray(vao);
        }

        public void vertexAttribPointer(int attrib_location, int size, int type,
                                        boolean normalized, int stride, int pointer){
            glVertexAttribPointer(attrib_location, size, type, normalized, stride, pointer);
            glEnableVertexAttribArray(attrib_location);
        }
    }

    public VertexArrays createVertexArrays(){
        bind();
        return new VertexArrays();
    }
}
