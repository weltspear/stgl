package net.stgl.state;

import net.stgl.debug.ErrorCheck;
import org.lwjgl.opengl.GL11;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL32.*;

public class GlobalTextureSlotManager {

    private static final Integer[] boundTextures = new Integer[32];
    private static final Integer[] boundTexturesTargets = new Integer[32];
    private static int i = 0;

    private static int current_slot = 0;

    /***
     * Binds a texture to designated texture slot and makes it active
     * @return Texture slot which is supposed to be used
     */
    public static int useTexture(int id, int target){
        if (Objects.equals(boundTextures[current_slot], id)){
            return current_slot;
        }

        int ts = 0;
        for (Integer t1: boundTextures){
            if (t1 == null){
                continue;
            }

            if (t1 == id){
                glActiveTexture(GL_TEXTURE0+ts);
                current_slot = ts;
                return ts;
            }
            ts++;
        }

        if (i > 31){
            i = 0;
        }

        while (true){
            if (boundTextures[i] != null){
                if (boundTextures[i] != -1){
                    break;
                }
            }
            if (boundTextures[i] == null){
                break;
            }
            i++;
            if (i > 31){
                i = 0;
            }
        }

        boundTextures[i] = id;
        boundTexturesTargets[i] = target;
        glActiveTexture(GL_TEXTURE0+i);
        current_slot = i;
        glBindTexture(target, id);
        i++;
        return i-1;
    }

    /***
     * Binds a texture to designated texture slot and makes it active
     * @return Texture slot which is supposed to be used
     */
    public static int useTexture(int id){
        return useTexture(id, GL_TEXTURE_2D);
    }


    /***
     * Changes sampler2D uniform to currently used texture slot
     * @param i value returned by useTexture
     * @see GlobalTextureSlotManager#useTexture(int)
     */
    public static void useCorrectTextureUniform(int uniform_location, int i){
        glUniform1i(uniform_location, i);
        ErrorCheck.checkErrorGL();
    }

    /***
     * If a certain texture slot has been used and it's contents have been altered
     * without checking whether this slot is used by STGL this will restore its
     * contents. If a slot which is being restored is reserved its reservation will be
     * reversed.
     */
    public static void restoreSlot(int slot){
        if (boundTextures[slot] != null)
            if (boundTextures[slot] == -1){
                boundTextures[slot] = null;
                return;
            }

        if (boundTextures[slot] == null){
            return;
        }

        glActiveTexture(GL_TEXTURE0+slot);
        glBindTexture(boundTexturesTargets[slot], boundTextures[slot]);
        current_slot = slot;
    }

    public static boolean isSlotReserved(int slot){
        return Objects.equals(boundTextures[slot], -1);
    }

    /***
     * Makes a slot unusable by STGL
     * @return a slot index where 0 < i < 32
     */
    public static int reserveSlot(){
        int _i = i;
        while (_i == 0 || (boundTextures[_i] != null && boundTextures[_i] == -1)){
            _i++;
            if (_i > 31){
                _i = 0;
            }
        }

        boundTextures[_i] = -1;
        return _i;
    }

    /***
     * Generates valid texture id
     * For some reason I have noticed that after deleting a texture
     * generating a new texture right after results in invalid texture
     * id being generated.
     * I have checked that using glIsTexture. This function guarantees a
     * fully valid texture id as it generates ids until valid one is generated.
     * This probably is caused by some sort of driver bug.
     */
    public static int generateValidTextureId(){
        return generateValidTextureId(GL_TEXTURE_2D);
    }

    /***
     * Generates valid texture id
     * For some reason I have noticed that after deleting a texture
     * generating a new texture right after results in invalid texture
     * id being generated.
     * I have checked that using glIsTexture. This function guarantees a
     * fully valid texture id as it generates ids until valid one is generated.
     * This probably is caused by some sort of driver bug.
     */
    public static int generateValidTextureId(int target){
        int id = GL11.glGenTextures();
        GlobalTextureSlotManager.useTexture(id, target);
        while (!glIsTexture(id)){
            if (ErrorCheck.isDebuggingEnabled())
                Logger.getGlobal().log(Level.WARNING, "[STGL] LWJGL generated trash texture id: " + id);
            id = GL11.glGenTextures();
            GlobalTextureSlotManager.useTexture(id, target);
        }
        return id;
    }
}
