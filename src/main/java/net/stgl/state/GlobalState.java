package net.stgl.state;

/***
 * This is here in order to not rebind stuff all the time when it is unnecessary
 */
public class GlobalState {

    public static int framebuffer = 0;

    /***
     * The ResourceManager is disposed when MainFrame is disposed
     */
    public static final ResourceManager defaultResourceManager = new ResourceManager();
}
