package net.stgl.state;

import net.stgl.shader.Shader;
import net.stgl.shader.ShaderManager;
import net.stgl.shader.ShaderProgram;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ResourceManager {

    private final HashMap<String, Shader> shadersLoaded = new HashMap<>();
    private final HashMap<String, ShaderProgram> shaderPrograms = new HashMap<>();

    private final HashMap<String, Texture> textureCache = new HashMap<>();

    public ResourceManager(){

    }

    public Shader loadShader(String filepath, int type){
        if (!shadersLoaded.containsKey(filepath)) {
            Shader shader = ShaderManager.loadShader(filepath, type);
            shadersLoaded.put(filepath, shader);
            return shader;
        }
        else return shadersLoaded.get(filepath);
    }

    public ShaderProgram getShaderProgram(String name){
        return shaderPrograms.get(name);
    }

    public void storeShaderProgram(String shaderProgramName, ShaderProgram shaderProgram){
        shaderPrograms.put(shaderProgramName, shaderProgram);
    }

    public Texture loadTexture(String path) throws IOException {
        if (!textureCache.containsKey(path)){
            Texture t = TextureManager.load(path);
            textureCache.put(path, t);
            return t;
        }
        return textureCache.get(path);
    }

    public void dispose(){
        for (Map.Entry<String, Shader> shaderEntry : shadersLoaded.entrySet()){
            shaderEntry.getValue().dispose();
        }

        for (Map.Entry<String, ShaderProgram> shaderProgramEntry : shaderPrograms.entrySet()){
            shaderProgramEntry.getValue().dispose();
        }

        for (Map.Entry<String, Texture> textureEntry: textureCache.entrySet()){
            textureEntry.getValue().dispose();
        }
    }
}
