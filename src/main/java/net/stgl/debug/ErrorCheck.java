package net.stgl.debug;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.GL_INVALID_FRAMEBUFFER_OPERATION;

public class ErrorCheck {
    private static boolean debug = false;

    public static boolean isDebuggingEnabled() {
        return debug;
    }

    /***
     * Check for OpenGL errors
     */
    public static void checkErrorGL(){
        if (debug) {
            String error = switch (glGetError()) {
                case GL_INVALID_ENUM -> "INVALID_ENUM";
                case GL_INVALID_VALUE -> "INVALID_VALUE";
                case GL_INVALID_OPERATION -> "INVALID_OPERATION";
                case GL_STACK_OVERFLOW -> "STACK_OVERFLOW";
                case GL_STACK_UNDERFLOW -> "STACK_UNDERFLOW";
                case GL_OUT_OF_MEMORY -> "OUT_OF_MEMORY";
                case GL_INVALID_FRAMEBUFFER_OPERATION -> "INVALID_FRAMEBUFFER_OPERATION";
                default -> "";
            };

            if (!error.equals("")) {
                throw new RuntimeException(error);
            }
        }
    }

    public static void enableDebugging(){
        debug = true;
    }
}
